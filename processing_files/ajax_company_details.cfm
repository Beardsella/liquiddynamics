<!DOCTYPE html>
<html lang="en">
<head>

  <cfquery name="this_company"  datasource="agbcodes_ldi">
    SELECT *
    FROM companies
    WHERE company_name = "#URLEncodedFormat(URL.q)#"
  </cfquery>

</head>
<body>
    <cfoutput>

        <cfif this_company.recordcount GT 0>

          <input type="hidden" name="company_id" id="company_id" value="#this_company.id#">

          <div class="col-12 alert alert-success alert-dismissible fade show" role="alert">
            The company details for "#URLDecode(this_company.company_name)#" have been populated
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
              <label for="company_email">Email Address</label>
                <input type="email" class="form-control" id="company_email" name="company_email" value="#URLDecode(this_company.email)#">
                </div>
              </div>

              <div class="col-lg-6">
              <div class="form-group">
              <label for="company_telephone">Phone Number</label>
                <input type="text" class="form-control" id="company_telephone" name="company_telephone" value="#URLDecode(this_company.telephone)#">
                </div>
              </div>
          </div> 

          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
              <label for="company_address">Address</label>
                <input type="text" class="form-control" id="company_address" name="company_address" value="#URLDecode(this_company.mail_1)# #URLDecode(this_company.mail_2)# #URLDecode(this_company.mail_3)#">
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                <label for="company_town">Town</label>
                  <input type="text" class="form-control" id="company_town" name="company_town" value="#URLDecode(this_company.town)#">
                  </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                <label for="company_postcode">Post Code</label>
                  <input type="text" class="form-control" id="company_postcode" name="company_postcode" value="#URLDecode(this_company.postcode)#">
                  </div>
              </div>

              <div class="col-lg-12">
                <div class="form-group">
                <label for="company_country">Country</label>
                  <input type="text" class="form-control" id="company_country" name="company_country" value="#URLDecode(this_company.country)#">
                </div>
              </div>

              <div class="col-lg-12">
                <div class="form-group">
                <label for="company_notes">Company Notes</label>
                  <textarea class="form-control" id="company_notes" name="company_notes">#URLDecode(this_company.notes)#</textarea>
                </div>
              </div>

          </div> 


        <cfelse>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
              <label for="company_email">Email Address</label>
                <input type="email" class="form-control" id="company_email" name="company_email">
                </div>
              </div>

              <div class="col-lg-6">
              <div class="form-group">
              <label for="company_telephone">Phone Number</label>
                <input type="text" class="form-control" id="company_telephone" name="company_telephone">
                </div>
              </div>
          </div> 

          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
              <label for="company_address">Address</label>
                <input type="text" class="form-control" id="company_address" name="company_address">
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                <label for="company_town">Town</label>
                  <input type="text" class="form-control" id="company_town" name="company_town">
                  </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                <label for="company_postcode">Post Code</label>
                  <input type="text" class="form-control" id="company_postcode" name="company_postcode">
                  </div>
              </div>

              <div class="col-lg-12">
                <div class="form-group">
                <label for="company_country">Country</label>
                  <input type="text" class="form-control" id="company_country" name="company_country">
                </div>
              </div>

              <div class="col-lg-12">
                <div class="form-group">
                <label for="company_notes">Company Notes</label>
                  <textarea class="form-control" id="company_notes" name="company_notes"></textarea>
                </div>
              </div>

          </div> 
        </cfif>


        </cfoutput>

</body>
</html>