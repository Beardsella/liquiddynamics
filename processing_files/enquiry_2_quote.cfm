<cfquery name="this_enquiry" datasource="agbcodes_ldi">
  SELECT *
  FROM enquirys_pump_systems
  WHERE id = '#URL.ID#'
</cfquery>

<cfquery name="this_company" datasource="agbcodes_ldi">
  SELECT *
  FROM companies
  WHERE id = "#this_enquiry.company_id#"
</cfquery>

<cfquery name="this_contact" datasource="agbcodes_ldi">
  SELECT *
  FROM contacts
  WHERE id = "#this_enquiry.contact_id#"
</cfquery>

<cfquery datasource="agbcodes_ldi">
  INSERT INTO quotes (linked_enq_id,
                      company_id, 
                      company_name, 
                      contact_id, 
                      first_name, 
                      last_name, 
                      telephone, 
                      email, 
                      seller)
  VALUES ("#this_enquiry.id#",
    '#this_enquiry.company_id#',
    "#this_company.company_name#",
    '#this_enquiry.contact_id#',
    "#this_contact.first_name#",
    "#this_contact.last_name#",
    "#this_contact.telephone#",
    "#this_contact.email#",
    "#this_enquiry.seller#")
</cfquery>

<cfquery name="this_quote" datasource="agbcodes_ldi">
  SELECT MAX(id) AS lastid
  FROM quotes
</cfquery>

<cfquery datasource="agbcodes_ldi">
  INSERT INTO quote_items (quote_id,
                          wetted_material,
                          non_wetted_material,
                          flexing_membrane,
                          working_pressure,
                          working_pressure_units,
                          design_pressure,
                          design_pressure_units)
  VALUES ("#this_quote.lastid#",
          "#this_enquiry.material_wetted#",
          "#this_enquiry.material_non_wetted#",
          "#this_enquiry.plastic_system_materials#",
          "#this_enquiry.max_system_pressure#",
          "#this_enquiry.max_system_pressure_units#",
          "#this_enquiry.system_design_pressure#",
          "#this_enquiry.system_design_pressure_units#")
</cfquery>

<cfif isDefined("this_enquiry.seller") AND this_enquiry.seller neq "">

  <cfset selling_company = "#URLDecode(this_enquiry.seller)#">

  <cfif selling_company eq "Liquid Dynamics International">
    <cfset formatted_id = "LDI-UK-Q-#this_quote.lastid#">
  <cfelseif selling_company eq "Pulse Gard">
    <cfset formatted_id = "PG-UK-Q-#this_quote.lastid#">
  <cfelseif selling_company eq "Shock Gard">
    <cfset formatted_id = "SG-UK-Q-#this_quote.lastid#">
  <cfelseif selling_company eq "Hydrotrole">
    <cfset formatted_id = "HT-UK-Q-#this_quote.lastid#">
  </cfif>

</cfif>

<cfquery datasource="agbcodes_ldi">
  UPDATE quotes
  SET formatted_id = "#formatted_id#"
  WHERE id = "#this_quote.lastid#"
</cfquery>

<cflocation url="../quote_management.cfm?id=#this_quote.lastid#" addtoken="no">