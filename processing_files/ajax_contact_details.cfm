<!DOCTYPE html>
<html lang="en">
<head>

  <cfquery name="this_company"  datasource="agbcodes_ldi">
    SELECT id
    FROM companies
    WHERE company_name = "#URLEncodedFormat(URL.q)#"
  </cfquery>

  <cfif this_company.recordcount GT 0>
  <cfquery name="these_contacts"  datasource="agbcodes_ldi">
    SELECT *
    FROM contacts
    WHERE company_id = "#this_company.id#"
  </cfquery>

</cfif>

</head>
<body>


  <cfoutput>

    <cfif this_company.recordcount GT 0>

      <cfif these_contacts.recordcount GT 0>
        <div class="alert alert-info col-12  alert-dismissible fade show" role="alert">
          #these_contacts.RecordCount# Contacts linked to this company.
          <ul>
            <cfloop query="these_contacts">
              <li>"#URLDecode(first_name)#", "#URLDecode(last_name)#"</li>
            </cfloop>
          </ul>
        </div>
      <cfelse>
        <div class="alert alert-warning col-12  alert-dismissible fade show" role="alert">
          There are currently no contacts linked to this company.
        </div>   
      </cfif>

    <cfelse>

      <div class="alert alert-info col-12  alert-dismissible fade show" role="alert">
        New company detected.
      </div>

    </cfif>

<cfif this_company.recordcount LT 1>

<div class="row">

            <div class="col-lg-6">
              <div class="form-group">
              <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Joe">
                </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
              <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Bloggs">
              </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_email">Email Address</label>
              <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="Email Address">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_telephone">Phone Number</label>
              <input type="text" class="form-control" id="contact_telephone" name="contact_telephone" placeholder="Phone Number">
              </div>
            </div>
        </div> 

          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="##contact_details" aria-expanded="false" aria-controls="contact_details">
            More Contact Details...
          </button>
        </p>
        
        <div class="collapse" id="contact_details">
        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="staff_role">Staff Role</label>
              <input type="text" class="form-control" id="staff_role" name="staff_role" placeholder="Purchasing Manager">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_country_based">Country Based</label>
              <input type="text" class="form-control" id="contact_country_based" name="contact_country_based" placeholder="UK">
              </div>
            </div>
        </div> 

        <div class="form-group">
        <label for="contact_notes">Contact Notes</label>
          <textarea class="form-control" id="contact_notes" name="contact_notes"></textarea>
        </div>

        </div>

        


<cfelse>

  <div class="row">

            <div class="col-lg-6">
              <div class="form-group">
              <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Joe" list="first_name_list">
                </div>
            </div>

            <datalist id="first_name_list">
              <cfloop query="these_contacts">
                <option value="#URLDecode(first_name)#">
              </cfloop>
            </datalist>

            <div class="col-lg-6">
              <div class="form-group">
              <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Bloggs" list="last_name_list" onchange="fill_in_contact(this.value)">
              </div>
            </div>

            <datalist id="last_name_list">
              <cfloop query="these_contacts">
                <option value="#URLDecode(last_name)#">
              </cfloop>
            </datalist>

        </div>

        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_email">Email Address</label>
              <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="Email Address">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_telephone">Phone Number</label>
              <input type="text" class="form-control" id="contact_telephone" name="contact_telephone" placeholder="Phone Number">
              </div>
            </div>
        </div> 

          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="##contact_details" aria-expanded="false" aria-controls="contact_details">
            More Contact Details...
          </button>
        </p>
        
        <div class="collapse" id="contact_details">
        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="staff_role">Staff Role</label>
              <input type="text" class="form-control" id="staff_role" name="staff_role" placeholder="Purchasing Manager">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_country_based">Country Based</label>
              <input type="text" class="form-control" id="contact_country_based" name="contact_country_based" placeholder="UK">
              </div>
            </div>
        </div> 

        <div class="form-group">
        <label for="contact_notes">Contact Notes</label>
          <textarea class="form-control" id="contact_notes" name="contact_notes"></textarea>
        </div>

        </div>

</cfif>


        </cfoutput>

</body>
</html>