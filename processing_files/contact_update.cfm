<cfquery name="this_contact" datasource="agbcodes_ldi">
    SELECT id
    FROM contacts
    WHERE id = #FORM.contact_id#
</cfquery>

<cfquery name="contacts" datasource="agbcodes_ldi">
    UPDATE contacts
    SET title = "#FORM.title#",
     first_name = "#FORM.first_name#",
      last_name = "#FORM.last_name#",
        telephone = "#FORM.telephone#",
         email = "#FORM.email#",
          staff_role = "#FORM.staff_role#",
           country_based = "#FORM.country_based#",
            notes = "#URLEncodedFormat(FORM.contact_notes)#"
    WHERE id = #FORM.contact_id#
</cfquery>

<cflocation url="../contact_detail.cfm?id=#this_contact.id#" addtoken="no">