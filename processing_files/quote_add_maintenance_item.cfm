<cfdump var="#form#">

<cfquery datasource="agbcodes_ldi">
  INSERT INTO quote_extra_items (quote_id, item_refrence, item_notes, item_price, currency, conversion)
  VALUES ('#FORM.quote_id#', "#URLEncodedFormat(FORM.NEW_MAINTENANCE_ITEM)#", "#URLEncodedFormat(FORM.NEW_MAINTENANCE_ITEM_NOTES)#", "#DecimalFormat(FORM.NEW_MAINTENANCE_PRICE_EACH)#", "#URLEncodedFormat(FORM.NEW_MAINTENANCE_CURRENCY)#", "#DecimalFormat(FORM.NEW_MAINTENANCE_CONVERSION_RATE)#")
</cfquery>

<cflocation URL="../quote_management.cfm?id=#FORM.quote_id#" addtoken="no">