<cfdump var="#form#">

<cfset exp_date = DateAdd("d", '#FORM.quote_expiration_date#', "#DateFormat(Now())#")>

<cfquery datasource="agbcodes_ldi">
	UPDATE quotes
	SET cust_enquiry_ref = "#URLEncodedFormat(FORM.cust_enquiry_ref)#",
		quote_intro = "#URLEncodedFormat(FORM.quote_intro)#",
		quote_dated = <cfqueryparam value="#FORM.quote_dated#" cfsqltype="cf_sql_timestamp">,
		dispatch_estimate = <cfqueryparam value="#FORM.dispatch_estimate#" cfsqltype="cf_sql_timestamp">,
		notes = "#URLEncodedFormat(FORM.notes)#",
		freebies = "#URLEncodedFormat(FORM.freebies)#",
		not_included = "#URLEncodedFormat(FORM.not_included)#",
		sale_condtions = "#URLEncodedFormat(FORM.sale_condtions)#",
		terms_of_payment = "#URLEncodedFormat(FORM.terms_of_payment)#",
		quote_vaild_for = <cfqueryparam value="#exp_date#" cfsqltype="cf_sql_timestamp">
	WHERE id = '#FORM.quote_id#' 
</cfquery>

<!-- IF Common Quote Params Exist -->
<cfif not isDefined("FORM.parameter_name") OR FORM.parameter_name eq "">

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO quote_params (quote_id, param, param_value, param_notes)
		VALUES ('#FORM.quote_id#', "#URLEncodedFormat(FORM.job_parameter_name)#", "#URLEncodedFormat(FORM.job_parameter_value)#", "#URLEncodedFormat(FORM.job_parameter_notes)#" )
	</cfquery>

</cfif>

<!-- IF Quote items Exist -->
<cfif>

	<!-- COUNT NUMBER OF ITEMS AND LOOP -->

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO quote_items (quote_id, item_name, item_refrence, item_notes, item_price, currency, conversion, system_operating_temp, system_working_pressure, system_design_pressure, co-efficent_thermal_expansion, connection, materials_of_construction, other)
		VALUES ('#FORM.quote_id#', "#URLEncodedFormat(FORM.item_name)#", "#URLEncodedFormat(FORM.parameter_value)#", "#URLEncodedFormat(FORM.parameter_notes)#" )
	</cfquery>

	<!-- IF QUOTE ITEM Params Exist -->
	<cfif>

		<cfquery name="quote_items" datasource="agbcodes_ldi">
			SELECT MAX(id) AS lastid
			FROM quote_items
		</cfquery>

		<cfquery datasource="agbcodes_ldi">
			INSERT INTO quote_item_params (quote_id, item_id, param, param_value)
			VALUES ('#FORM.quote_id#', '#quote_items.lastid#', "#URLEncodedFormat(FORM.parameter_name)#", "#URLEncodedFormat(FORM.parameter_value)#", "#URLEncodedFormat(FORM.parameter_notes)#" )
		</cfquery>

	</cfif>

</cfif>

<!-- IF Common Quote Params Exist -->
<cfif not isDefined("FORM.maintenance_item") OR FORM.maintenance_item eq "">

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO quote_extra_items (quote_id, item_refrence, item_notes, item_price, currency, conversion)
		VALUES ('#FORM.quote_id#', "#URLEncodedFormat(FORM.maintenance_item)#", "#URLEncodedFormat(FORM.maintenance_item_notes)#", '#FORM.price_each#', "#URLEncodedFormat(FORM.currency)#", '#FORM.conversion_rate#' )
	</cfquery>

</cfif>