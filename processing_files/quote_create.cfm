<cfif not isDefined("FORM.company_id")>

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO companies (company_name, email, telephone, mail_1, town, postcode, country, notes)
		VALUES ("#URLEncodedFormat(FORM.company_name)#", "#URLEncodedFormat(FORM.company_email)#", "#URLEncodedFormat(FORM.company_telephone)#", "#URLEncodedFormat(FORM.company_address)#", "#URLEncodedFormat(FORM.company_town)#", "#URLEncodedFormat(FORM.company_postcode)#", "#URLEncodedFormat(FORM.company_country)#", "#URLEncodedFormat(FORM.company_notes)#" )
	</cfquery>

	<cfquery name="company" datasource="agbcodes_ldi">
		SELECT MAX(id) AS lastid
		FROM companies
	</cfquery>

	<cfset company_id = "#company.lastid#">

<cfelse>

	<cfset company_id = "#FORM.company_id#">

</cfif>

<cfif not isDefined("FORM.contact_id") OR FORM.contact_id eq ''>

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO contacts (first_name, last_name, company, email, telephone, staff_role, country_based, notes, company_id)
		VALUES ("#URLEncodedFormat(FORM.first_name)#", "#URLEncodedFormat(FORM.last_name)#", "#URLEncodedFormat(FORM.company_name)#", "#URLEncodedFormat(FORM.contact_email)#", "#URLEncodedFormat(FORM.contact_telephone)#", "#URLEncodedFormat(FORM.staff_role)#", "#URLEncodedFormat(FORM.contact_country_based)#", "#URLEncodedFormat(FORM.contact_notes)#", '#company_id#')
	</cfquery>

	<cfquery name="contact" datasource="agbcodes_ldi">
		SELECT MAX(id) AS lastid
		FROM contacts
	</cfquery>

	<cfset contact_id = "#contact.lastid#">

<cfelse>

	<cfset contact_id = "#FORM.contact_id#">

</cfif>

<cfquery datasource="agbcodes_ldi">
	INSERT INTO quotes (company_id, company_name, contact_id, first_name, last_name, telephone, email, seller)
	VALUES ('#company_id#', "#URLEncodedFormat(FORM.company_name)#", '#contact_id#', "#URLEncodedFormat(FORM.first_name)#", "#URLEncodedFormat(FORM.last_name)#", "#URLEncodedFormat(contact_telephone)#", "#URLEncodedFormat(contact_email)#", "#URLEncodedFormat(FORM.seller)#")
</cfquery>

<cfquery name="quote" datasource="agbcodes_ldi">
	SELECT MAX(id) AS lastid
	FROM quotes
</cfquery>

<cfif isDefined("FORM.seller") AND FORM.seller neq "">

	<cfif FORM.seller eq "Liquid Dynamics International">
	  <cfset formatted_id = "LDI-UK-Q-#quote.lastid#">
	<cfelseif FORM.seller eq "Pulse Gard">
	  <cfset formatted_id = "PG-UK-Q-#quote.lastid#">
	<cfelseif FORM.seller eq "Shock Gard">
	  <cfset formatted_id = "SG-UK-Q-#quote.lastid#">
	<cfelseif FORM.seller eq "Hydrotrole">
	  <cfset formatted_id = "HT-UK-Q-#quote.lastid#">
	</cfif>

</cfif>

<cfquery datasource="agbcodes_ldi">
	INSERT INTO jobs (quote_id, company_id, contact_id, refrence_id)
	VALUES ("#quote.lastid#", "#company_id#", "#contact_id#", "#formatted_id#")
</cfquery>

<cfquery name="job" datasource="agbcodes_ldi">
	SELECT MAX(id) AS lastid
	FROM jobs
</cfquery>

<cfquery datasource="agbcodes_ldi">
	INSERT INTO job_vs_quote (quote_id, job_id)
	VALUES ("#quote.lastid#", "#job.lastid#")
</cfquery>

<cfset relative_path = ExpandPath("../contracts/#formatted_id#")>

<cfif DirectoryExists(relative_path)>

	<h1>Yay it already exists</h1>

<cfelse>

	<cfset DirectoryCreate(relative_path)>

</cfif>

<!-- Inset blank Params and items so they have somthing to fill in asap -->
<cfquery datasource="agbcodes_ldi">
	INSERT INTO quote_params (quote_id)
	VALUES ('#quote.lastid#')
</cfquery>

<cfquery datasource="agbcodes_ldi">
	INSERT INTO quote_items (quote_id)
	VALUES ('#quote.lastid#')
</cfquery>

<cfquery datasource="agbcodes_ldi">
	INSERT INTO quote_extra_items (quote_id)
	VALUES ('#quote.lastid#')
</cfquery>

<cflocation url="../quote_management.cfm?id=#quote.lastid#" addtoken="no">