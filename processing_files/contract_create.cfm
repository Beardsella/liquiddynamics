<cfif isDefined("URL.quote_id") AND URL.quote_id neq "">

    <cfquery name="this_quote" datasource="agbcodes_ldi">
        SELECT *
        FROM quotes
        WHERE id = "#URL.quote_id#"
    </cfquery>

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO works_orders (company_id,
								 company_name,
								 contact_id,
								 first_name,
								 last_name,
								 telephone,
								 email,
								 seller,
								 linked_quote_id)
		VALUES ('#this_quote.company_id#',
				 "#this_quote.company_name#",
				 '#this_quote.contact_id#',
				 "#this_quote.first_name#",
				 "#this_quote.last_name#",
				 "#this_quote.telephone#",
				 "#this_quote.email#",
				 "#this_quote.seller#",
				 "#URL.quote_id#")
	</cfquery>

    <cfquery name="new_wo" datasource="agbcodes_ldi">
        SELECT MAX(id) AS lastid
        FROM works_orders
    </cfquery>

	<cfset selling_company = "#URLDecode(this_quote.seller)#">

	<cfif selling_company eq "Liquid Dynamics International">
		<cfset wo_id = "LDI-UK-WO-#new_wo.lastid#">
		<cfset contract_id = "LDI-UK-#new_wo.lastid#">
	<cfelseif selling_company eq "Pulse Gard">
		<cfset wo_id = "PG-UK-WO-#new_wo.lastid#">
		<cfset contract_id = "PG-UK-#new_wo.lastid#">
	<cfelseif selling_company eq "Shock Gard">
		<cfset wo_id = "SG-UK-WO-#new_wo.lastid#">
		<cfset contract_id = "SG-UK-#new_wo.lastid#">
	<cfelseif selling_company eq "Hydrotrole">
		<cfset wo_id = "HT-UK-WO-#new_wo.lastid#">
		<cfset contract_id = "HT-UK-#new_wo.lastid#">
	</cfif>

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO jobs (quote_id, company_id, contact_id)
		VALUES ("#URL.quote_id#", "#this_quote.company_id#", "#this_quote.contact_id#")
	</cfquery>

	<cfquery name="job" datasource="agbcodes_ldi">
		SELECT MAX(id) AS lastid
		FROM jobs
	</cfquery>

	<cfif this_quote.linked_enq_id NEQ "">
		<cfquery datasource="agbcodes_ldi">
			INSERT INTO job_vs_enq (enquiry_id, job_id)
			VALUES ("#this_quote.linked_enq_id#", "#job.lastid#")
		</cfquery>
	</cfif>

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO job_vs_quote (quote_id, job_id)
		VALUES ("#URL.quote_id#", "#job.lastid#")
	</cfquery>

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO job_vs_works_order (w_order_id, job_id)
		VALUES ("#new_wo.lastid#", "#job.lastid#")
	</cfquery>

	<cfquery datasource="agbcodes_ldi">
		UPDATE works_orders
		SET formatted_id = "#wo_id#"
	    WHERE id = '#new_wo.lastid#' 
	</cfquery>

	<cfquery datasource="agbcodes_ldi">
		UPDATE jobs
		SET formatted_id = "#contract_id#"
	    WHERE id = '#job.lastid#' 
	</cfquery>

	<cflocation url="../contracts_overview.cfm" addtoken="no">

</cfif>