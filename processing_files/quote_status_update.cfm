<cfquery name="this_quote" datasource="agbcodes_ldi">
	SELECT *
	FROM quotes
	WHERE id = "#URL.id#"
</cfquery>

<cfquery datasource="agbcodes_ldi">
	INSERT INTO works_orders (company_id, company_name, contact_id, first_name, last_name, telephone, email, seller)
	VALUES ('#this_quote.company_id#', "#this_quote.company_name#", '#this_quote.contact_id#', "#this_quote.first_name#", "#this_quote.last_name#", "#this_quote.telephone#", "#this_quote.email#", "#this_quote.seller#")
</cfquery>

<cfquery name="new_wo" datasource="agbcodes_ldi">
	SELECT MAX(id) AS lastid
	FROM works_orders
</cfquery>

<cfquery datasource="agbcodes_ldi">
	INSERT INTO job_vs_works_order (job_id, w_order_id)
	VALUES ("#URL.job#", "#new_wo.lastid#")
</cfquery>

<cfquery datasource="agbcodes_ldi">
    UPDATE quotes
    SET q_status = "#URLEncodedFormat(URL.status)#",
    linked_wo_id = "#new_wo.lastid#"
    WHERE id = #URL.id#
</cfquery>

<cflocation url="../contract_details.cfm?id=#URL.job#" addtoken="no">