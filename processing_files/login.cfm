<!-- check there password is correct -->
<cfif IsDefined ("FORM.inputPassword") AND FORM.inputPassword neq "">

  <cfquery name="verifyuser" datasource="agbcodes_ldi">
    SELECT username, slevel, handshake, id
    FROM a_team
    WHERE username = "#FORM.inputUsername#"
  </cfquery>

    <!-- First Check their password -->
    <cfif FORM.inputPassword eq "#verifyuser.handshake#">    

		<!-- Set Session Vars -->
		<cfset session.username = '#verifyuser.username#'>
		<cfset session.slevel = '#verifyuser.slevel#'>
		<cfset session.userid = '#verifyuser.id#'>
		<cfset this.sessionTimeout = createTimeSpan( 0, 0, 30, 1 ) />

		<!--  write away login time  -->
		<cfquery datasource="agbcodes_ldi">
			INSERT INTO login_log (login_by, login_ip, logged_in_out) 
			VALUES ('#session.username#', '#CGI.REMOTE_ADDR#', 'IN')
		</cfquery>

	    <cflocation url="../admin/customer-enquiry-overview.cfm" addtoken="no">
      
    <cfelse>

		<!--  write away reject time  -->
		<cfquery datasource="agbcodes_ldi">
			INSERT INTO login_log (login_by, login_ip, logged_in_out) 
			VALUES ('#FORM.inputUsername#', '#CGI.REMOTE_ADDR#', 'Rejected - user + pass combo inncorect')
		</cfquery>

		<cflocation url="../index.cfm?action=wrong_pw" addtoken="no">

    </cfif>

<cfelse>

  <cflocation url="../index.cfm?action=no_pw" addtoken="no">

</cfif>