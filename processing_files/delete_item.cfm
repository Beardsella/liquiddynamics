<cfif IsDefined("URL.del_id") AND URL.del_id NEQ "">

	<cfquery name="query_cart">
		SELECT quantity
		FROM cart_lines
		WHERE id = #URL.del_id#
	</cfquery>

	<cfquery>
		DELETE 
		FROM cart_lines
		WHERE id = #URL.del_id#
	</cfquery>

	<cflock timeout=30 scope="Session" type="Exclusive"> 
		<cfset session.shoppingcartitems = #session.shoppingcartitems# - #query_cart.quantity#>
	</cflock>


	<cflocation url="#CGI.http_referer#?success=1" addtoken="false">

<cfelse>

	<cflocation url="#CGI.http_referer#?error=1" addtoken="false">

</cfif>

<cfdump var="#session#">