<!DOCTYPE html>
<html lang="en">
  <head>
    <link href="css/bootstrap.min.css" rel="stylesheet" content-type="text/css">
    <link href="css/signin.css" rel="stylesheet" content-type="text/css">
    <link rel="icon" href="images/favicon.ico">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    
    <link rel="icon" href="favicon.ico">

    <title>Liquid Dynamics Login</title>

  <!--Admin Login check -->
  <cfif IsDefined ("session.slevel") AND session.slevel lt 5>

      <cflocation url="admin/index.cfm" addtoken="no">

  </cfif>
  <!-- End of admin login check-->

  </head>

  <body>
  
  <div class="container">

      <div class="alert alert-danger col-sm-12 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4 col-xl-offset-4 col-xl-4" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <cfoutput>
          <h3>#URL.action#</h3>
        </cfoutput>
        <p>Please check that your username and password are correct</p>
      </div>

    <cfparam name="session.session_message_shown" default="0" >
    <cfif IsDefined ("url.session_expired") AND '#session.session_message_shown#' eq 0>
      <div class="alert alert-warning col-sm-12 col-md-offset-4 col-md-4 col-lg-offset-4 col-lg-4 col-xl-offset-4 col-xl-4" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3>Session Expired</h3>
        <p>You've been logged out.</p>
      </div>
      <cfset session.session_message_shown = 1 >
    </cfif>

      <div class="clearfix"></div>

      <form class="form-signin" action="processing_files/login.cfm" method="post" enctype="multipart/form-data">
        <h2 class="form-signin-heading text-center">Please sign in</h2>
        <label for="inputUsername" class="sr-only">Email address</label>
        <input type="text" name="inputUsername" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Password" required>
<!--         <p class="text-center"><a href="password_retrieval.cfm">Forgotton your password?</a><br>
        <a href="password_retrieval.cfm">Register a new account</a></p>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div> -->
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
    </div> <!-- /container -->

  </body>
</html>