<!DOCTYPE html>
<html lang="en">
<head>

  <cfinclude template="includes/default_header.cfm">

  <title>Customer Enquiry - Liquid Dynamics Group Ltd</title>

  <!-- CSS for autocomplete -->
  <link rel="stylesheet" href="css/jquery-ui-1.12.1.css">

  <cfquery name="pumps" datasource="agbcodes_ldi">
      SELECT *
      FROM wip_pumps
  </cfquery>

  <cfquery name="metals" datasource="agbcodes_ldi">
      SELECT *
      FROM wip_metals
  </cfquery>

  <cfquery name="plastics" datasource="agbcodes_ldi">
      SELECT *
      FROM wip_plastics
  </cfquery>

</head>

<body>

  <cfinclude template="includes/nav.cfm">

  <!-- Page Content -->
  <div class="container">

  <div class="col-12">
      <h1 class="page-header" id="content">Customer Enquiry</h1>
      <p class="lead">This information will be used to help determine the correct damper for your system. <strong>If you cannot answer all the questions below or are unsure then please contact us or leave blank and we will contact you and assist.</strong> <a href="product_photos/Slurry-hose-dampener.gif">Example gif</a> to demonstrate.</p>
  </div>

  <form class="form" action="processing_files/enquirys_pump_systems_insert.cfm" method="post" enctype="multipart/form-data">


        <h3>Contact Details</h3>
        <hr>
    
        <div class="row">
           
            <div class="col-lg-6">
              <div class="form-group">
              <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Joe">
                </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
              <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Bloggs">
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="enquiry_email">Email Address</label>
              <input type="email" class="form-control" id="enquiry_email" name="enquiry_email" placeholder="Email Address">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_telephone">Phone Number</label>
              <input type="text" class="form-control" id="contact_telephone" name="contact_telephone" placeholder="Phone Number">
              </div>
            </div>
        </div> 

          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#contact_details" aria-expanded="false" aria-controls="contact_details">
            More Contact Details...
          </button>
        </p>
        <div class="collapse" id="contact_details">
        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="staff_role">Staff Role</label>
              <input type="text" class="form-control" id="staff_role" name="staff_role" placeholder="Purchasing Manager">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_country_based">Country Based</label>
              <input type="text" class="form-control" id="contact_country_based" name="contact_country_based" placeholder="UK">
              </div>
            </div>
        </div> 

        <div class="form-group">
        <label for="contact_notes">Contact Notes</label>
          <textarea class="form-control" id="contact_notes" name="contact_notes"></textarea>
        </div>

        </div>

        <h3>Company Details</h3>
        <hr>

            <div class="form-group">
            <label for="company">Company Name</label>
              <input type="text" class="form-control" id="company" name="company" placeholder="Company">
            </div>

          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#company_details" aria-expanded="false" aria-controls="company_details">More Company Details...</button>
        
        <div class="collapse" id="company_details">

        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="company_email">Email Address</label>
              <input type="email" class="form-control" id="company_email" name="company_email" placeholder="Email Address">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="company_telephone">Phone Number</label>
              <input type="text" class="form-control" id="company_telephone" name="company_telephone" placeholder="Phone Number">
              </div>
            </div>
        </div> 

        <div class="row">
            <div class="col-lg-12">
            <div class="form-group">
            <label for="company_address">Address</label>
              <input type="text" class="form-control" id="company_address" name="company_address" placeholder="Email Address">
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
              <label for="company_town">Town</label>
                <input type="text" class="form-control" id="company_town" name="company_town" placeholder="Town">
                </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
              <label for="company_postcode">Post Code</label>
                <input type="text" class="form-control" id="company_postcode" name="company_postcode" placeholder="Post Code">
                </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
              <label for="company_country">Country</label>
                <input type="text" class="form-control" id="company_country" name="company_country" placeholder="Country">
                </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
              <label for="company_notes">Company Notes</label>
                <textarea class="form-control" id="company_notes" name="company_notes"></textarea>
              </div>
            </div>

        </div> 

        </div>

        <h3>Enquiry Details</h3>
        <hr>

        <div class="form-group">
           <label for="problem">Type of Problem?</label>
            <select id="problem" name="problem" class="form-control">
              <option value="Other / Not Sure">Other / Not Sure</option>
              <option value="Plusation Suction">Plusation Suction</option>
              <option value="Plusation Discharge">Plusation Discharge</option>
              <option value="Frequency">Frequency</option>
              <option value="Thermal Expansion">Thermal Expansion</option>
              <option value="Accumulation">Accumulation</option>
              <option value="Shock Alleviation / Water Hammer">Shock Alleviation / Water Hammer</option>
            </select>
        </div>

        <div class="form-group">
           <label for="pump_type">Type of Pump</label>
            <select id="pump_type" name="pump_type" class="form-control">
              <option value="Other">Other</option>
              <option value="Centrifugal Pumps">Centrifugal Pumps</option>
              <option value="AODD">AODD</option>
              <option value="Hose Pump">Hose Pump</option>
              <option value="Gear Pump">Gear Pump</option>
              <option value="Vane Pump">Vane Pump</option>
              <option value="Packed Plunger Pump">Packed Plunger Pump</option>
              <option value="Metering Pump">Metering Pump</option>
              <option value="Progressive Cavity">Progressive Cavity</option>
              <option value="Power Pump">Power Pump</option>
            </select>
        </div>
        
        <!-- Number of Displacers (Simplex, Duplex, Triplex etc) -->
        <div class="form-group">
           <label for="num_of_displacers">Number of Displacers</label>
            <select id="num_of_displacers" name="num_of_displacers" class="form-control">
              <option value=""></option>
              <option value="Simplex">1 - (Simplex)</option>
              <option value="Duplex">2 - (Duplex)</option>
              <option value="Triplex">3 - (Triplex)</option>
              <option value="Other">Other</option>
            </select>
        </div>
        
        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="flow_rate">Flow Rate</label>
                  <input type="number" class="form-control" id="flow_rate" name="flow_rate" placeholder="485">
            </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="flow_rate_units">Flow Rate Units</label>
                <select id="flow_rate_units" name="flow_rate_units" class="form-control">
                  <option value=""></option>
                  <option value="Litres per minute">Litres per minute</option>
                  <option value="Litres per hour">Litres per hour</option>
                </select>
            </div>
            </div>
          </div>

          <div class="form-row">
          <div class="form-group">
           <label for="SPMorRPM">SPM (strokes per minute) or RPM (revs per minute)</label>
            <select id="SPMorRPM" name="SPMorRPM" class="form-control">
              <option value=""></option>
              <option value="SPM">SPM</option>
              <option value="RPM">RPM</option>
            </select>
        </div>
        </div>


        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#stroke_displacement_box" aria-expanded="false" aria-controls="stroke_displacement_box">Please click here if you don't know your SPM  / RPM</button>

        <div class="collapse" id="stroke_displacement_box">
          <div class="form-group">
            <label for="stroke_displacement">What is you displacement per stroke?</label>
            <input type="text" class="form-control" id="stroke_displacement" name="stroke_displacement">
          </div>     
          <hr>   
        </div>
        
        <!-- Liquid being pumped (water, diesel, slurry etc) -->
        <div class="form-group">
            <label for="pumped_liquid">Liquid being pumped</label>
            <input type="text" class="form-control" id="pumped_liquid" name="pumped_liquid">
        </div>  

        <!-- System Operating Temperature -->
        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="system_temp">System Operating Temperature</label>
                  <input type="number" class="form-control" id="system_temp" name="system_temp" placeholder="35">
            </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="system_temp_units">System Operating Units</label>
                <select id="system_temp_units" name="system_temp_units" class="form-control">
                  <option value="Degrees Celsius">Degrees Celsius</option>
                  <option value="Fahrenheit">Fahrenheit</option>
                </select>
            </div>
            </div>
          </div>

        <!-- System Design Temperature -->
        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="system_design_temp">System Design Temperature</label>
                  <input type="number" class="form-control" id="system_design_temp" name="system_design_temp" placeholder="80">
            </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="system_design_temp_units">System Design Units</label>
                <select id="system_design_temp_units" name="system_design_temp_units" class="form-control">
                  <option value="Degrees Celsius">Degrees Celsius</option>
                  <option value="Fahrenheit">Fahrenheit</option>
                </select>
            </div>
            </div>
          </div>
        
        <!-- System Maximum Working Pressure -->
        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="max_system_pressure">System Maximum Working Pressure</label>
                  <input type="number" class="form-control" id="max_system_pressure" name="max_system_pressure" placeholder="80">
            </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="max_system_pressure_units">System Maximum Working Pressure Units</label>
                <select id="max_system_pressure_units" name="max_system_pressure_units" class="form-control">
                  <option value="PSI">PSI</option>
                  <option value="BAR">BAR</option>
                </select>
            </div>
            </div>
          </div>
        
        <!-- System Design Pressure -->
        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="system_design_pressure">System Design Pressure</label>
                  <input type="number" class="form-control" id="system_design_pressure" name="system_design_pressure" placeholder="40">
            </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="system_design_pressure_units">System Design Pressure Units</label>
                <select id="system_design_pressure_units" name="system_design_pressure_units" class="form-control">
                  <option value="PSI">PSI</option>
                  <option value="BAR">BAR</option>
                </select>
            </div>
            </div>
          </div>

        <!-- Pipework Size -->
        <div class="form-group">
          <label for="pipe_size">Pipework Size</label>
          <input type="text" class="form-control" id="pipe_size" name="pipe_size">
        </div>
        
        <!-- Connection Type Required (Flange, NPT, Tri Clamp etc) -->
        <div class="form-group">
           <label for="connection_type">Connection Type Required</label>
            <select id="connection_type" name="connection_type" class="form-control">
              <option value=""></option>
              <option value="Flange">Flange</option>
              <option value="NPT">NPT</option>
              <option value="BSP">BSP</option>
              <option value="Tri Clamp">Tri Clamp</option>
              <option value="Other">Other</option>
            </select>
        </div>

        <!-- Single Connection or Twin Flow-Through Required -->
        <div class="form-group">
           <label for="connection_type_2">Single Connection or Twin Flow-Through Required</label>
            <select id="connection_type_2" name="connection_type_2" class="form-control">
              <option value=""></option>
              <option value="Single Connection">Single Connection</option>
              <option value="Twin Flow-Through">Twin Flow-Through</option>
            </select>
        </div>

        <div class="form-group">
            <label for="metalic_system_materials">MOC - Metalics</label>
            <button type="button" class="btn btn-link pull-right" data-toggle="popover" data-placement="left" title="Materials of Construction - Metalics" data-content="Please enter whatever metalic materials you need here">What does this mean?</button>
            <input type="text" class="form-control" id="metalic_system_materials" name="metalic_system_materials">
        </div>  

        <div class="form-group">
            <label for="plastic_system_materials">MOC - Elastomer</label>
            <button type="button" class="btn btn-link pull-right" data-toggle="popover" data-placement="left" title="Materials of Construction - Elastomer" data-content="Please enter whatever material you need for the Elastomer">What does this mean?</button>
            <input type="text" class="form-control" id="plastic_system_materials" name="plastic_system_materials">
        </div>  

        <h3>Enquiry Action</h3>
        <hr>

        <!-- Connection Type Required (Flange, NPT, Tri Clamp etc) -->
        <div class="form-group">
           <label for="actioned">Enquiry Status</label>
            <select id="actioned" name="actioned" class="form-control">
              <option value="0">New</option>
              <option value="3">Wating for Client</option>
              <option value="2">In Progress</option>
              <option value="4">Completed</option>
            </select>
        </div>

        <div class="form-group">
          <label>Action Date</label>
            <input type="text" id="action_date" name="action_date" class="form-control" placeholder="A reminder date to action the next step of their query">
        </div>

        <div class="form-group">
        <label for="enquiry_notes">Enquiry Notes</label>
          <textarea class="form-control" id="enquiry_notes" name="enquiry_notes"></textarea>
        </div>

        <button type="submit" class="btn btn-success btn-block">Save Enquiry</button>

      </form>

    </div>
    <!-- End of container -->

    <hr>

  <cfinclude template="includes/footer.cfm">
  <cfinclude template="includes/java_include.cfm">

  <!-- JS for autocomplete -->
  <script src="js/jquery-ui-1.12.1.js"></script>
  <script>
  $( function() {
    var availableTags1 = [
      "Stainless Steel",
      "Duplex",
      "Carbon Steel"
    ];
    $( "#metalic_system_materials" ).autocomplete({
      source: availableTags1
    });
  } );

    $( function() {
    var availableTags2 = [
      "Nitrile",
      "EPDM",
      "Viton"
    ];
    $( "#plastic_system_materials" ).autocomplete({
      source: availableTags2
    });
  } );

$(function() {
        $( "#action_date" ).datepicker({
            dateFormat : 'dd/mm/yy',
            changeMonth : true,
            changeYear : true,
        });
    }); 

$(function () {
  $('[data-toggle="popover"]').popover()
})
     </script>
  <!-- End of autocomplete JS -->
  
</body>
</html>