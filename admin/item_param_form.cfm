<!-- Form Name -->
<h3>Item #CurrentRow# Parameters - Item ID ###id#</h3>
<hr>

<cfparam name="this_quote_item_params.param" default="">
<cfparam name="this_quote_item_params.param_value" default="">
<cfparam name="this_quote_item_params.param_notes" default="">


<div class="row">

  <!-- Text input-->
  <div class="col-md-6 form-group">
    <label class="control-label" for="item_parameter_name">Parameter Name</label>  
    <div class="">
      <input id="item_parameter_name" name="item_parameter_name" type="text" placeholder="Working Pressure" class="form-control input-md" value="#URLDecode(this_quote_item_params.param)#">
    </div>
  </div>

  <!-- Text input-->
  <div class="col-md-6 form-group">
    <label class="control-label" for="item_parameter_value">Parameter Value</label>  
    <div class="">
    <input id="item_parameter_value" name="item_parameter_value" type="text" placeholder="100 bar" class="form-control input-md" value="#URLDecode(this_quote_item_params.param_value)#">
      
    </div>
  </div>

</div>

<div class="row">
  <!-- Textarea -->
  <div class="form-group col-md-12">
    <label class="control-label" for="item_parameter_notes">Parameter Notes</label>
    <div class="">                     
      <textarea class="form-control" id="item_parameter_notes" name="item_parameter_notes">#URLDecode(this_quote_item_params.param_notes)#</textarea>
    </div>
  </div>
</div>

<div class="row">
  <button type="submit" class="btn btn-primary btn-block">Add more pramerters for this item #CurrentRow#.</button>
</div>