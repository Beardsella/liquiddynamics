<cfquery name="this_job" datasource="agbcodes_ldi">
  SELECT *
  FROM jobs
  WHERE quote_id = "#URL.id#"
</cfquery>

<cfset selling_company = "#URLDecode(this_quote.seller)#">

<cfif selling_company eq "Liquid Dynamics International">
  <cfset file_name_prep = "LDI-UK-Quote-#this_quote.id#">
  <cfset formated_id = "LDI-UK-Q-#this_quote.id#">
<cfelseif selling_company eq "Pulse Gard">
  <cfset file_name_prep = "PG-UK-Quote-#this_quote.id#">
  <cfset formated_id = "PG-UK-Q-#this_quote.id#">
<cfelseif selling_company eq "Shock Gard">
  <cfset file_name_prep = "SG-UK-Quote-#this_quote.id#">
  <cfset formated_id = "SG-UK-Q-#this_quote.id#">
<cfelseif selling_company eq "Hydrotrole">
  <cfset file_name_prep = "HT-UK-Quote-#this_quote.id#">
  <cfset formated_id = "HT-UK-Q-#this_quote.id#">
</cfif>

<cfif this_quote_version EQ '1'>
  <cfset file_name =  "#file_name_prep#.pdf">
<cfelse>
  <cfset file_name =  "#file_name_prep#-Ver-#this_quote_version#.pdf">
</cfif>

<cfset file_path = "contracts/#this_job.formatted_id#/">
<cfset relative_path = ExpandPath("contracts/#this_job.formatted_id#/")>

<cfdocument
    format = "PDF"
    backgroundVisible = "no"
    bookmark = "no"
    overwrite = "yes"
    encryption = "none"
    filename = "#relative_path##file_name#"
    fontEmbed = "true"
    localUrl = "true"
    pagetype="a4"> 

 
<cfdocumentitem type="footer" evalatprint="true"> 
<table width="100%" border="0" cellpadding="0" cellspacing="0"> 
<tr><td align="center"><cfoutput>#cfdocument.currentpagenumber# of 
#cfdocument.totalpagecount#</cfoutput></td></tr> 
</table> 
</cfdocumentitem> 

<cfoutput query="this_quote">

  <cfdocumentsection> 

    <table style="width:100%;">
      
      <tr>
        <td colspan="4" style="text-align:center;"><h1>Quotation</h1></td>
      </tr>

      <tr>
        <td>&nbsp;</td>
      </tr>

      <tr>
        <td>Customer</td>
        <td>#URLDecode(this_quote.company_name)#</td>
        <td>Date</td>
        <td>#DateFormat(quote_dated, "DD-MMM-YYYY")#</td>
      </tr>

      <tr>
        <td>Attention Of</td>
        <td>#URLDecode(this_quote.first_name)# #URLDecode(this_quote.last_name)#</td>
        <td>Customer Ref</td>
        <td>#URLDecode(this_quote.cust_enquiry_ref)#</td>
      </tr>

      <tr>
        <td>From</td>
        <td>#URLDecode(this_quote.seller)#</td>
        <td>Quote No</td>
        <td>#formated_id#</td>
      </tr>

    </table>

    <div><p>#URLDecode(this_quote.quote_intro)#</p></div>

    <hr>

    <cfquery name="quote_params" datasource="agbcodes_ldi">
      SELECT *
      FROM quote_params
      WHERE quote_id = '#this_quote.id#'
    </cfquery>

    <cfif quote_params.recordcount gte '1'>

      <h3>Common Parameters</h3>

      <table style="width:100%;">

        <cfloop query="quote_params">

            <tr>
              <td>#URLDecode(param)#:</td>
              <td>#URLDecode(param_value)#</td>
            </tr>      
            <tr>
              <td>#URLDecode(param_notes)#</td>
            </tr>

        </cfloop>

      </table>

      <hr>

    </cfif>

  </cfdocumentsection>

  <cfquery name="quote_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_items
    WHERE quote_id = '#this_quote.id#'
  </cfquery>

  <cfloop query="quote_items">

    <cfdocumentsection> 

      <cfif quote_items.recordcount gte '1'>

        <h3>Quote Items: #quote_items.RecordCount#</h3>

        <h4>Item ###quote_items.currentRow# - "#URLDecode(quote_items.item_name)#"</h4>

        <p>Item Qty: #quote_items.item_qty#</p>

        <table style="width:100%; text-align:center;">

          <tr>
            <td></td>
            <td>Preventer Type</td>
            <td>Volume</td>
            <td>Membrane</td>
            <td>Pressure</td>
            <td>Connection</td>
            <td>Wetted Material</td>
            <td>Miscellaneous</td>
          </tr>

          <tr>
            <td>Unit Refrence:</td>
            <td>#URLDecode(quote_items.preventer_type)#</td>
            <td>#URLDecode(quote_items.volume)#</td>
            <td>#URLDecode(quote_items.membrane)#</td>
            <td>#URLDecode(quote_items.pressure)#</td>
            <td>#URLDecode(quote_items.connection)#</td>
            <td>#URLDecode(quote_items.wetted_material)#</td>
            <td>#URLDecode(quote_items.miscellaneous)#</td>
          </tr>

        </table>

        <cfquery name="quote_item_params" datasource="agbcodes_ldi">
          SELECT *
          FROM quote_item_params
          WHERE quote_id = '#this_quote.id#' AND item_id = #quote_items.id#
        </cfquery>

        <cfloop query="quote_item_params">

          <table style="width:100%;">
            
            <tr>
              <th colspan="2">Pump Parameters</th>
              <th colspan="2">System Information</th>
            </tr>

            
            <tr>
              <td>Type of Pump</td>
              <td></td>
              <td>Fluid</td>
              <td></td>
            </tr>

            
            <tr>
              <td>No. of displacers</td>
              <td></td>
              <td>Max operating temp</td>
              <td></td>
            </tr>

            
            <tr>
              <td>Piston diameter</td>
              <td></td>
              <td>Degree of damping %</td>
              <td></td>
            </tr>

            
            <tr>
              <td>Stroke length</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>

          </table>

        </cfloop>

      </cfif>

    </cfdocumentsection> 

  </cfloop>


  <cfdocumentsection> 

    <h5>Terms of Payment</h5>
    <p>#URLDecode(this_quote.terms_of_payment)#</p>

    <p>This quotation is valid untill (#DateFormat(quote_vaild_for, "DD-MMM-YYYY")#)</p>

    <h3>#URLDecode(this_quote.notes)#</h3>

    <div>#URLDecode(this_quote.summary)#</div>

    <p>Best Regards<br><br><br><br>
    For and on behalf of<br>
    #URLDecode(this_quote.seller)# Limited</p>

  </cfdocumentsection>

</cfoutput>

</cfdocument>