<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <title>Liquid Dynamics Group Ltd</title>

    <cfquery name="all_jobs" datasource="agbcodes_ldi">
        SELECT *
        FROM jobs
    </cfquery>  

    <cfquery name="countries" datasource="agbcodes_ldi">
        SELECT *
        FROM countries
    </cfquery>

    <cfquery name="enquiries" datasource="agbcodes_ldi">
        SELECT enquirys_pump_systems.*
        FROM enquirys_pump_systems
        RIGHT JOIN job_vs_enq 
        ON enquirys_pump_systems.id = job_vs_enq.enquiry_id
        ORDER BY enquirys_pump_systems.enquiry_date
    </cfquery>

    <cfquery name="quotes" datasource="agbcodes_ldi">
        SELECT *
        FROM quotes
    </cfquery>

    <cfquery name="w_orders" datasource="agbcodes_ldi">
        SELECT *
        FROM works_orders
    </cfquery>

    <cfquery name="invoices" datasource="agbcodes_ldi">
        SELECT *
        FROM invoices
    </cfquery>

    <cfquery name="uploaded_docs" datasource="agbcodes_ldi">
        SELECT uploaded_docs.*
        FROM uploaded_docs
        RIGHT JOIN job_vs_docs 
        ON uploaded_docs.id = job_vs_docs.doc_id
    </cfquery>

</head>
<body>

    <cfinclude template="includes/nav.cfm">

      <!-- Page Content -->
      <main role="main" class="container-fluid">

            <cfoutput>
        

                <div class="col-lg-12">
                    <h1 class="page-header">Current Work Overview</h1>
                    <p class="lead">An overview of active work. Currently <span class="badge badge-light">#all_jobs.RecordCount#</span> active contracts</p>



</cfoutput>

    <cfinclude template="contracts_linked.cfm">
                    


      </main>

    <hr>
    <!-- End of page main content -->

    <cfinclude template="includes/footer.cfm">
    <cfinclude template="includes/java_include.cfm">

</body>
</html>