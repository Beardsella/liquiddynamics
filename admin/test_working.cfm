<!DOCTYPE html>
<html lang="en">
<head>

  <cfinclude template="includes/default_header.cfm">

  <cfquery name="this_quote" datasource="agbcodes_ldi">
    SELECT *
    FROM quotes
    WHERE id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quote_params" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_params
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quotes_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_items
    WHERE quote_id = "#URL.id#"
  </cfquery>
  
  <cfquery name="this_quote_extra_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_extra_items
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfset quote_date_formated = "#DateFormat(this_quote.quote_dated, 'dd/mm/yyyy')#">
  <cfset dispatch_date_formated = "#DateFormat(this_quote.dispatch_estimate, 'dd/mm/yyyy')#">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



<script type="text/javascript" src="js/html2canvas.js"></script>
  <script type="text/javascript" src="js/jspdf.min.js"></script>

  <script type="text/javascript">
  function genPDF()
  {
html2canvas($("body"), {
onclone: function(document) {
if (options.elementsToIgnore.length) {
for (var i = 0; i < options.elementsToIgnore.length; i++) {
$(document).find(options.elementsToIgnore[i]).remove();
}
}
}}).then(function(canvas) {
var imgData = canvas.toDataURL('image/png');
var doc = new jsPDF('p', 'pt', 'a4');
doc.addImage(imgData, 'PNG', 0, 0);
doc.save('sample-file.pdf'); //creates a pdf
}).catch(function(error) {
done(error);
});

  }
</script>

</head>
<body>

  <div id="ignoreContent">

    <p>Once your happy with your PDF preview click the button below to generate you PDF</p>

    <a href="javascript:genPDF()" class="btn btn-success">Generate PDF</a>

  </div>

<cfoutput query="this_quote">

  <div id="PDFcontent">

    <div class="row">

      <div class="col-6">
        <h3>ID## = LDI-UK-#numberFormat(this_quote.id,'000000')#</h3>
        <h3>ID## = PGL-UK-#numberFormat(this_quote.id,'000000')#</h3>
        <p class="lead">#URLDecode(cust_enquiry_ref)#</p>
      </div>

      <div class="col-6 text-right">
        <p class="lead">#quote_dated#</p>
      </div>

    </div>

    <p class="lead">#URLDecode(first_name)# #URLDecode(last_name)#</p>
    <p>#URLDecode(quote_intro)#</p>


    <div class="row">

      <div class="col-md-12 form-group">
        <label class="control-label" for="quote_dated">Quote Date</label>  
        <input type="text" id="quote_dated" name="quote_dated" class="form-control" placeholder="What date will show up on the quote" value="#quote_date_formated#">
      </div>

    </div>

    <div class="row">
      
      <!-- Textarea -->
      <div class="col-md-12 form-group">
        <label class="control-label" for="notes">Notes</label>
        <div class="">                     
          <textarea class="form-control" id="notes" name="notes" placeholder="Unit design...">#URLDecode(notes)#</textarea>
        </div>
      </div>  

    </div>

  </div>

</cfoutput>

</body>
</html>