<cfparam name="this_quote_params.param" default="">
<cfparam name="this_quote_params.param_value" default="">
<cfparam name="this_quote_params.param_notes" default="">


<div class="row mt-5">

  <div class="col-6">
    <cfoutput>
      <h3>Common Job Parameters <span class="badge badge-primary">#this_quote_params.recordcount#</span></h3>
    </cfoutput>
  </div> 

  <div class="col-6">
    
    <!-- New Common Job Parameter Modal TRIGGER -->
    <button class="btn btn-outline-success float-right mr-2" type="button" data-toggle="modal" data-target="#new_common_param_modal">Add a common job Parameter</button>

    <!-- Expand existing Common Job Parameters TRIGGER -->
    <button class="btn btn-outline-info float-right mr-2" type="button" data-toggle="collapse" data-target="#common_job_params_div" aria-expanded="false" aria-controls="common_job_params_div">Expand Common Job Parameters</button>
  </div>

</div>

<div class="clearfix"></div>
<hr>

<div class="collapse" id="common_job_params_div">

    <cfset common_params = '0'>

  <cfoutput query="this_quote_params">

    <cfset common_params = '#common_params#' + '1'>

    <div class="row">
      <div class="col-9">
        <h4><span class="badge badge-warning">Common Parameter ###CurrentRow#</span></h4>
      </div>
      <div class="col-3 text-right">
        <h6><a href="processing_files/quote_delete_common_param.cfm?id=#id#" class="text-danger">Delete Parameter ###CurrentRow#</a></h6>
      </div>
    </div>

    <input type="hidden" name="job_param_id_#CurrentRow#" id="job_param_id_#CurrentRow#" value="#id#">

    <div class="row">

      <!-- Text input-->
      <div class="col-md-6 form-group">
        <label class="control-label" for="job_parameter_name">Parameter Name</label>  
        <div class="">
          <input id="job_parameter_name_#CurrentRow#" name="job_parameter_name_#CurrentRow#" type="text" placeholder="Working Pressure" class="form-control input-md" value="#URLDecode(param)#">
        </div>
      </div>

      <!-- Text input-->
      <div class="col-md-6 form-group">
        <label class="control-label" for="job_parameter_value">Parameter Value</label>  
        <div class="">
          <input id="job_parameter_value_#CurrentRow#" name="job_parameter_value_#CurrentRow#" type="text" placeholder="100 bar" class="form-control input-md" value="#URLDecode(param_value)#">
        </div>
      </div>

    </div>

    <div class="row">

      <!-- Textarea -->
      <div class="col-md-12 form-group">
        <label class="control-label" for="job_parameter_notes">Parameter Notes</label>
        <div class="">                     
          <textarea class="form-control" id="job_parameter_notes_#CurrentRow#" name="job_parameter_notes_#CurrentRow#">#URLDecode(this_quote_params.param_notes)#</textarea>
        </div>
      </div>

    </div>

    <hr>

  </cfoutput>

  <cfif common_params EQ '0'>
    <cfinclude template="common_quote_param_form_blank.cfm">
  </cfif>

</div>