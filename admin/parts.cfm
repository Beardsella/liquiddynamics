<!DOCTYPE html>
<html lang="en">
<head>

  <cfinclude template="includes/default_header.cfm">

  <title>Liquid Dynamics</title>

<cfif isDefined("FORM.preventer_type") OR isDefined("FORM.volume") OR isDefined("FORM.membrane") OR isDefined("FORM.PRESS") OR isDefined("FORM.part_connection") OR isDefined("FORM.soild_material") OR isDefined("FORM.OTHER")>


<cfset next_clause = "WHERE">
  
<cfif isDefined("FORM.fuzzy_search_1")>
  <cfset qualifer_1 = "like">
  <cfset fuzzy_markers_1 = "%">
<cfelse>
  <cfset qualifer_1 = "=">
  <cfset fuzzy_markers_1 = "">
</cfif>  
<cfif isDefined("FORM.fuzzy_search_2")>
  <cfset qualifer_2 = "like">
  <cfset fuzzy_markers_2 = "%">
<cfelse>
  <cfset qualifer_2 = "=">
  <cfset fuzzy_markers_2 = "">
</cfif>  
<cfif isDefined("FORM.fuzzy_search_3")>
  <cfset qualifer_3 = "like">
  <cfset fuzzy_markers_3 = "%">
<cfelse>
  <cfset qualifer_3 = "=">
  <cfset fuzzy_markers_3 = "">
</cfif>  
<cfif isDefined("FORM.fuzzy_search_4")>
  <cfset qualifer_4 = "like">
  <cfset fuzzy_markers_4 = "%">
<cfelse>
  <cfset qualifer_4 = "=">
  <cfset fuzzy_markers_4 = "">
</cfif>  
<cfif isDefined("FORM.fuzzy_search_5")>
  <cfset qualifer_5 = "like">
  <cfset fuzzy_markers_5 = "%">
<cfelse>
  <cfset qualifer_5 = "=">
  <cfset fuzzy_markers_5 = "">
</cfif>  
<cfif isDefined("FORM.fuzzy_search_6")>
  <cfset qualifer_6 = "like">
  <cfset fuzzy_markers_6 = "%">
<cfelse>
  <cfset qualifer_6 = "=">
  <cfset fuzzy_markers_6 = "">
</cfif>  
<cfif isDefined("FORM.fuzzy_search_7")>
  <cfset qualifer_7 = "like">
  <cfset fuzzy_markers_7 = "%">
<cfelse>
  <cfset qualifer_7 = "=">
  <cfset fuzzy_markers_7 = "">
</cfif>

  <cfquery name="diffrent_parts" datasource="agbcodes_ldi">
      SELECT *
      FROM alpha4

      <cfif FORM.preventer_type IS NOT "">
        #next_clause#  preventer_type #qualifer_1# "#fuzzy_markers_1##FORM.preventer_type##fuzzy_markers_1#"
        <cfset next_clause = "AND">
      </cfif>

      <cfif FORM.volume IS NOT "">
          #next_clause# volume #qualifer_2# "#fuzzy_markers_2##FORM.volume##fuzzy_markers_2#"
          <cfset next_clause = "AND">
      </cfif>


      <cfif FORM.membrane IS NOT "">
          #next_clause# membrane #qualifer_3# "#fuzzy_markers_3##FORM.membrane##fuzzy_markers_3#"
          <cfset next_clause = "AND">
      </cfif>


      <cfif FORM.PRESS IS NOT "">
          #next_clause# PRESS #qualifer_4# "#fuzzy_markers_4##FORM.PRESS##fuzzy_markers_4#"
          <cfset next_clause = "AND">
      </cfif>


      <cfif FORM.part_connection IS NOT "">
          #next_clause# connection #qualifer_5# "#fuzzy_markers_5##FORM.part_connection##fuzzy_markers_5#"
          <cfset next_clause = "AND">
      </cfif>


      <cfif FORM.soild_material IS NOT "">
          #next_clause# soild_material #qualifer_6# "#fuzzy_markers_6##FORM.soild_material##fuzzy_markers_6#"
          <cfset next_clause = "AND">
      </cfif>


      <cfif FORM.OTHER IS NOT "">
          #next_clause# OTHER #qualifer_7# "#fuzzy_markers_7##FORM.OTHER##fuzzy_markers_7#"
          <cfset next_clause = "AND">
      </cfif>

      ORDER BY id DESC
  </cfquery>

<cfelse>

  <cfquery name="diffrent_parts" datasource="agbcodes_ldi">
      SELECT id, preventer_type, volume, membrane, press, connection, soild_material, other
      FROM alpha4
      ORDER BY id DESC
  </cfquery>

</cfif>

</head>
<body>

  <cfinclude template="includes/nav.cfm">

  <!-- Page Content -->
  <div class="container-fluid">

    <div class="row">
                
        <div class="col-lg-12">
          <cfoutput>
            <cfif isDefined("FORM.part_refrence")>
              <h1 class="page-header">Replacement Part <span class="badge badge-pill badge-success">#diffrent_parts.refrence#</span></h1>
            <cfelse>
              <h1 class="page-header">Replacement Parts <span class="badge badge-pill badge-success">#diffrent_parts.recordcount#</span></h1>
            </cfif>

            </cfoutput>

        </div>

      </div>

      <!-- Include search form -->
      <cfinclude template="includes/parts_search_form.cfm">


        <table class="table table-striped table-dark" id="ss_table">
          <thead>
            <tr>
              <th scope="col">Part ID</th>
              <th scope="col">Preventer Type</th>
              <th scope="col">Volume</th>
              <th scope="col">Membrane</th>
              <th scope="col">Pressure</th>
              <th scope="col">Connection</th>
              <th scope="col">Soild Material</th>
              <th scope="col">Other</th>
<!--               <th scope="col">Price Each</th>
              <th scope="col">Quantity Sold</th>
 -->            </tr>
          </thead>
          <tbody>
            <cfoutput query="diffrent_parts" maxrows="1000">

              <tr>

                <td><a href="part_details.cfm?id=#id#">#id#</a></td>
                <td><cfif isDefined("preventer_type")>#URLDecode(preventer_type)#</cfif></td>
                <td><cfif isDefined("volume")>#URLDecode(volume)#</cfif></td>
                <td><cfif isDefined("membrane")>#URLDecode(membrane)#</cfif></td>
                <td><cfif isDefined("PRESS")>#URLDecode(PRESS)#</cfif></td>
                <td><cfif isDefined("connection")>#URLDecode(connection)#</cfif></td>
                <td><cfif isDefined("soild_material")>#URLDecode(soild_material)#</cfif></td>
                <td><cfif isDefined("OTHER")>#URLDecode(OTHER)#</cfif></td>                
<!--                 <td><cfif isDefined("pr_ea")>&pound;#NumberFormat(pr_ea, 0.00)#</cfif></td>
                <td><cfif isDefined("qty")>#qty#</cfif></td>
 -->              </tr>
            </cfoutput>
          </tbody>
        </table>

    </div>

  </div>
  
  <hr>
  <!-- End of page main content -->

  <cfinclude template="includes/footer.cfm">
  <cfinclude template="includes/java_include.cfm">

  <script type="text/JavaScript">
    $(document).ready(function(){ 
      $("#ss_table").tablesorter(); 
    }); 
  </script>

</body>
</html>