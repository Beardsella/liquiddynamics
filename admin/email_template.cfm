<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Billing e.g. invoices and receipts</title>

<style type="text/css">
	/* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
* {
  margin: 0;
  font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
  box-sizing: border-box;
  font-size: 14px;
}

img {
  max-width: 100%;
}

body {
  -webkit-font-smoothing: antialiased;
  -webkit-text-size-adjust: none;
  width: 100% !important;
  height: 100%;
  line-height: 1.6em;
  /* 1.6em * 14px = 22.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 22px;*/
}

/* Let's make sure all tables have defaults */
table td {
  vertical-align: top;
}

/* -------------------------------------
    BODY & CONTAINER
------------------------------------- */
body {
  background-color: #f6f6f6;
}

.body-wrap {
  background-color: #f6f6f6;
  width: 100%;
}

.container {
  display: block !important;
  max-width: 600px !important;
  margin: 0 auto !important;
  /* makes it centered */
  clear: both !important;
}

.content {
  max-width: 600px;
  margin: 0 auto;
  display: block;
  padding: 20px;
}

/* -------------------------------------
    HEADER, FOOTER, MAIN
------------------------------------- */
.main {
  background-color: #fff;
  border: 1px solid #e9e9e9;
  border-radius: 3px;
}

.content-wrap {
  padding: 20px;
}

.content-block {
  padding: 0 0 20px;
}

.header {
  width: 100%;
  margin-bottom: 20px;
}

.footer {
  width: 100%;
  clear: both;
  color: #999;
  padding: 20px;
}
.footer p, .footer a, .footer td {
  color: #999;
  font-size: 12px;
}

/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, h2, h3 {
  font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  color: #000;
  margin: 40px 0 0;
  line-height: 1.2em;
  font-weight: 400;
}

h1 {
  font-size: 32px;
  font-weight: 500;
  /* 1.2em * 32px = 38.4px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 38px;*/
}

h2 {
  font-size: 24px;
  /* 1.2em * 24px = 28.8px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 29px;*/
}

h3 {
  font-size: 18px;
  /* 1.2em * 18px = 21.6px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 22px;*/
}

h4 {
  font-size: 14px;
  font-weight: 600;
}

p, ul, ol {
  margin-bottom: 10px;
  font-weight: normal;
}
p li, ul li, ol li {
  margin-left: 5px;
  list-style-position: inside;
}

/* -------------------------------------
    LINKS & BUTTONS
------------------------------------- */
a {
  color: #348eda;
  text-decoration: underline;
}

.btn-primary {
  text-decoration: none;
  color: #FFF;
  background-color: #348eda;
  border: solid #348eda;
  border-width: 10px 20px;
  line-height: 2em;
  /* 2em * 14px = 28px, use px to get airier line-height also in Thunderbird, and Yahoo!, Outlook.com, AOL webmail clients */
  /*line-height: 28px;*/
  font-weight: bold;
  text-align: center;
  cursor: pointer;
  display: inline-block;
  border-radius: 5px;
  text-transform: capitalize;
}

/* -------------------------------------
    OTHER STYLES THAT MIGHT BE USEFUL
------------------------------------- */
.last {
  margin-bottom: 0;
}

.first {
  margin-top: 0;
}

.aligncenter {
  text-align: center;
}

.alignright {
  text-align: right;
}

.alignleft {
  text-align: left;
}

.clear {
  clear: both;
  margin-bottom: 20px;
}

/* -------------------------------------
    ALERTS
    Change the class depending on warning email, good email or bad email
------------------------------------- */
.alert {
  font-size: 16px;
  color: #fff;
  font-weight: 500;
  padding: 20px;
  text-align: center;
  border-radius: 3px 3px 0 0;
}
.alert a {
  color: #fff;
  text-decoration: none;
  font-weight: 500;
  font-size: 16px;
}
.alert.alert-warning {
  background-color: #FF9F00;
}
.alert.alert-bad {
  background-color: #D0021B;
}
.alert.alert-good {
  background-color: #68B90F;
}

/* -------------------------------------
    INVOICE
    Styles for the billing table
------------------------------------- */
.invoice {
  margin: 40px auto;
  text-align: left;
  width: 80%;
}
.invoice td {
  padding: 5px 0;
}
.invoice .invoice-items {
  width: 100%;
}
.invoice .invoice-items td {
  border-top: #eee 1px solid;
}
.invoice .invoice-items .total td {
  border-top: 2px solid #333;
  border-bottom: 2px solid #333;
  font-weight: 700;
}

/* -------------------------------------
    RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
@media only screen and (max-width: 640px) {
  body {
    padding: 0 !important;
  }

  h1, h2, h3, h4 {
    font-weight: 800 !important;
    margin: 20px 0 5px !important;
  }

  h1 {
    font-size: 22px !important;
  }

  h2 {
    font-size: 18px !important;
  }

  h3 {
    font-size: 16px !important;
  }

  .container {
    padding: 0 !important;
    width: 100% !important;
  }

  .content {
    padding: 0 !important;
  }

  .content-wrap {
    padding: 10px !important;
  }

  .invoice {
    width: 100% !important;
  }
}

/*# sourceMappingURL=styles.css.map */

</style>

</head>

<body itemscope itemtype="http://schema.org/EmailMessage">

<cfoutput>

<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" width="600">
			<div class="content">
				<table class="main" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="content-wrap aligncenter">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="content-block">
										<h1 class="aligncenter">&pound; #total# Paid</h1>
									</td>
								</tr>
								<tr>
									<td class="content-block">
										<h2 class="aligncenter">Thanks for using liquiddynamics.co</h2>
                    <p class="aligncenter">We've recieved your payment via PayPal</p>
									</td>
								</tr>
								<tr>
									<td class="content-block aligncenter">
										<table class="invoice">
                      <tr>
                        <td>#URLDecode(firstname)# #URLDecode(lastname)#<br>
                        <a href="mailto:#URLDecode(email)#">#URLDecode(email)#</a><br>
                        Invoice## #id#<br>
                        #DateFormat(payment_confirmation_time, 'dd/mmm/yy')#<br></td>
                      </tr>
											<tr>
												<td>
													<table class="invoice-items" cellpadding="0" cellspacing="0">

                            <cfquery name="order_details">
                              SELECT *
                              FROM order_lines
                              WHERE order_id = #this_order.id#
                            </cfquery>

                            <cfloop query="order_details">

                              <cfquery name="this_product">
                                SELECT *
                                FROM products
                                WHERE id = #order_details.prod_id#
                              </cfquery>

  														<tr>
                              
                                <td>
                                  <a href="https://liquiddynamics.co/#URLDecode(this_product.prod_url)#">#URLDecode(this_product.product)#</a><br>
                                  <cfif order_details.option_1 NEQ "">
                                    <cfquery name="option_1_details">
                                      SELECT name, type
                                      FROM options
                                      WHERE id = '#order_details.option_1#'
                                    </cfquery>
                                    #UCFirst(option_1_details.type)# = #URLDecode(option_1_details.name)#<br>
                                  </cfif>
                                  <cfif order_details.option_2 NEQ "">
                                    <cfquery name="option_2_details">
                                      SELECT name, type
                                      FROM options
                                      WHERE id = '#order_details.option_2#'
                                    </cfquery>
                                    #UCFirst(option_2_details.type)# = #URLDecode(option_2_details.name)#<br>
                                  </cfif>
                                  <cfif order_details.option_3 NEQ "">
                                    <cfquery name="option_3_details">
                                      SELECT name, type
                                      FROM options
                                      WHERE id = '#order_details.option_3#'
                                    </cfquery>
                                    #UCFirst(option_3_details.type)# = #URLDecode(option_3_details.name)#<br>
                                  </cfif>
                                </td>

                                <td>x #URLDecode(order_details.quantity)#</td>
  															<td class="alignright">&pound; #this_product.price#</td>
  														</tr>

                            </cfloop>

                            <tr>

                              <cfif #discount_savings# EQ '0.00'>

                              <cfelseif #this_order.discount# contains "bogof">
                                <tr>
                                  <td>Discount</td>
                                  <td class="aligncenter">Buy one get on free</td>
                                  <td class="alignright">-&pound; #discount_savings#</td>
                                </tr>
                              <cfelse>
                                <tr>
                                  <td>Discount</td>
                                  <td></td>
                                  <td class="alignright">-&pound; #discount_savings#</td>
                                </tr>
                              </cfif>

                            </tr>

                            <tr>
                              <td>Postage</td>
                              <td></td>
                              <td class="alignright">&pound; #postage#</td>
                            </tr>

                            <tr class="total">
                              <td></td>
                              <td class="alignright">Total (Inc VAT)</td>
                              <td class="alignright">&pound; #total#</td>
                            </tr>


													</table>

                          <div class="clear"></div>

                          <table class="invoice-items" cellpadding="0" cellspacing="0">
                            
                            <tr>
                              <th>Delivery Address</th>
                              <td class="alignright">#URLDecode(this_order.street_add)#</td>
                            </tr> 
                            <cfif #this_order.street_add2# NEQ "null" AND #this_order.street_add2# NEQ "">                          
                              <tr>
                                <td></td>
                                <td class="alignright">#URLDecode(this_order.street_add2)#</td>
                              </tr>
                            </cfif>                      
                            <tr>
                              <td></td>
                              <td class="alignright">#URLDecode(this_order.city_add)#</td>
                            </tr>       
                            <cfif #this_order.state_add# NEQ "null" AND #this_order.state_add# NEQ "">                          
                              <tr>
                                <td></td>
                                <td class="alignright">#URLDecode(this_order.state_add)#</td>
                              </tr>
                            </cfif>
                            <tr>
                              <td>Country</td>
                              <td class="alignright">#URLDecode(this_order.country)#</td>
                            </tr>
                            <tr>
                              <td>Post Code</td>
                              <td class="alignright">#URLDecode(this_order.post_code)#</td>
                            </tr>
                          </table>

												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="content-block aligncenter">
										<a href="https://liquiddynamics.co">View in browser</a>
									</td>
								</tr>
								<tr>
									<td class="content-block aligncenter">
										<a href="https://liquiddynamics.co/contact-us.cfm">Contact liquiddynamics</a>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<div class="footer">
					<table width="100%">
						<tr>
							<td class="aligncenter content-block">Questions? Email <a href="mailto:info@liquiddynamics.co">info@liquiddynamics.co</a></td>
						</tr>
					</table>
				</div></div>
		</td>
		<td></td>
	</tr>
</table>

</cfoutput>

</body>
</html>
