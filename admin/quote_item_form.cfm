<div class="row mt-5">

  <div class="col-6">
    <cfoutput>
      <h3>Quote Items <span class="badge badge-primary">#this_quotes_items.recordcount#</span></h3>
    </cfoutput>
  </div> 

  <div class="col-6 float-right">
    
    <!-- New Common Job Parameter Modal TRIGGER -->
    <button class="btn btn-outline-success float-right mr-2" type="button" data-toggle="modal" data-target="#new_item_modal">Add a New Quote Item</button>

    <cfif '#this_quotes_items.recordcount#' GTE '1'>
        <!-- Expand existing Common Job Parameters TRIGGER -->
        <button class="btn btn-outline-info float-right mr-2" type="button" data-toggle="collapse" data-target="#quote_items" aria-expanded="false" aria-controls="quote_items">Expand Quote Items</button>
    </cfif>

  </div>

</div>

<hr>

<div class="collapse" id="quote_items">
  <cfset quote_items = '0'>

  <cfoutput query="this_quotes_items">

    <input type="hidden" name="item_#this_quotes_items.CurrentRow#_id" id="item_#this_quotes_items.CurrentRow#_id" value="#id#">

    <cfset quote_items = '#quote_items#' + '1'>

      <h3 class="text-center mt-5 mb-5">Damper #this_quotes_items.CurrentRow# Info</h3>




      <div class="row">
        <div class="col-2">Qty Required</div>
        <div class="col-2"><input id="item_#this_quotes_items.CurrentRow#_qty" name="item_#this_quotes_items.CurrentRow#_qty" type="text" class="form-control input-md" value="#URLDecode(this_quotes_items.item_qty)#" required></div>
        <div class="col-2">Type of Damper</div>
        <div class="col-6"><input list="damper_type_list" type="text" class="form-control input-group-text" id="item_#this_quotes_items.CurrentRow#_damper_type" name="item_#this_quotes_items.CurrentRow#_damper_type" placeholder="Search for damper type" value="#URLDecode(this_quotes_items.damper_type)#"></div>
      </div>

      <datalist id="damper_type_list">
        <cfloop query="all_quotes_items" group="damper_type">
          <option value="#URLDecode(damper_type)#">
        </cfloop>
      </datalist>

<div class="row mt-3">

  <div class="col-md">
    <p class="align-middle">Unit Ref</p>
  </div>

  <div class="col-md">
    <label for="item_#this_quotes_items.CurrentRow#_preventer_type">Preventer Type</label>
    <div class="input-group mb-3">
      <input list="preventer_types" type="text" class="form-control input-group-text" id="item_#this_quotes_items.CurrentRow#_preventer_type" name="item_#this_quotes_items.CurrentRow#_preventer_type" value="#URLDecode(this_quotes_items.preventer_type)#">
    </div>
  </div>

  <datalist id="preventer_types">
    <cfloop query="all_parts_data" group="preventer_type">
      <option value="#URLDecode(preventer_type)#">
    </cfloop>
  </datalist>

  <div class="col-md">
    <label for="item_#this_quotes_items.CurrentRow#_volume">Volume</label>
    <div class="input-group mb-3">
      <input list="volume_list" type="text" class="form-control" id="item_#this_quotes_items.CurrentRow#_volume" name="item_#this_quotes_items.CurrentRow#_volume" value="#URLDecode(this_quotes_items.volume)#">
    </div>
  </div>

  <datalist id="volume_list">
    <cfloop query="all_parts_data" group="volume">
      <option value="#URLDecode(volume)#">
    </cfloop>
  </datalist>


  <div class="col-md">
    <label for="item_#this_quotes_items.CurrentRow#_membrane">Membrane</label>
    <div class="input-group mb-3">
      <input list="membrane_list" type="text" class="form-control" id="item_#this_quotes_items.CurrentRow#_membrane" name="item_#this_quotes_items.CurrentRow#_membrane" value="#URLDecode(this_quotes_items.membrane)#">
    </div>
  </div>

  <datalist id="membrane_list">
    <cfloop query="all_parts_data" group="membrane">
      <option value="#URLDecode(membrane)#">
    </cfloop>
  </datalist>

  <div class="col-md">
    <label for="item_#this_quotes_items.CurrentRow#_pressure">Pressure</label>
    <div class="input-group mb-3">
      <input list="PRESS_list" type="text" class="form-control" id="item_#this_quotes_items.CurrentRow#_pressure" name="item_#this_quotes_items.CurrentRow#_pressure" value="#URLDecode(this_quotes_items.pressure)#">
    </div>
  </div>

  <datalist id="PRESS_list">
    <cfloop  query="all_parts_data" group="PRESS">
      <option value="#URLDecode(PRESS)#">
    </cfloop>
  </datalist>


  <div class="col-md">
    <label for="item_#this_quotes_items.CurrentRow#_part_connection">Connection</label>
    <div class="input-group mb-3">
      <input list="part_connection_list" type="text" class="form-control" id="item_#this_quotes_items.CurrentRow#_part_connection" name="item_#this_quotes_items.CurrentRow#_part_connection" value="#URLDecode(this_quotes_items.connection)#">
    </div>
  </div>

  <datalist id="part_connection_list">
    <cfloop query="all_parts_data" group="connection">
      <option value="#URLDecode(connection)#">
    </cfloop>
  </datalist>


  <div class="col-md">
    <label for="item_#this_quotes_items.CurrentRow#_soild_material">Soild Material</label>
    <div class="input-group mb-3">
      <input list="soild_material_list" type="text" class="form-control" id="item_#this_quotes_items.CurrentRow#_soild_material" name="item_#this_quotes_items.CurrentRow#_soild_material" value="#URLDecode(this_quotes_items.solid_material)#">
    </div>
  </div>


  <datalist id="soild_material_list">
    <cfloop query="all_parts_data" group="soild_material">
      <option value="#URLDecode(soild_material)#">
    </cfloop>
  </datalist>


  <div class="col-md">
    <label for="item_#this_quotes_items.CurrentRow#_OTHER">Other</label>
    <div class="input-group mb-3">
      <input list="OTHER_list" type="text" class="form-control" id="item_#this_quotes_items.CurrentRow#_OTHER" name="item_#this_quotes_items.CurrentRow#_OTHER" value="#URLDecode(this_quotes_items.miscellaneous)#">
    </div>
  </div>

  <datalist id="OTHER_list">
    <cfloop query="all_parts_data" group="OTHER">
      <option value="#URLDecode(OTHER)#">
    </cfloop>
  </datalist>


</div>


<div class="row mt-3">


    <div class="col-6">

      <div class="row">
        <div class="col-6">Working Pressure</div>
        <div class="col-2"><input id="item_#this_quotes_items.CurrentRow#_working_pressure" name="item_#this_quotes_items.CurrentRow#_working_pressure" type="text" class="form-control input-md" value="#URLDecode(this_quotes_items.working_pressure)#"></div>
        <div class="col-4">                
          <select id="item_#this_quotes_items.CurrentRow#_working_pressure_units" name="item_#this_quotes_items.CurrentRow#_working_pressure_units" class="form-control">
            <option value="#this_quotes_items.working_pressure_units#">#URLDecode(this_quotes_items.working_pressure_units)#</option>
            <option value="PSI">PSI</option>
            <option value="BAR">BAR</option>
          </select>
        </div>
      </div>

      <datalist id="damper_type_list">
        <cfloop query="all_quotes_items" group="damper_type">
          <option value="#URLDecode(working_pressure)#">
        </cfloop>
      </datalist>

      <div class="row">
        <div class="col-6">Design Pressure</div>
        <div class="col-2"><input id="item_#this_quotes_items.CurrentRow#_design_pressure" name="item_#this_quotes_items.CurrentRow#_design_pressure" type="text" class="form-control input-md" value="#this_quotes_items.design_pressure#"></div>

        <div class="col-4">                
          <select id="item_#this_quotes_items.CurrentRow#_design_pressure_units" name="item_#this_quotes_items.CurrentRow#_design_pressure_units" class="form-control">
            <option value="#this_quotes_items.design_pressure_units#">#URLDecode(this_quotes_items.design_pressure_units)#</option>
            <option value="PSI">PSI</option>
            <option value="BAR">BAR</option>
          </select>
        </div>        
      </div>


      <datalist id="damper_type_list">
        <cfloop query="all_quotes_items" group="damper_type">
          <option value="#URLDecode(damper_type)#">
        </cfloop>
      </datalist>

      <div class="row">
        <div class="col-6">Pre-fill pressure<br><small class="text-danger">Customers Responsibility</small></div>
        <div class="col-6"><input id="item_#this_quotes_items.CurrentRow#_cushion_prefill_pressure" name="item_#this_quotes_items.CurrentRow#_cushion_prefill_pressure" type="text" class="form-control input-md" list="prefill_pressure_list" value="#URLDecode(this_quotes_items.cushion_prefill_pressure)#"></div>
      </div>

      <datalist id="prefill_pressure_list">
        <cfloop query="all_quotes_items" group="cushion_prefill_pressure">
          <option value="#URLDecode(cushion_prefill_pressure)#">
        </cfloop>
      </datalist>

      <div class="row">
        <div class="col-6">Nominal Length</div>
        <div class="col-6"><input id="item_#this_quotes_items.CurrentRow#_length" name="item_#this_quotes_items.CurrentRow#_length" type="text" class="form-control input-md" value="#this_quotes_items.length#"></div>
      </div>
      <div class="row">
        <div class="col-6">Nominal Width</div>
        <div class="col-6"><input id="item_#this_quotes_items.CurrentRow#_width" name="item_#this_quotes_items.CurrentRow#_width" type="text" class="form-control input-md" value="#this_quotes_items.width#"></div>
      </div>
      
    </div>
    <div class="col-6">

      <h4 class="text-center">Materials Of Construction</h4>

      <div class="row">
        <div class="col-6">Wetted Material</div>
        <div class="col-6"><input id="item_#this_quotes_items.CurrentRow#_wetted_material" name="item_#this_quotes_items.CurrentRow#_wetted_material" type="text" class="form-control input-md" value="#URLDecode(this_quotes_items.wetted_material)#" list="wetted_material_list"></div>
      </div>

      <datalist id="wetted_material_list">
        <cfloop query="all_quotes_items" group="wetted_material">
          <option value="#URLDecode(wetted_material)#">
        </cfloop>
      </datalist>

      <div class="row">
        <div class="col-6">Non-Wetted Material</div>
        <div class="col-6"><input id="item_#this_quotes_items.CurrentRow#_non_wetted_material" name="item_#this_quotes_items.CurrentRow#_non_wetted_material" type="text" class="form-control input-md" value="#URLDecode(this_quotes_items.non_wetted_material)#" list="non_wetted_material_list"></div>
      </div>

      <datalist id="non_wetted_material_list">
        <cfloop query="all_quotes_items" group="non_wetted_material">
          <option value="#URLDecode(non_wetted_material)#">
        </cfloop>
      </datalist>

      <div class="row">
        <div class="col-6">Flexing Membrane &amp; Sealing</div>
        <div class="col-6"><input id="item_#this_quotes_items.CurrentRow#_flexing_membrane" name="item_#this_quotes_items.CurrentRow#_flexing_membrane" type="text" class="form-control input-md" value="#URLDecode(this_quotes_items.flexing_membrane)#" list="flexing_membrane_list"></div>
      </div>

      <datalist id="flexing_membrane_list">
        <cfloop query="all_quotes_items" group="flexing_membrane">
          <option value="#URLDecode(flexing_membrane)#">
        </cfloop>
      </datalist>

      <div class="row">
        <div class="col-6">Painting Spec</div>
        <div class="col-6"><input id="item_#this_quotes_items.CurrentRow#_paint_spec" name="item_#this_quotes_items.CurrentRow#_paint_spec" type="text" class="form-control input-md" list="paint_spec_list" value="#URLDecode(this_quotes_items.paint_spec)#"></div>
      </div>

      <datalist id="paint_spec_list">
        <cfloop query="all_quotes_items" group="paint_spec">
          <option value="#URLDecode(paint_spec)#">
        </cfloop>
      </datalist>

    </div>
</div>

<div class="clearfix"></div>

  <div class="row">
    <div class="col-2">Design Code</div>
    <div class="col-10"><input id="item_#this_quotes_items.CurrentRow#_design_code" name="item_#this_quotes_items.CurrentRow#_design_code" type="text" class="form-control input-block" value="#URLDecode(this_quotes_items.design_code)#"></div>
  </div>

  <div class="row">

    <!-- Text input-->
    <div class="col-2">Price Per Unit &pound;</div>  
    <div class="col-10">
      <input id="item_price_#CurrentRow#" name="item_price_#CurrentRow#" type="text" placeholder="100" class="form-control input-md" value="#this_quotes_items.item_price#" required>
    </div>

    <hr>

    <button class="btn btn-link btn-block" type="button" data-toggle="collapse" data-target="##cost_details" aria-expanded="false" aria-controls="cost_details">More Cost Details...</button>
        
  </div>

<div class="collapse" id="cost_details">

  <div class="row">

    <!-- Text input-->
    <div class="col-md-4 form-group">
      <label class="control-label" for="item_currency_#CurrentRow#">Currency if not GBP</label>  
      <div class="">
      <input id="item_currency_#CurrentRow#" name="item_currency_#CurrentRow#" type="text" placeholder="GBP" class="form-control input-md" value="#URLDecode(this_quotes_items.currency)#">
        
      </div>
    </div>

    <!-- Text input-->
    <div class="col-md-4 form-group">
      <label class="control-label" for="item_conversion_#CurrentRow#">Conversion rate to GBP</label>  
      <div class="">
      <input id="item_conversion_#CurrentRow#" name="item_conversion_#CurrentRow#" type="text" placeholder="0.678" class="form-control input-md" value="#this_quotes_items.conversion#">
        
      </div>
    </div>


    <!-- Text input-->
    <div class="col-md-4 form-group">
      <label class="control-label" for="discount_percent_#CurrentRow#">Discount %</label>  
      <div class="">
      <input id="discount_percent_#CurrentRow#" name="discount_percent_#CurrentRow#" type="text" placeholder="100" class="form-control input-md" value="#this_quotes_items.discount_percent#">
        
      </div>
    </div>

    </div>

    <div class="row">

        <!-- Textarea -->
        <div class="form-group col-12">
          <label class="control-label" for="discount_notes_#CurrentRow#">Discount Notes</label>
          <div class="">                     
            <textarea class="form-control" id="discount_notes_#CurrentRow#" name="discount_notes_#CurrentRow#">#URLDecode(this_quotes_items.discount_notes)#</textarea>
          </div>
        </div>
</div>

</div>  

<div class="clearfix"></div>
<hr>
</cfoutput>

</div>