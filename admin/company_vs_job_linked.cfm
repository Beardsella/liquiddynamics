<cfoutput>
    <form class="form-horizontal" action="processing_files/enquirys_pump_systems_insert.cfm" method="post" enctype="multipart/form-data">

      <div class="col-lg-12">

        <!-- System Operating Temperature -->
        <div class="form-group">
          <label for="company_name">Company Name</label>
          <input type="text" class="form-control" id="company_name" name="company_name" value="#URLDecode(this_company.company_name)#">
        </div>

        <!-- System Operating Temperature -->
        <div class="form-group">
          <label for="telephone">Telephone</label>
          <input type="text" class="form-control" id="telephone" name="telephone" value="#URLDecode(this_company.telephone)#">
        </div>

        <!-- System Design Temperature -->
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" name="email" value="#URLDecode(this_company.email)#">
        </div>

        <!-- System Operating Temperature -->
        <div class="form-group">
          <label for="mail_1">Address</label>
          <input type="text" class="form-control" id="mail_1" name="mail_1" value="#URLDecode(this_company.mail_1)#">
          <input type="text" class="form-control" id="mail_2" name="mail_2" value="#URLDecode(this_company.mail_2)#">
          <input type="text" class="form-control" id="mail_3" name="mail_3" value="#URLDecode(this_company.mail_3)#">
          <input type="text" class="form-control" id="county" name="county" value="#URLDecode(this_company.county)#" placeholder="County">
          <input type="text" class="form-control" id="postcode" name="postcode" value="#URLDecode(this_company.postcode)#" placeholder="Post Code">
        </div>
        <!-- System Operating Temperature -->
        <div class="form-group">
          <label for="fax_number">Fax</label>
          <input type="text" class="form-control" id="fax_number" name="fax_number" value="#URLDecode(this_company.fax_number)#">
        </div>

        <!-- Connection Type Required (Flange, NPT, Tri Clamp etc) -->
        <div class="form-group">
           <label for="country">Country</label>
            <select id="country" name="country" class="form-control">
              <option value="">#URLDecode(this_company.country)#</option>
                <cfloop query="countries">
                      <option value="#country#">#country#</option>
                </cfloop>
            </select>
        </div>

        <button type="submit" class="btn btn-success btn-block">Update Details</button>

      </div>

    </form>

</cfoutput>