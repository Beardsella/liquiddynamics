<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <title>Manage Contracts - Liquid Dynamics Group Ltd</title>

    <cfquery name="all_jobs" datasource="agbcodes_ldi">
        SELECT *
        FROM jobs
        WHERE job_status <> "complete"
    </cfquery>    

    <cfquery name="countries" datasource="agbcodes_ldi">
        SELECT *
        FROM countries
    </cfquery>


</head>
<body>

    <cfinclude template="includes/nav.cfm">

      <!-- Page Content -->
      <main role="main" class="container-fluid">

            <cfoutput>

                <nav aria-label="breadcrumb" role="navigation">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.cfm">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="jobs_overview.cfm">Contracts Overview</a></li>
                  </ol>
                </nav>            

                <div class="col-lg-12">
                    <h1 class="page-header">Active Contracts <span class="badge badge-info">#all_jobs.RecordCount#</span></h1>
                    <p class="lead">Here you can see active Contracts that are still in progress. Non active contracts can still be found here <a href="contracts_archive.cfm">All Contracts</a></p>

                <table class="table table-striped table-dark" id="ss_table">

                  <thead>
                      <th>Contract ID ##</th>
                      <th>Buyer</th>
                      <th>Contact</th>
                      <th>Enquiry</th>
                      <th>Quote</th>
                      <th>Works Order</th>
                      <th>Invoice</th>
                  </thead>

                  <tbody>

                    <cfloop query="all_jobs">

                      <cfquery name="this_company" datasource="agbcodes_ldi">
                          SELECT *
                          FROM companies
                          WHERE id = "#company_id#"
                      </cfquery>      

                      <cfquery name="staff" datasource="agbcodes_ldi">
                          SELECT *
                          FROM contacts
                          WHERE id = "#contact_id#"
                      </cfquery>

                      <cfquery name="linked_enquiries" datasource="agbcodes_ldi">
                          SELECT enquirys_pump_systems.formatted_id, job_vs_enq.*
                          FROM enquirys_pump_systems
                          RIGHT JOIN job_vs_enq 
                          ON enquirys_pump_systems.id = job_vs_enq.enquiry_id
                          WHERE job_vs_enq.job_id = '#all_jobs.id#'
                      </cfquery>

                      <cfquery name="quotes" datasource="agbcodes_ldi">
                          SELECT quotes.*
                          FROM quotes
                          RIGHT JOIN job_vs_quote 
                          ON quotes.id = job_vs_quote.quote_id
                          WHERE job_vs_quote.job_id = "#id#"
                      </cfquery>

                      <cfquery name="w_orders" datasource="agbcodes_ldi">
                          SELECT works_orders.*
                          FROM works_orders
                          RIGHT JOIN job_vs_works_order 
                          ON works_orders.id = job_vs_works_order.w_order_id
                          WHERE job_vs_works_order.job_id = "#id#"
                      </cfquery>

                      <cfquery name="invoices" datasource="agbcodes_ldi">
                          SELECT invoices.*
                          FROM invoices
                          RIGHT JOIN job_vs_invoice 
                          ON invoices.id = job_vs_invoice.invoice_id
                          WHERE job_vs_invoice.job_id = "#id#"
                      </cfquery>

                      <cfquery name="uploaded_docs" datasource="agbcodes_ldi">
                          SELECT uploaded_docs.*
                          FROM uploaded_docs
                          RIGHT JOIN job_vs_docs 
                          ON uploaded_docs.id = job_vs_docs.doc_id
                          WHERE job_vs_docs.job_id = "#id#"
                      </cfquery>

                        <cfif isDefined("quotes.seller") AND quotes.seller neq "">

                          <cfset selling_company = "#URLDecode(quotes.seller)#">

                          <cfif selling_company eq "Liquid Dynamics International">
                            <cfset company_prefix = "LDI-UK">
                          <cfelseif selling_company eq "Pulse Gard">
                            <cfset company_prefix = "PG-UK">
                          <cfelseif selling_company eq "Shock Gard">
                            <cfset company_prefix = "SG-UK">
                          <cfelseif selling_company eq "Hydrotrole">
                            <cfset company_prefix = "HT-UK">
                          </cfif>

                        </cfif>

                      <tr>
                          <td><a href="contract_details.cfm?id=#id#" class="btn btn-outline-info btn-block">#formatted_id#</a></td>
                          <td><a href="company_detail.cfm?id=#this_company.id#" class="agb-table-link">#URLDecode(this_company.company_name)#</a></td>
                          <td><a href="contact_detail.cfm?id=#staff.id#" class="agb-table-link">#URLDecode(staff.first_name)# #URLDecode(staff.last_name)#</a></td>

                          <cfif linked_enquiries.recordcount GTE '1'>
                            <td><cfloop query="linked_enquiries"><a href="enquiry_management.cfm?id=#linked_enquiries.id#" class="btn btn-outline-info btn-block">#linked_enquiries.formatted_id#</a></cfloop></td>
                          <cfelse>
                            <td class="text-center"><span class="text-danger">No linked enquiry</span></td>
                          </cfif>
                          <td>
                            <cfif quotes.recordcount GTE '1'>
                            <cfloop query="quotes"><a href="quote_management.cfm?id=#quotes.id#" class="btn btn-outline-info btn-block">#quotes.formatted_id#</a></cfloop>
                            <cfelse>

                            </cfif>
                          </td>
                          <td>
                            <cfif w_orders.recordcount GTE '1'>
                              <cfloop query="w_orders"><a href="works_order.cfm?id=#w_orders.id#" class="btn btn-outline-info btn-block">#w_orders.formatted_id#</a></cfloop></td>
                            <cfelse>
                              <a href="" class="btn btn-block btn-outline-success">Create new works order</a>
                            </cfif>
                          <td>
                            <cfif invoices.recordcount GTE '1'>
                              <cfloop query="invoices">
                                <a href="invoice.cfm?id=#invoices.id#" class="btn btn-outline-info btn-block">#invoices.formatted_id#</a>
                              </cfloop>
                            <cfelse>
                                <button type="button" class="btn btn-outline-warning btn-block" data-toggle="modal" data-target="##new_invoice_modal">Create a new Invoice</button>
                            </cfif>
                          </td>

                        </tr>



<!-- Modal -->
<div class="modal fade" id="new_invoice_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create a new Invoice</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="lead">This action will create a new "Invoice" based off the current quote "#formatted_id#".</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="processing_files/invoice_create.cfm" class="btn btn-primary">Create new Invoice</a>
      </div>
    </div>
  </div>
</div>                        

                      </cfloop>

                  </tbody>                

                </table>


                </div>

                

            </cfoutput>

      </main>

    <hr>
    <!-- End of page main content -->

    <cfinclude template="includes/footer.cfm">
    <cfinclude template="includes/java_include.cfm">

</body>
</html>