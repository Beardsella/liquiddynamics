<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <title>Company Detail - Liquid Dynamics Group Ltd</title>

    <cfif isDefined("URL.company") AND #URL.company# NEQ "">

      <cfquery name="this_company" datasource="agbcodes_ldi">
          SELECT *
          FROM companies
          WHERE company_name = "#URLDecode(URL.company)#"
      </cfquery>

    <cfelse>

      <cfquery name="this_company" datasource="agbcodes_ldi">
          SELECT *
          FROM companies
          WHERE id = "#URL.id#"
      </cfquery>      

    </cfif>

    <cfquery name="countries" datasource="agbcodes_ldi">
        SELECT *
        FROM countries
    </cfquery>

    <cfquery name="staff" datasource="agbcodes_ldi">
        SELECT *
        FROM contacts
        WHERE company_id = "#this_company.id#"
        ORDER BY last_name
    </cfquery>

    <cfquery name="enquiries" datasource="agbcodes_ldi">
        SELECT *
        FROM enquirys_pump_systems
        WHERE company_id = '#this_company.id#'
        ORDER BY enquiry_date
    </cfquery>

    <cfquery name="quotes" datasource="agbcodes_ldi">
        SELECT *
        FROM quotes
        WHERE company_id = '#this_company.id#'
    </cfquery>

</head>
<body>

    <cfinclude template="includes/nav.cfm">

      <!-- Page Content -->
      <main role="main" class="container">

            <cfoutput>

                <nav aria-label="breadcrumb" role="navigation">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.cfm">Home</a></li>
                    <li class="breadcrumb-item"><a href="companies_overview.cfm">Companies Overview</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Company Detail</li>
                  </ol>
                </nav>            

                <div class="col-lg-12">
                    <h1 class="page-header">Company Detail</h1>
                    <p class="lead">Here you can view details and related data to "#URLDecode(this_company.company_name)#"</p>

  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="company-tab" data-toggle="tab" href="##company" role="tab" aria-controls="company" aria-selected="true">Company Details</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="contacts-tab" data-toggle="tab" href="##contacts" role="tab" aria-controls="contacts" aria-selected="false">Staff</a>
    </li>    
    <li class="nav-item">
      <a class="nav-link" id="jobs-tab" data-toggle="tab" href="##jobs" role="tab" aria-controls="jobs" aria-selected="false">Jobs</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="enquiries-tab" data-toggle="tab" href="##enquiries" role="tab" aria-controls="enquiries" aria-selected="true">Enquiries</a>
    </li>  
    <li class="nav-item">
      <a class="nav-link" id="quote-tab" data-toggle="tab" href="##quotes" role="tab" aria-controls="quote" aria-selected="true">Quotes</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="w_order-tab" data-toggle="tab" href="##w_orders" role="tab" aria-controls="w_order" aria-selected="true">Works Orders</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="invoice-tab" data-toggle="tab" href="##invoices" role="tab" aria-controls="invoice" aria-selected="true">Invoices</a>
    </li>
  </ul>

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="company" role="tabpanel" aria-labelledby="company-tab">

    <form class="form-horizontal" action="processing_files/enquirys_pump_systems_insert.cfm" method="post" enctype="multipart/form-data">

      <div class="col-lg-12">

        <!-- System Operating Temperature -->
        <div class="form-group">
          <label for="company_name">Company Name</label>
          <input type="text" class="form-control" id="company_name" name="company_name" value="#URLDecode(this_company.company_name)#">
        </div>

        <!-- System Operating Temperature -->
        <div class="form-group">
          <label for="telephone">Telephone</label>
          <input type="text" class="form-control" id="telephone" name="telephone" value="#URLDecode(this_company.telephone)#">
        </div>

        <!-- System Design Temperature -->
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" name="email" value="#URLDecode(this_company.email)#">
        </div>

        <!-- System Operating Temperature -->
        <div class="form-group">
          <label for="mail_1">Address</label>
          <input type="text" class="form-control" id="mail_1" name="mail_1" value="#URLDecode(this_company.mail_1)#">
          <input type="text" class="form-control" id="mail_2" name="mail_2" value="#URLDecode(this_company.mail_2)#">
          <input type="text" class="form-control" id="mail_3" name="mail_3" value="#URLDecode(this_company.mail_3)#">
          <input type="text" class="form-control" id="county" name="county" value="#URLDecode(this_company.county)#" placeholder="County">
          <input type="text" class="form-control" id="postcode" name="postcode" value="#URLDecode(this_company.postcode)#" placeholder="Post Code">
        </div>
        <!-- System Operating Temperature -->
        <div class="form-group">
          <label for="fax_number">Fax</label>
          <input type="text" class="form-control" id="fax_number" name="fax_number" value="#URLDecode(this_company.fax_number)#">
        </div>

        <!-- Connection Type Required (Flange, NPT, Tri Clamp etc) -->
        <div class="form-group">
           <label for="country">Country</label>
            <select id="country" name="country" class="form-control">
              <option value="">#URLDecode(this_company.country)#</option>
                <cfloop query="countries">
                      <option value="#country#">#country#</option>
                </cfloop>
            </select>
        </div>

        <button type="submit" class="btn btn-success btn-block">Update Details</button>

      </div>

    </form>

  </div><!-- End of Tab -->                    
  
  <div class="tab-pane fade show" id="contacts" role="tabpanel" aria-labelledby="contacts-tab">

    <cfinclude template="contact_linked.cfm">
    
  </div><!-- End of Tab -->   

  <div class="tab-pane fade show" id="enquiries" role="tabpanel" aria-labelledby="enquiries-tab">

    <cfinclude template="enquiries_linked.cfm">

  </div><!-- End of Tab -->    

  <div class="tab-pane fade show" id="quotes" role="tabpanel" aria-labelledby="quote-tab">

    <cfinclude template="quotes_linked.cfm">

  </div><!-- End of Tab -->                    
                </div>

                

            </cfoutput>

      </main>

    <hr>
    <!-- End of page main content -->

    <cfinclude template="includes/footer.cfm">
    <cfinclude template="includes/java_include.cfm">

</body>
</html>