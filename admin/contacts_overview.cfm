<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <title>Contacts Overview - Liquid Dynamics Group Ltd</title>

    <cfquery name="contacts" datasource="agbcodes_ldi">
        SELECT *
        FROM contacts
    </cfquery>

</head>
<body>

    <cfinclude template="includes/nav.cfm">

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <cfoutput>          

                <div class="col-lg-12">
                    <h1 class="page-header">Contacts Overview   <span class="badge badge-info">#contacts.recordcount#</span></h1>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="##exampleModal">
                  Add a new Contact
                </button>
                <hr>
                </div>
            </cfoutput>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add a new contact</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success">Add Contact</button>
      </div>
    </div>
  </div>
</div>

            <table class="table table-hover table-striped">

              <tr>
                <th>Name</th>
                <th>Company</th>
                <th>Staff Role</th>
                <th>Telephone</th>
                <th>Email</th>
              </tr>

              <cfoutput query="contacts">
              
                <tr>
                  <td><a href="contact_detail.cfm?id=#id#">#URLDecode(contacts.first_name)# #URLDecode(contacts.last_name)#</a></td>
                  <td><a href="company_detail.cfm?company=#URLEncodedFormat(contacts.company)#">#URLDecode(contacts.company)#</a></td>
                  <td>#URLDecode(contacts.staff_role)#</td>
                  <td><a href="tel:#URLDecode(contacts.telephone)#">#URLDecode(contacts.telephone)#</a></td>
                  <td><a href="mailto:#URLDecode(contacts.email)#">#URLDecode(contacts.email)#</a></td>
                </tr>

              </cfoutput>
              
            </table>

        </div>                

    </div>

    <hr>
    <!-- End of page main content -->

  <cfinclude template="includes/footer.cfm">
  <cfinclude template="includes/java_include.cfm">

</body>
</html>