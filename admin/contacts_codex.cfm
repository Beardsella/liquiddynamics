<cfquery name="companies" datasource="agbcodes_ldi">
    SELECT *
    FROM companies
    ORDER BY company_name
</cfquery>

<cfoutput query="companies">

	<cfif isDefined("companies.number_1_sn") AND "#companies.number_1_sn#" NEQ "">
	<cfquery datasource="agbcodes_ldi">
		INSERT INTO contacts (title, first_name, last_name, telephone, staff_role, country_based, company, notes) 
		VALUES ("Mr", "#companies.number_1_sn#", "#companies.number_1_sn#", "#companies.telephone#", "Senior", "#companies.country#", "#companies.company_name#", "This is data recover for the old alpha 4 database please use with caution")
	</cfquery>
	</cfif>

	<cfif isDefined("companies.number_2_sn") AND "#companies.number_2_sn#" NEQ "">
		<cfquery datasource="agbcodes_ldi">
			INSERT INTO contacts (title, first_name, last_name, telephone, staff_role, country_based, company, notes) 
			VALUES ("Mr", "#companies.number_2_sn#", "#companies.number_2_sn#", "#companies.telephone#", "##2", "#companies.country#", "#companies.company_name#", "This is data recover for the old alpha 4 database please use with caution")
		</cfquery>
	</cfif>

	<cfif isDefined("companies.number_1_fn") AND "#companies.number_1_fn#" NEQ "">
		<cfquery datasource="agbcodes_ldi">
			INSERT INTO contacts (title, first_name, last_name, telephone, staff_role, country_based, company, notes) 
			VALUES ("Mr", "#companies.number_1_fn#", "#companies.number_1_fn#", "#companies.telephone#", "#number_1_jb#", "#companies.country#", "#companies.company_name#", "This is data recover for the old alpha 4 database please use with caution")
		</cfquery>
	</cfif>

	<cfif isDefined("companies.number_2_fn") AND "#companies.number_2_fn#" NEQ "">
		<cfquery datasource="agbcodes_ldi">
			INSERT INTO contacts (title, first_name, last_name, telephone, staff_role, country_based, company, notes) 
			VALUES ("Mr", "#companies.number_2_fn#", "#companies.number_2_fn#", "#companies.telephone#", "#number_2_jb#", "#companies.country#", "#companies.company_name#", "This is data recover for the old alpha 4 database please use with caution")
		</cfquery>
	</cfif>

</cfoutput>