<!DOCTYPE html>
<html lang="en">
<head>

	<cfinclude template="includes/default_header.cfm">

	<title>Pump System quotes - Liquid Dynamics</title>

	<!-- CSS for autocomplete -->
	<link rel="stylesheet" href="css/jquery-ui-1.12.1.css">

    <cfquery name="quotes" datasource="agbcodes_ldi">
        SELECT *
        FROM quotes
    </cfquery>

</head>
<body>

	<cfinclude template="includes/nav.cfm">

	<div class="container-fluid">

	    <!-- Start of the main page content -->
        <h1 class="page-header">Customer quotes</h1>
        <p class="lead">These are quote forms filled out by customers that havent been marked as actioned. They can be sorted by column and clicked on for more information. There are <cfoutput><span class="badge badge-pill badge-primary" title="quotes needing attention">#quotes.recordcount#</span></cfoutput> quotes needing attention.</p> <p class="lead">This is a link to the <a href="create-quote.cfm">main quote form</a></strong></p>


		<div class="tab-content">

			<!-- New quotes -->
			<div role="tabpanel" class="tab-pane active" id="new_quotes">

				<table class="table table-striped table-dark" id="ss_table">

			        <thead>
			            <th>#</th>
			            <th>Client</th>
			            <th>Contact</th>
			            <th>Date Created</th>
			            <th>Quote Refrence Name</th>
			            <th>Linked Job</th>
			            <th>PDF Preview</th>
			        </thead>

			        <tbody>

			            <cfoutput query="quotes">

						    <cfquery name="quotes" datasource="agbcodes_ldi">
						        SELECT quotes.*
						        FROM quotes
						        RIGHT JOIN job_vs_quote 
						        ON quotes.id = job_vs_quote.quote_id
						        WHERE job_vs_quote.job_id = "#id#"
						    </cfquery>

							<cfquery name="linked_job" datasource="agbcodes_ldi">
							    SELECT *
							    FROM jobs
						        WHERE quote_id = "#id#"					        							    
							</cfquery>



			                <tr>
			                	<td><a href="quote_management.cfm?id=#id#" class="agb-table-link" title="Click to view quote">#formatted_id#</a></td>
			                    <td><a href="company_detail.cfm?id=#company_id#" class="agb-table-link" title="Click to view company details">#URLDecode(company_name)#</a></td>
			                    <td><a href="contact_detail.cfm?id=#contact_id#" class="agb-table-link">#URLDecode(first_name)# #URLDecode(last_name)#</a></td>
			                    <td>#DateFormat("#quote_created#", "dd / mmm / yyyy")#</td>
			                    <td><a href="quote_management.cfm?id=#id#" class="agb-table-link" title="Click to view quote">#URLDecode(cust_enquiry_ref)#</a></td>

			                    <td>

									<cfif isDefined("linked_job.job_title") AND linked_job.job_title neq "">
										<a href="contract_details.cfm?id=#linked_job.id#" class="agb-table-link">#URLDecode(linked_job.job_title)#</a>
									<cfelseif isDefined("linked_job.id") AND linked_job.id neq "">
										<a href="contract_details.cfm?id=#linked_job.id#" class="agb-table-link">Job ###linked_job.id#</a>									
									<cfelse>
										<a href="processing_files/job_create.cfm?quote_id=#id#" class="agb-table-link">Create a new job</a>
									</cfif>

			                    </td>
			                	
			                	<td><a href="quote_pdf_preview.cfm?id=#id#" class="agb-table-link" title="Click to view quote">PDF Preview</a></td>
		                    </tr>

			            </cfoutput>
			        </tbody>                

		        </table>

		    </div>
			<!-- End of Tab -->


		</div>
		<!-- End of Tab -->
		</div>        

	<cfinclude template="includes/footer.cfm">
	<cfinclude template="includes/java_include.cfm">

	<!-- Extra table sorting for 4 extra tables -->
	<script type="text/JavaScript">
		$(document).ready(function(){ 
			$("#ss_table_1, #ss_table_2, #ss_table_3, #ss_table_4").tablesorter(); 
		}); 
	</script>

</body>
</html>