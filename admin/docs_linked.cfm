
    <cfquery name="all_files" datasource="agbcodes_ldi">
      SELECT uploaded_files.*, file_vs_job.job_id
      FROM uploaded_files
      left JOIN file_vs_job
      ON uploaded_files.id = file_vs_job.file_id
      ORDER BY uploaded_files.file_name
    </cfquery>

    <cfif find("localhost", "#CGI.http_host#")>
      <cfset local_location = "../user_uploaded_files/">
    <cfelse>
      <cfset local_location = expandPath("../user_uploaded_files/")>
    </cfif>

        <!-- Start of the main page content -->
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2">
          <cfoutput>
          <h1 class="page-header">Manage Uploaded Files <span class="badge" title="Number of uploaded files">#all_files.recordcount#</span></h1></cfoutput>

          <cfif IsDefined("URL.error") AND URL.error EQ 1>
          
            <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>Warning!</strong> There was an error whilst deleting an uploaded item, it was probaly the item has been deleted already or renamed. Please check if your item has been deleted
            </div>

          </cfif>

          <cfif IsDefined("URL.deleted")>
            <cfoutput>
              <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Success!</strong> You have deleted "#local_location##URL.deleted#"
              </div>
            </cfoutput>
          </cfif>


          <p class="lead">Here you can Edit and Delete Uploaded files.</p>
          <p>Please click on a cloumn header to sort that column.</p>

          <div class="table-responsive">
            <table class="table table-striped" id="ss_table">
              <thead>
                <tr>
                  <th></th>
                  <th></th>
                  <th>File Name</th>
                  <th>Linked Page/Prod</th>
                  <th style="max-width:400px; overflow:hidden;">Path</th>
                  <th>Meta Descrption</th>
                  <th>Meta Title</th>
                  <th>File Type</th>
                  <th>Last Edited</th>
                </tr>
              </thead>

              
              <tbody>

              <cfif all_files.recordcount lt 1>

                <tr>
                  <td colspan="9"><h2 style="text-align:center;">No uploaded files to display</h1></td>
                </tr>

              <cfelse>

                <cfoutput query="all_files">

                  <cfquery name="linked_page">
                    SELECT *
                    FROM pages
                    WHERE id = '#page_id#'
                  </cfquery>

                  <cfquery name="linked_prod">
                    SELECT *
                    FROM products
                    WHERE id = '#prod_id#'
                  </cfquery>

                  <tr>
                    <td><a href="edit_file.cfm?id=#id#" class="btn btn-primary" title="View and edit the details of this file"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
                    <td>
                      <form action="processing_files/uploaded_file_delete.cfm" method="post" enctype="multipart/form-data" addtoken="false">
                        <input type="hidden" value="#id#" name="file_delete_id" id="file_delete_id">
                        <button type="submit" class="btn btn-danger" alt="Delete '#file_name#' from the server" title="Delete '#file_name#' from the server"><span class="glyphicon glyphicon-remove" aria-hidden="true" ></span></button>
                      </form>
                    </td>
                    <td><a href="#local_location##file_name#" target="_blank">#URLDecode(file_name)#</a></td>

                    <cfif isDefined("linked_page.name")>
                      <td><a href="page_edit.cfm?id=#linked_page.id#">#URLDecode(linked_page.name)#</a> <small class="muted">Page</small></td>
                    <cfelseif isDefined("linked_prod.product")>
                      <td><a href="product_edit.cfm.cfm?id=#linked_prod.id#">#URLDecode(linked_prod.product)#</a> <small class="muted">Product</small></td>
                    <cfelse>
                      <td>N/A</td>
                    </cfif>

                    <td class="tiny" style="max-width:400px; overflow:scroll;">#URLDecode(location)##URLDecode(file_name)#</td>
                    <td>#URLDecode(description)#</td>
                    <td>#URLDecode(title)#</td>
                    <td>#file_type#</td>
                    <td>#DateFormat(last_edited, "dd/mmm/yy")#<br>
                    #TimeFormat(last_edited, "hh:mm tt")#</td>
                  </tr>

                </cfoutput>

              </cfif>  

              </tbody>
            </table>

