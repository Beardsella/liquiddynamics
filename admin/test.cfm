<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favicon.ico">
  <meta name="author" content="Adam Beardsell - http://agb.codes">
  <meta name="description" content="Liquid Dynamics International - Engineering and manufacturing services">
  <meta name="keywords" content="Liquid Dynamics, Engineering, Oil Rig Manufacturing Specialists">

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/custom_pdf.css" rel="stylesheet">

  <cfquery name="this_quote" datasource="agbcodes_ldi">
    SELECT *
    FROM quotes
    WHERE id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quote_params" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_params
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quotes_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_items
    WHERE quote_id = "#URL.id#"
  </cfquery>
  
  <cfquery name="this_quote_extra_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_extra_items
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfset quote_date_formated = "#DateFormat(this_quote.quote_dated, 'dd/mm/yyyy')#">
  <cfset dispatch_date_formated = "#DateFormat(this_quote.dispatch_estimate, 'dd/mm/yyyy')#">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



<script type="text/javascript" src="js/jsPDF.min.js"></script>
<script type="text/javascript" src="js/html2canvas.min.js"></script>

<script type="text/javascript" src="js/adler32cs.js"></script>
<script type="text/javascript" src="js/css_colors.js"></script>
<script type="text/javascript" src="js/Deflater.js"></script>
<script type="text/javascript" src="js/html2pdf.js"></script>
<script type="text/javascript" src="js/polyfill.js"></script>
<script type="text/javascript" src="js/ttffont.js"></script>

<script type="text/javascript">

  var url_string = window.location.href; 
  var url = new URL(url_string);
  var quote_file_name = url.searchParams.get("id");
  var quote_file_name = "LDI-Quote-UK-" + quote_file_name + ".pdf";
  console.log(quote_file_name);
  console.log(window.devicePixelRatio);


function makePDF() {

    var quotes = document.getElementById('main_content');

    html2canvas(quotes, {
        onrendered: function(canvas) {

        //! MAKE YOUR PDF
        var pdf = new jsPDF('p', 'pt', 'letter');

        for (var i = 0; i <= quotes.clientHeight/980; i++) {
            //! This is all just html2canvas stuff
            var srcImg  = canvas;
            var sX      = 0;
            var sY      = 980*i; // start 980 pixels down for every new page
            var sWidth  = 900;
            var sHeight = 980;
            var dX      = 0;
            var dY      = 0;
            var dWidth  = 900;
            var dHeight = 980;

            window.onePageCanvas = document.createElement("canvas");
            onePageCanvas.setAttribute('width', 900);
            onePageCanvas.setAttribute('height', 980);
            var ctx = onePageCanvas.getContext('2d');
            // details on this usage of this function: 
            // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Slicing
            ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);

            // document.body.appendChild(canvas);
            var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

            var width         = onePageCanvas.width;
            var height        = onePageCanvas.clientHeight;

            //! If we're on anything other than the first page,
            // add another page
            if (i > 0) {
                pdf.addPage(612, 791); //8.5" x 11" in pts (in*72)
            }
            //! now we declare that we're working on that page
            pdf.setPage(i+1);
            //! now we add content to that page!
            pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width*.62), (height*.62));

        }
        //! after the for loop is finished running, we save the pdf.
        pdf.save(quote_file_names);
    }
  });
}

</script>

</head>
<body>


<div id="main_content">

<cfoutput query="this_quote">

      <div class="row color-invoice">
      <div class="col-6">
        <h5>Quote Refrence##: LDI-UK-#id#</h5>
      </div>
      <div class="col-6 text-right">
        <p class="lead">#quote_dated#</p>
      </div>
      <div class="col-12">
        <div class="row">
          <div class="col-lg-7 col-md-7 col-sm-7">
            <h1>LDI QUOTE</h1>
            <br />
            <strong>Email : </strong> info@ldi.co.uk
            <br />
            <strong>Call : </strong> +44 161 480 9625
          </div>
          <div class="col-lg-5 col-md-5 col-sm-5">

            <h2>Liquid Dynamics International</h2> 
            716 Greg Street, Reddish
            <br> Manchester, UK.
            <br> SK5 LDI

          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-lg-7 col-md-7 col-sm-7">
            <h3>Client Details : </h3>
            <h5>#URLDecode(first_name)# - #URLDecode(last_name)#</h5> 789/90 , Lane Here, New York,
            <br /> United States
          </div>
          <div class="col-lg-5 col-md-5 col-sm-5">
            <h3>Client Contact :</h3> Mob: +1-99-88-77-55
            <br> email: info@domainname.com
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <strong>ITEM DESCRIPTION & DETAILS :</strong>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="table-responsive">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>S. No.</th>
                    <th>Perticulars</th>
                    <th>Quantity.</th>
                    <th>Unit Price</th>
                    <th>Sub Total</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Website Design</td>
                    <td>1</td>
                    <td>5000 USD</td>
                    <td>5000 USD</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Website Development</td>
                    <td>2</td>
                    <td>5000 USD</td>
                    <td>10000 USD</td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Customization</td>
                    <td>1</td>
                    <td>4000 USD</td>
                    <td>4000 USD</td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Plugin Setup</td>
                    <td>1</td>
                    <td>3000 USD</td>
                    <td>3000 USD</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <hr>
            <div>
              <h4>  Total : 22000 USD </h4>
            </div>
            <hr>
            <div>
              <h4>  Taxes : 4400 USD ( 20 % on Total Bill ) </h4>
            </div>
            <hr>
            <div>
              <h3>  Bill Amount : 26400 USD </h3>
            </div>
            <hr />
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <strong> Important: </strong>
            <ol>
              <li>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.

              </li>
              <li>
                Nulla eros eros, laoreet non pretium sit amet, efficitur eu magna.
              </li>
              <li>
                Curabitur efficitur vitae massa quis molestie. Ut quis porttitor justo, sed euismod tortor.
              </li>
            </ol>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <a href="##" class="btn btn-success btn-sm">Print Invoice</a>    
            <a href="##" class="btn btn-info btn-sm">Download In Pdf</a>
          </div>
        </div>
        
        <hr>
        <div class="row">
  

</div>
      </div>
    </div>

  </div>

</cfoutput>
<hr data-html2canvas-ignore="true">

  <div class="col-12 text-center" data-html2canvas-ignore="true">

    <p class="lead">Once your happy with your PDF preview click the button below to generate you PDF</p>

    <a href="javascript:makePDF()" class="btn btn-success">Generate PDF</a>

  </div>

</div>
</body>
</html>