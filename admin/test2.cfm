<!---
    Create a query with sibling rows that have matching data. We'll
    use the GROUP attribute to condence similar rows to a single
    outer row of our query.
    NOTE: We are using the new QueryNew() features of ColdFusion 10
    to populate the new query.
--->
<cfset friends = queryNew(
    "id, name, gender",
    "cf_sql_integer, cf_sql_varchar, cf_sql_varchar",
    [
        [ 1, "Sarah", "Female" ],
        [ 2, "Tricia", "Female" ],
        [ 3, "Joanna", "Female" ],
        [ 4, "Arnold", "Male" ],
        [ 5, "Vin", "Male" ],
        [ 6, "Vin", "Female" ]
    ]
) />


<cfoutput>

    You have #friends.recordCount# friends!<br />

    <!---
        Loop over the friends, but output them by adjacent gender
        (using the GROUP attribute). This way, we can output the
        Women and then the Men.
    --->
    <cfloop query="friends" group="name">

        <br />
        #friends.name#:
        <br />

        <!---
            Since we are in a Grouped query, we need to run an inner-
            cfloop to output the actual grouped rows. Exclude the
            NAME attribute since we want this CFLoop tag to connect
            with the outer, grouped tag.
        --->
        <cfloop>

            - #friends.name#<br />

        </cfloop>

    </cfloop>

</cfoutput>