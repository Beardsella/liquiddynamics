<cfoutput query="this_quote">
  <h5>Terms of Payment</h5>
  <p>#URLDecode(this_quote.terms_of_payment)#</p>

  <p>This quotation is valid untill (#DateFormat(quote_vaild_for, "DD-MM-YYYY")#)</p>

  <h3>#URLDecode(this_quote.notes)#</h3>

  <div><p>#URLDecode(this_quote.summary)#</p></div>

  <p>Best Regards<br><br><br><br>
  For and on behalf of<br>
  #URLDecode(this_quote.seller)# Limited</p>
</cfoutput>