<!DOCTYPE html>
<html lang="en">
<head>

	<cfinclude template="includes/default_header.cfm">

	<title>Pump System Enquirys - Liquid Dynamics</title>

	<!-- CSS for autocomplete -->
	<link rel="stylesheet" href="css/jquery-ui-1.12.1.css">

    <cfquery name="enquirys" datasource="agbcodes_ldi">
        SELECT *
        FROM enquirys_pump_systems
        WHERE actioned = 0
    </cfquery>

    <cfquery name="all_enquirys" datasource="agbcodes_ldi">
        SELECT *
        FROM enquirys_pump_systems
    </cfquery>

    <cfquery name="wip_enquirys" datasource="agbcodes_ldi">
        SELECT *
        FROM enquirys_pump_systems
        WHERE actioned = 2
    </cfquery>

    <cfquery name="customer_enquirys" datasource="agbcodes_ldi">
        SELECT *
        FROM enquirys_pump_systems
        WHERE actioned = 3
    </cfquery>

    <cfquery name="completed_enquirys" datasource="agbcodes_ldi">
        SELECT *
        FROM enquirys_pump_systems
        WHERE actioned = 1 OR actioned = 4
    </cfquery>

    <cfset enquirys_needing_attention = #enquirys.recordcount# + #wip_enquirys.recordcount#>

</head>
<body>

	<cfinclude template="includes/nav.cfm">

	<div class="container-fluid">

	    <!-- Start of the main page content -->
        <h1 class="page-header">Pump System Enquirys</h1>
        <p class="lead">These are enquiry forms filled out by customers that havent been marked as actioned. They can be sorted by column and clicked on for more information. There are <cfoutput><span class="badge badge-pill badge-primary" title="Enquirys needing attention">#enquirys_needing_attention#</span></cfoutput> enquirys needing attention.</p> <p class="lead">This is a link to the <a href="customer-enquiry.cfm" target="_blank">main enquiry form</a></strong></p>

		<cfoutput>

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">

				<li class="nav-item"><a href="##new_enquirys" class="nav-link active" aria-controls="new_enquirys" role="tab" data-toggle="tab">
				<span class="badge badge-warning">New Enquirys = #enquirys.recordcount#</span>
				</a></li>

				<li class="nav-item"><a href="##wip_enquirys" class="nav-link" aria-controls="wip_enquirys" role="tab" data-toggle="tab">
				<span class="badge badge-danger">In Progress Enquirys = #wip_enquirys.recordcount#</span></a>
				</li>

				<li class="nav-item"><a href="##customer_enquirys" class="nav-link" aria-controls="customer_enquirys" role="tab" data-toggle="tab">
				<span class="badge badge-info">Wating on customer = #customer_enquirys.recordcount#</span></a>
				</li>

				<li class="nav-item"><a href="##completed" class="nav-link" aria-controls="completed" role="tab" data-toggle="tab">
				<span class="badge badge-success">Completed = #completed_enquirys.recordcount#</span>
				</a></li>

				<li class="nav-item"><a href="##all_enquirys" class="nav-link" aria-controls="all_enquirys" role="tab" data-toggle="tab">
				<span class="badge badge-primary">All Enquirys = #all_enquirys.recordcount#</span>
				</a></li>

			</ul>

		</cfoutput>

		<div class="tab-content">

			<!-- New Enquirys -->
			<div role="tabpanel" class="tab-pane active" id="new_enquirys">

				<table class="table table-striped table-dark" id="ss_table">

			        <thead>
			            <th>#</th>
			            <th>Name</th>
			            <th>Email</th>
			            <th>Pump Type</th>
			            <th>Flow Rate</th>
			            <th>Pumped Liquid</th>
			            <th>Enquiry Date</th>
			            <th></th>
			            <th></th>
			            <th></th>
			        </thead>

			        <tbody>
			            <cfoutput query="enquirys">

						    <cfquery name="this_contact" datasource="agbcodes_ldi">
						        SELECT first_name, last_name, email
						        FROM contacts
						        WHERE id = '#contact_id#'
						    </cfquery>

			                <tr>
			                	<td>#id#</td>
			                    <td><a href="customer-enquiry-edit.cfm?id=#id#" class="agb-table-link">#URLDecode(this_contact.first_name)# #URLDecode(this_contact.last_name)#</a></td>
			                    <td>#URLDecode(this_contact.email)#</td>
			                    <td>#URLDecode(pump_type)#</td>
			                    <td>#flow_rate# #URLDecode(flow_rate_units)#</td>
			                    <td>#URLDecode(pumped_liquid)#</td>
			                    <td>#DateFormat("#enquiry_date#", "dd / mmm / yyyy")#</td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=wip" class="btn btn-outline-warning btn-sm">In progress</a></td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=customer" class="btn btn-outline-info btn-sm">Customer</a></td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=finished" class="btn btn-outline-success btn-sm">Finished</a></td>
		                    </tr>

			            </cfoutput>
			        </tbody>                

		        </table>

		    </div>
			<!-- End of Tab -->

			<!-- WIP Enquirys -->
			<div role="tabpanel" class="tab-pane" id="wip_enquirys">

				<table class="table table-striped table-dark" id="ss_table_1">

			        <thead>
			        	<th>#</th>
			            <th>Name</th>
			            <th>Email</th>
			            <th>Pump Type</th>
			            <th>Flow Rate</th>
			            <th>Pumped Liquid</th>
			            <th>Enquiry Date</th>
			            <th></th>
			            <th></th>
			            <th></th>
			        </thead>

			        <tbody>
			            <cfoutput query="wip_enquirys">

						    <cfquery name="wip_contacts" datasource="agbcodes_ldi">
						        SELECT first_name, last_name, email
						        FROM contacts
						        WHERE id = '#contact_id#'
						    </cfquery>			                

    						<tr>
    							<td>#id#</td>
			                    <td><a href="customer-enquiry-edit.cfm?id=#id#" class="agb-table-link">#URLDecode(wip_contacts.first_name)# #URLDecode(wip_contacts.last_name)#</a></td>
			                    <td>#URLDecode(wip_contacts.email)#</td>
			                    <td>#URLDecode(pump_type)#</td>
			                    <td>#flow_rate# #URLDecode(flow_rate_units)#</td>
			                    <td>#URLDecode(pumped_liquid)#</td>
			                    <td>#DateFormat("#enquiry_date#", "dd / mmm / yyyy")#</td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=customer" class="btn btn-outline-info btn-sm">Customer</a></td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=finished" class="btn btn-outline-success btn-sm">Finished</a></td>
			                </tr>
			            </cfoutput>
			        </tbody>                

		        </table>

		    </div>
			<!-- End of Tab -->			

			<!-- Wating on customer Enquirys -->
			<div role="tabpanel" class="tab-pane" id="customer_enquirys">

				<table class="table table-striped table-dark" id="ss_table_4">

			        <thead>
			        	<th>#</th>
			            <th>Name</th>
			            <th>Email</th>
			            <th>Pump Type</th>
			            <th>Flow Rate</th>
			            <th>Pumped Liquid</th>
			            <th>Enquiry Date</th>
			            <th></th>
			            <th></th>
			            <th></th>
			        </thead>

			        <tbody>
			            <cfoutput query="customer_enquirys">

						    <cfquery name="customer_contacts" datasource="agbcodes_ldi">
						        SELECT first_name, last_name, email
						        FROM contacts
						        WHERE id = '#contact_id#'
						    </cfquery>			                

    						<tr>
    							<td>#id#</td>
			                    <td><a href="customer-enquiry-edit.cfm?id=#id#" class="agb-table-link">#URLDecode(customer_contacts.first_name)# #URLDecode(customer_contacts.last_name)#</a></td>
			                    <td>#URLDecode(customer_contacts.email)#</td>
			                    <td>#URLDecode(pump_type)#</td>
			                    <td>#flow_rate# #URLDecode(flow_rate_units)#</td>
			                    <td>#URLDecode(pumped_liquid)#</td>
			                    <td>#DateFormat("#enquiry_date#", "dd / mmm / yyyy")#</td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=wip" class="btn btn-outline-warning btn-sm">In progress</a></td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=finished" class="btn btn-outline-success btn-sm">Finished</a></td>
			                </tr>

			            </cfoutput>
			        </tbody>                

		        </table>

		    </div>
			<!-- End of Tab -->

			<!-- Completed Enquirys -->
			<div role="tabpanel" class="tab-pane" id="completed">

				<table class="table table-striped table-dark" id="ss_table_2">

			        <thead>
			        	<th>#</th>
			            <th>Name</th>
			            <th>Email</th>
			            <th>Pump Type</th>
			            <th>Flow Rate</th>
			            <th>Pumped Liquid</th>
			            <th>Enquiry Date</th>
			            <th></th>
			            <th></th>
			            <th></th>
			        </thead>

			        <tbody>
			            <cfoutput query="completed_enquirys">

						    <cfquery name="completed_contacts" datasource="agbcodes_ldi">
						        SELECT first_name, last_name, email
						        FROM contacts
						        WHERE id = '#contact_id#'
						    </cfquery>			      

			                <tr>
			                	<td>#id#</td>
			                    <td><a href="customer-enquiry-edit.cfm?id=#id#" class="agb-table-link">#URLDecode(completed_contacts.first_name)# #URLDecode(completed_contacts.last_name)#</a></td>
			                    <td>#URLDecode(completed_contacts.email)#</td>
			                    <td>#URLDecode(pump_type)#</td>
			                    <td>#flow_rate# #URLDecode(flow_rate_units)#</td>
			                    <td>#URLDecode(pumped_liquid)#</td>
			                    <td>#DateFormat("#enquiry_date#", "dd / mmm / yyyy")#</td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=finished" class="btn btn-outline-warning btn-sm">In progress</a></td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=finished" class="btn btn-outline-info btn-sm">Customer</a></td>
			                </tr>

			            </cfoutput>
			        </tbody>                

		        </table>

		    </div>

			<!-- All Enquirys -->
			<div role="tabpanel" class="tab-pane" id="all_enquirys">

				<table class="table table-striped table-dark" id="ss_table_3">

			        <thead>
			        	<th>#</th>
			            <th>Name</th>
			            <th>Email</th>
			            <th>Pump Type</th>
			            <th>Flow Rate</th>
			            <th>Pumped Liquid</th>
			            <th>Enquiry Date</th>
			            <th>Status</th>
			            <th></th>
			            <th></th>
			            <th></th>
			        </thead>

			        <tbody>
			            <cfoutput query="all_enquirys">

						    <cfquery name="all_enq_contacts" datasource="agbcodes_ldi">
						        SELECT first_name, last_name, email
						        FROM contacts
						        WHERE id = '#contact_id#'
						    </cfquery>			            

							<cfparam name="style" default="">
							<cfparam name="enquiry_date" default="">
							
							<cfif actioned eq 0>
								<cfset style="danger">
							<cfelseif actioned eq 2>
								<cfset style="warning">
							<cfelseif actioned eq 3>
								<cfset style="info">
							<cfelseif actioned eq 4>
								<cfset style="success">
							</cfif>

			                <tr class="#style#">
			                	<td>#id#</td>
			                    <td><a href="customer-enquiry-edit.cfm?id=#id#" class="agb-table-link">#URLDecode(all_enq_contacts.first_name)# #URLDecode(all_enq_contacts.last_name)#</a></td>
			                    <td>#URLDecode(all_enq_contacts.email)#</td>
			                    <td>#URLDecode(pump_type)#</td>
			                    <td>#flow_rate# #URLDecode(flow_rate_units)#</td>
			                    <td>#URLDecode(pumped_liquid)#</td>
			                    <td>#DateFormat("#enquiry_date#", "dd / mmm / yyyy")#</td>
			                    <td>#URLDecode(action_description)#</td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=wip" class="btn btn-outline-warning btn-sm">In progress</a></td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=customer" class="btn btn-outline-info btn-sm">Customer</a></td>
			                    <td><a href="processing_files/enquiry_status_update.cfm?id=#id#&status=finished" class="btn btn-outline-success btn-sm">Finished</a></td>
		                    </tr>

			            </cfoutput>
			        </tbody>                

		        </table>

		    </div>

		</div>
		<!-- End of Tab -->
		</div>        

	<cfinclude template="includes/footer.cfm">
	<cfinclude template="includes/java_include.cfm">

	<!-- Extra table sorting for 4 extra tables -->
	<script type="text/JavaScript">
		$(document).ready(function(){ 
			$("#ss_table_1, #ss_table_2, #ss_table_3, #ss_table_4").tablesorter(); 
		}); 
	</script>

</body>
</html>