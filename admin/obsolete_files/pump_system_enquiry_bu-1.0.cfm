<!DOCTYPE html>
<html lang="en">
<head>

  <!-- blank_template.cfm Ver: 1.0.0 -->
  <cfinclude template="includes/default_header.cfm">

    <title>Pump System Enquiry - Liquid Dynamics Group Ltd</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/thumbnail-gallery.css" rel="stylesheet">

    <!-- Start of autocomplete CSS-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <!-- End of autocomplete CSS-->

    <cfquery name="pumps" datasource="agbcodes_ldi">
        SELECT *
        FROM wip_pumps
    </cfquery>

    <cfquery name="metals" datasource="agbcodes_ldi">
        SELECT *
        FROM wip_metals
    </cfquery>

    <cfquery name="plastics" datasource="agbcodes_ldi">
        SELECT *
        FROM wip_plastics
    </cfquery>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.cfm">Liquid Dynamics Group Ltd</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="enquirys_pump_systems.cfm">Pump System Enquiry</a>
                    </li>
                    <li>
                        <a href="customer-enquiry-overview.cfm">Enquiry Overview</a>
                    </li>
                    <li>
                        <a href="companies_overview.cfm">Companies</a>
                    </li>                    
                    <li>
                        <a href="contacts_overview.cfm">Contacts</a>
                    </li>
                    <li>
                        <a href="login.cfm">Login</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <h1 class="page-header">Pump System Enquiry</h1>
                <p class="lead">This information will be used to help determine the correct damper for your system. <strong>If you cannot answer all the questions below or are unsure then please contact us or leave blank and we will contact you and assist.</strong> <a href="product_photos/Slurry-hose-dampener.gif">Example gif</a> to demonstrate.</p>
            </div>

          <form class="form-horizontal" action="processing_files/enquirys_pump_systems_insert.cfm" method="post" enctype="multipart/form-data">

            <div class="col-lg-12">
            
                <div class="form-group">
                    <div class="col-lg-6">
                      <label for="enquiry_name">Name</label>
                      <input type="text" class="form-control" id="enquiry_name" name="enquiry_name" placeholder="Joe Bloggs">
                    </div>

                    <div class="col-lg-6">
                      <label for="enquiry_email">Email</label>
                      <input type="email" class="form-control" id="enquiry_email" name="enquiry_email" placeholder="joe.bloggs@example.com">
                    </div>
                </div>  

                <!-- Type of Pump (Centrif, Hose, Diaphragm, Metering etc) -->
                <div class="form-group">
                   <label for="pump_type">Type of Pump</label>
                    <select id="pump_type" name="pump_type" class="form-control">
                      <option value=""></option>
                      <cfoutput query="pumps">
                          <option value="#pump_name#">#URLDecode(pump_name)#</option>
                        </cfoutput>
                      <option value="Other">Other</option>
                    </select>
                </div>
                
                <!-- Number of Displacers (Simplex, Duplex, Triplex etc) -->
                <div class="form-group">
                   <label for="num_of_displacers">Number of Displacers</label>
                    <select id="num_of_displacers" name="num_of_displacers" class="form-control">
                      <option value=""></option>
                      <option value="Simplex">Simplex</option>
                      <option value="Duplex">Duplex</option>
                      <option value="Triplex">Triplex</option>
                      <option value="Other">Other</option>
                    </select>
                </div>

                <!-- Flow Rate (Litres per minute or per hour) -->
                <div class="form-group">
                    <label for="flow_rate">Flow Rate</label>
                    <div class="clearfix"></div>
                    <div class="col-lg-6">
                          <input type="number" class="form-control" id="flow_rate" name="flow_rate" placeholder="666">
                    </div>
                    <div class="col-lg-6">
                        <select id="flow_rate_units" name="flow_rate_units" class="form-control">
                          <option value=""></option>
                          <option value="Litres per minute">Litres per minute</option>
                          <option value="Litres per hour">Litres per hour</option>
                        </select>
                    </div>
                </div>
        
                <!-- SPM or RPM -->
                <div class="form-group">
                   <label for="SPMorRPM">SPM (steps per minute) or RPM (revs per minute)</label>
                    <select id="SPMorRPM" name="SPMorRPM" class="form-control">
                      <option value=""></option>
                      <option value="SPM">SPM</option>
                      <option value="RPM">RPM</option>
                    </select>
                </div>
                
                <!-- Liquid being pumped (water, diesel, slurry etc) -->
                <div class="form-group">
                    <label for="pumped_liquid">Liquid being pumped (autocomplete)</label>
                    <input type="text" class="form-control" id="pumped_liquid" name="pumped_liquid">
                </div>  

                <!-- System Operating Temperature -->
                <div class="form-group">
                  <label for="system_temp">System Operating Temperature (degrees celcius)</label>
                  <input type="number" class="form-control" id="system_temp" name="system_temp">
                </div>

                <!-- System Design Temperature -->
                <div class="form-group">
                  <label for="system_design_temp">System Design Temperature (degrees celcius)</label>
                  <input type="number" class="form-control" id="system_design_temp" name="system_design_temp">
                </div>
                
                <!-- System Maximum Working Pressure -->
                <div class="form-group">
                  <label for="max_system_pressure">System Maximum Working Pressure (PSI)</label>
                  <input type="number" class="form-control" id="max_system_pressure" name="max_system_pressure">
                </div>
                
                <!-- System Design Pressure -->
                <div class="form-group">
                  <label for="system_design_pressure">System Design Pressure (PSI)</label>
                  <input type="number" class="form-control" id="system_design_pressure" name="system_design_pressure">
                </div>
                
                <!-- Pipework Size -->
                <div class="form-group">
                  <label for="pipe_size">Pipework Size</label>
                  <input type="text" class="form-control" id="pipe_size" name="pipe_size">
                </div>
                
                <!-- Connection Type Required (Flange, NPT, Tri Clamp etc) -->
                <div class="form-group">
                   <label for="connection_type">Connection Type Required</label>
                    <select id="connection_type" name="connection_type" class="form-control">
                      <option value=""></option>
                      <option value="Flange">Flange</option>
                      <option value="NPT">NPT</option>
                      <option value="Tri Clamp">Tri Clamp</option>
                      <option value="Other">Other</option>
                    </select>
                </div>

                <!-- Single Connection or Twin Flow-Through Required -->
                <div class="form-group">
                   <label for="connection_type_2">Single Connection or Twin Flow-Through Required</label>
                    <select id="connection_type_2" name="connection_type_2" class="form-control">
                      <option value=""></option>
                      <option value="Single Connection">Single Connection</option>
                      <option value="Twin Flow-Through">Twin Flow-Through</option>
                    </select>
                </div>

                <!-- System Materials (Metals Stainless, Carbon steel etc. Elastomers  Viton, Nitrile etc) -->
                <div class="form-group">
                   <label for="metalic_system_materials">Metalic System Materials</label>
                    <select id="metalic_system_materials" name="metalic_system_materials" class="form-control">
                      <option value=""></option>
                      <cfoutput query="metals">
                          <option value="#col_21#">#URLDecode(col_21)#</option>
                        </cfoutput>
                    </select>
                </div>

                <!-- System Materials (Metals Stainless, Carbon steel etc. Elastomers  Viton, Nitrile etc) -->
                <div class="form-group">
                   <label for="plastic_system_materials">Elastomer System Materials</label>
                    <select id="plastic_system_materials" name="plastic_system_materials" class="form-control">
                      <option value=""></option>
                      <cfoutput query="plastics">
                          <option value="#col_18#">#URLDecode(col_18)#</option>
                        </cfoutput>
                    </select>
                </div>

                <button type="submit" class="btn btn-success btn-block">Send Message</button>
              </form>
            </div>                

        </div>

        <hr>

      <!-- Footer -->
      <footer>
          <div class="row">
              <div class="col-lg-12">
                  <p>Copyright &copy; Liquid Dynamics Group Ltd 2018</p>
              </div>
          </div>
      </footer>

  </div>
  <!-- /.container -->

  <!-- jQuery -->
  <script src="js/jquery.js"></script>

  <!-- Bootstrap Core JavaScript -->
  <script src="js/bootstrap.min.js"></script>

  <!-- Start of autocomplete -->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    var availableTags = [
      "Water",
      "Diesel",
      "Slurry"
    ];
    $( "#pumped_liquid" ).autocomplete({
      source: availableTags
    });
  } );
  </script>
  <!-- End of autocomplete -->

</body>
</html>