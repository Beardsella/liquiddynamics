<!DOCTYPE html>
<html lang="en">
<head>

  <cfinclude template="includes/default_header.cfm">

  <title>Liquid Dynamics</title>

<cfif isDefined("FORM.part_refrence")>


  <cfquery name="diffrent_parts" datasource="agbcodes_ldi">
      SELECT DISTINCT refrence
      FROM parts
      WHERE refrence = "#FORM.part_refrence#"
  </cfquery>

<cfelse>

  <cfquery name="diffrent_parts" datasource="agbcodes_ldi">
      SELECT DISTINCT refrence
      FROM parts
  </cfquery>

</cfif>

</head>
<body>

  <cfinclude template="includes/nav.cfm">

  <!-- Page Content -->
  <div class="container">

    <div class="row">
                
        <div class="col-lg-12">
          <cfoutput>
            <cfif isDefined("FORM.part_refrence")>
              <h1 class="page-header">Replacement Part "#diffrent_parts.refrence#"</h1>
            <cfelse>
              <h1 class="page-header">Replacement Parts (#diffrent_parts.recordcount#)</h1>
            </cfif>

            </cfoutput>
          <p class="lead">A list of replacement parts and the usual cost.</p>

   <form class="form" action="parts.cfm" method="post" enctype="multipart/form-data">

  <div class="input-group">
    <input list="parts" type="text" class="form-control" id="part_refrence" name="part_refrence" placeholder="Search for Part" onchange="showCompany(this.value)">
    
  <div class="input-group-append">
    <button class="btn btn-outline-success" type="submit">Search</button>
  </div>
  </div>            

            <datalist id="parts">
              <cfoutput query="diffrent_parts">
                <option value="#refrence#">
              </cfoutput>
            </datalist>
</form>
        </div>


        <table class="table table-striped" id="ss_table">
          <thead>
            <tr>
              <th scope="col">Membrane</th>
              <th scope="col">Average Price Each</th>
              <th scope="col">Quantity Sold</th>
            </tr>
          </thead>
          <tbody>
            <cfoutput query="diffrent_parts">

              <cfquery name="current_part_qty" datasource="agbcodes_ldi">
                SELECT SUM(qty) AS TotalSold        
                FROM parts
                WHERE refrence = "#diffrent_parts.refrence#"
              </cfquery>

              <cfquery name="current_part_price" datasource="agbcodes_ldi">
                SELECT AVG(pr_ea) AS AveragePrice          
                FROM parts
                WHERE refrence = "#diffrent_parts.refrence#"
              </cfquery>

              <tr>
                <td scope="row">#refrence#</td>
                <td> &pound;#NumberFormat(current_part_price.AveragePrice, 0.00)#</td>
                <td>#current_part_qty.TotalSold#</td>
              </tr>
            </cfoutput>
          </tbody>
        </table>

    </div>

  </div>
  
  <hr>
  <!-- End of page main content -->

  <cfinclude template="includes/footer.cfm">
  <cfinclude template="includes/java_include.cfm">

  <script type="text/JavaScript">
    $(document).ready(function(){ 
      $("#ss_table").tablesorter(); 
    }); 
  </script>

</body>
</html>