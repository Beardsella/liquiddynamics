<table class="table table-hover table-striped">

  <tr>
    <th>Name</th>
    <th>Company</th>
    <th>Staff Role</th>
    <th>Telephone</th>
    <th>Email</th>
  </tr>

  <cfoutput query="staff">
  
    <tr>
      <td><a href="contact_detail.cfm?id=#id#">#URLDecode(staff.first_name)# #URLDecode(staff.last_name)#</a></td>
      <td><a href="company_detail.cfm?company=#URLEncodedFormat(staff.company)#">#URLDecode(staff.company)#</a></td>
      <td>#URLDecode(staff.staff_role)#</td>
      <td>#URLDecode(staff.telephone)#</td>
      <td>#URLDecode(staff.email)#</td>
    </tr>

  </cfoutput>
  
</table>