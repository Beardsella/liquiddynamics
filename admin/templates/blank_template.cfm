<!DOCTYPE html>
<html lang="en">
<head>

  <!-- blank_template.cfm Ver: 1.0.0 -->

  <cfinclude template="default_header.cfm">

  <!--Set the CurrentPage Var-->
  <cfset CurrentPage = GetFileFromPath(GetTemplatePath())>

  <cfquery name="this_page">
    SELECT pages.*, page_vs_template.group_id
    FROM pages
    Left JOIN page_vs_template
    ON pages.id=page_vs_template.page_id
    WHERE page_url LIKE "#ListLast(cgi.script_name, '/' )#"
    ORDER BY page_title
  </cfquery>

  <meta name="description" content="Discover the ShakeSphere, the latest innovation in the protein shaker bottle world. Practical and easily cleaned. Personalise your own ShakeSphere now!">
  <meta name="keywords" content="shakesphere,shaker bottle,shakesphere shaker bottle">
  <meta name="author" content="Adam Beardsell">
  <title>ShakeSphere.co</title>



</head>
<body>

  <cfinclude template="nav.cfm">

  <div class="col-md-12">
    <cfoutput>
      #URLDecode(this_page.main_content)#
    </cfoutput>
  </div>

  <cfinclude template="footer.cfm">
  <cfinclude template="java_include.cfm">

</body>

<script type="text/javascript" id="cookiebanner" src="http://cookiebanner.eu/js/cookiebanner.min.js"></script>

</html>