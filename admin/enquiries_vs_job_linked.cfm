<table class="table table-striped table-dark" id="ss_table">

  <thead>
      <th>#</th>
      <th>Company</th>
      <th>Contact</th>
      <th>Email</th>
      <th>Enquiry Date</th>
  </thead>

  <tbody>

    <cfoutput query="enquiries">

      <cfif isDefined("enquiries.seller") AND enquiries.seller neq "">
        <cfset selling_company = "#URLDecode(seller)#">

        <cfif selling_company eq "Liquid Dynamics International">
          <cfset company_prefix = "LD-UK-ENQ">
        <cfelseif selling_company eq "Pulse Gard">
          <cfset company_prefix = "PG-UK-ENQ">
        <cfelseif selling_company eq "Shock Gard">
          <cfset company_prefix = "SG-UK-ENQ">
        <cfelseif selling_company eq "Hydrotrole">
          <cfset company_prefix = "HT-UK-ENQ">
        </cfif>

      </cfif>

      <cfquery name="this_contact" datasource="agbcodes_ldi">
          SELECT first_name, last_name, email
          FROM contacts
          WHERE id = '#contact_id#'
      </cfquery>

      <tr>
        <td><a href="enquiry_management.cfm?id=#id#" class="btn btn-block btn-outline-info" title="Click to view enquiry">#formatted_id#</a></td>
          <td><a href="company_detail.cfm?id=#company_id#" class="agb-table-link" title="Click to view company details">#URLDecode(this_company.company_name)#</a></td>
          <td><a href="contact_detail.cfm?id=#contact_id#" class="agb-table-link">#URLDecode(this_contact.first_name)# #URLDecode(this_contact.last_name)#</a></td>
          <td>#URLDecode(this_contact.email)#</td>
          <td>#DateFormat("#enquiry_date#", "dd / mmm / yyyy")#</td>
        </tr>

      </cfoutput>
      
  </tbody>                

</table>