<!DOCTYPE html>
<html lang="en">
<head>

  <cfinclude template="includes/default_header.cfm">

  <title>Create Quote - Liquid Dynamics Group Ltd</title>

  <!-- CSS for autocomplete -->
  <link rel="stylesheet" href="css/jquery-ui-1.12.1.css">

  <cfquery name="companies" datasource="agbcodes_ldi">
      SELECT distinct(company_name)
      FROM companies
      ORDER BY company_name
  </cfquery>

</head>

<body>

  <cfinclude template="includes/nav.cfm">

  <!-- Page Content -->
  <div class="container">

  <div class="col-12">
      <h1 class="page-header" id="content">Create a Quote</h1>
      <p class="lead">To creat a Quote you will first have to specify a customer and which company is selling the product.</p>
  </div>

  <form class="form-horizontal" autocomplete="off" action="processing_files/quote_create.cfm" method="post">
<fieldset>

 <h3>Seller</h3>
      <hr>

          <div class="form-group">
          <label for="seller">Selling Company</label>
            <input list="sellers_list" autocomplete="off" type="text" class="form-control" id="seller" name="seller" placeholder="Company">
          </div>

          <datalist id="sellers_list">
            <option value="Liquid Dynamics International">
            <option value="Pulse Gard">
            <option value="Shock Gard">
            <option value="Hydrotrole">
          </datalist> 

<h3>Company Details</h3>
        <hr>

            <div class="form-group">
            <label for="company_name">Company Name</label>
              <input list="compaines" autocomplete="off" type="text" class="form-control" id="company_name" name="company_name" placeholder="Company" onchange="showCompany(this.value)">
            </div>

            <datalist id="compaines">
              <cfoutput query="companies">
                <option value="#URLDecode(company_name)#">
              </cfoutput>
            </datalist>

          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#company_details" aria-expanded="false" aria-controls="company_details">More Company Details...</button>
        
        <div class="collapse" id="company_details">

          <div class="row">
              <div class="col-lg-6">
              <div class="form-group">
              <label for="company_email">Email Address</label>
                <input type="email" class="form-control" id="company_email" name="company_email" placeholder="Email Address">
                </div>
              </div>

              <div class="col-lg-6">
              <div class="form-group">
              <label for="company_telephone">Phone Number</label>
                <input type="text" class="form-control" id="company_telephone" name="company_telephone" placeholder="Phone Number">
                </div>
              </div>
          </div> 

          <div class="row">
              <div class="col-lg-12">
              <div class="form-group">
              <label for="company_address">Address</label>
                <input type="text" class="form-control" id="company_address" name="company_address" placeholder="Email Address">
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                <label for="company_town">Town</label>
                  <input type="text" class="form-control" id="company_town" name="company_town" placeholder="Town">
                  </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                <label for="company_postcode">Post Code</label>
                  <input type="text" class="form-control" id="company_postcode" name="company_postcode" placeholder="Post Code">
                  </div>
              </div>

              <div class="col-lg-12">
                <div class="form-group">
                <label for="company_country">Country</label>
                  <input type="text" class="form-control" id="company_country" name="company_country" placeholder="Country">
                  </div>
              </div>

              <div class="col-lg-12">
                <div class="form-group">
                <label for="company_notes">Company Notes</label>
                  <textarea class="form-control" id="company_notes" name="company_notes"></textarea>
                </div>
              </div>

          </div> 

        </div>

        <h3>Contact Details</h3>
        <hr>

<span id="contact_form">
    
        <div class="row">
           
            <div class="col-lg-6">
              <div class="form-group">
              <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Joe">
                </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
              <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Bloggs">
              </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_email">Email Address</label>
              <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="Email Address" required>
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_telephone">Phone Number</label>
              <input type="text" class="form-control" id="contact_telephone" name="contact_telephone" placeholder="Phone Number" required>
              </div>
            </div>
        </div> 

          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#contact_details" aria-expanded="false" aria-controls="contact_details">
            More Contact Details...
          </button>
        

        <div class="collapse" id="contact_details">
          <div class="row">
              <div class="col-lg-6">
              <div class="form-group">
              <label for="staff_role">Staff Role</label>
                <input type="text" class="form-control" id="staff_role" name="staff_role" placeholder="Purchasing Manager">
                </div>
              </div>

              <div class="col-lg-6">
              <div class="form-group">
              <label for="contact_country_based">Country Based</label>
                <input type="text" class="form-control" id="contact_country_based" name="contact_country_based" placeholder="UK">
                </div>
              </div>
          </div> 

          <div class="form-group">
            <label for="contact_notes">Contact Notes</label>
            <textarea class="form-control" id="contact_notes" name="contact_notes"></textarea>
          </div>

        </div>

        </span>

<div class="row">
  <button type="submit" class="btn btn-success btn-block">Create a new quote for this customer</button>
</div>

</fieldset>
</form>


    </div>
    <!-- End of container -->

    <hr>

  <cfinclude template="includes/footer.cfm">
  <cfinclude template="includes/java_include.cfm">


  <!-- JS for autocomplete -->
  <script src="js/jquery-ui-1.12.1.min.js"></script>

<script>
function showCompany(str) {
    if (str == "") {
        document.getElementById("company").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("company_details").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","processing_files/ajax_company_details.cfm?q="+str,true);
        xmlhttp.send();  
    }
    if (str == "") {
        document.getElementById("contact_form").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("contact_form").innerHTML = this.responseText;
        }
    };
    xmlhttp.open("GET","processing_files/ajax_contact_details.cfm?q="+str,true);
    xmlhttp.send();

  }

}
</script>

<script>
function fill_in_contact(str) {
    if (str == "") {
        document.getElementById("last_name").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("contact_form").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","processing_files/ajax_contact_details_2.cfm?q="+str,true);
        xmlhttp.send(); 

        alert("Contact info found");
    }

}
</script>
  
</body>
</html>