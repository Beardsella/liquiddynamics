<cfquery name="quotes" datasource="agbcodes_ldi">
    SELECT *
    FROM quotes
</cfquery>

<table class="table table-striped table-dark" id="ss_table">


   <thead>
                  <th>#</th>
                  <th>Current Status</th>
                  <th>Client</th>
                  <th>Contact</th>
                  <th>Date Created</th>
                  <th>Quote Refrence Name</th>
                  <th>Linked Job</th>
                  <th>PDF's</th>
              </thead>

              <tbody>

                  <cfoutput query="quotes">

              <cfif isDefined("quotes.seller") AND quotes.seller neq "">

                <cfset selling_company = "#URLDecode(quotes.seller)#">

                <cfif selling_company eq "Liquid Dynamics International">
                  <cfset company_prefix = "LD-UK-ENQ-">
                <cfelseif selling_company eq "Pulse Gard">
                  <cfset company_prefix = "PG-UK-ENQ-">
                <cfelseif selling_company eq "Shock Gard">
                  <cfset company_prefix = "SG-UK-ENQ-">
                <cfelseif selling_company eq "Hydrotrole">
                  <cfset company_prefix = "HT-UK-ENQ-">
                </cfif>

              </cfif> 



                <cfquery name="quotes" datasource="agbcodes_ldi">
                    SELECT quotes.*
                    FROM quotes
                    RIGHT JOIN job_vs_quote 
                    ON quotes.id = job_vs_quote.quote_id
                    WHERE job_vs_quote.job_id = "#id#"
                </cfquery>

              <cfquery name="linked_job" datasource="agbcodes_ldi">
                  SELECT *
                  FROM jobs
                    WHERE quote_id = "#id#"                                   
              </cfquery>



                      <tr>
                        <td><a href="quote_management.cfm?id=#id#" class="agb-table-link" title="Click to view quote">#company_prefix##id#</a></td>

        <cfif "#URLDecode(q_status)#" neq "quoted">
          <td><a href="processing_files/quote_status_update.cfm?id=#id#&job=#all_jobs.id#&status=quoted" class="btn btn-outline-success btn-sm">Convert to Works Order</a></td>

        <cfelse>

          <td>
            <a href="works_order.cfm?id=#linked_wo_id#" class="badge badge-#current_badge#">Works Order - "#linked_wo_id#"</a>
          </td>

        </cfif>                        
                          <td><a href="company_detail.cfm?id=#company_id#" class="agb-table-link" title="Click to view company details">#URLDecode(company_name)#</a></td>
                          <td><a href="contact_detail.cfm?id=#contact_id#" class="agb-table-link">#URLDecode(first_name)# #URLDecode(last_name)#</a></td>
                          <td>#DateFormat("#quote_created#", "dd / mmm / yyyy")#</td>
                          <td><a href="quote_management.cfm?id=#id#" class="agb-table-link" title="Click to view quote">#URLDecode(cust_enquiry_ref)#</a></td>

                          <td>

                  <cfif isDefined("linked_job.job_title") AND linked_job.job_title neq "">
                    <a href="contract_details.cfm?id=#linked_job.id#" class="agb-table-link">#URLDecode(linked_job.job_title)#</a>
                  <cfelseif isDefined("linked_job.id") AND linked_job.id neq "">
                    <a href="contract_details.cfm?id=#linked_job.id#" class="agb-table-link">Job ###linked_job.id#</a>                 
                  <cfelse>
                    <a href="processing_files/job_create.cfm?quote_id=#id#" class="agb-table-link">Create a new job</a>
                  </cfif>

                          </td>
                        
                        <td><a href="quote_pdf_preview.cfm?id=#id#" class="agb-table-link" title="Click to view quote">PDF Preview</a></td>
                        </tr>

                  </cfoutput>
              </tbody>          

</table>