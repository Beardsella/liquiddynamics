<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <title>Liquid Dynamics Group Ltd</title>

    <cfquery name="all_jobs" datasource="agbcodes_ldi">
        SELECT *
        FROM jobs
    </cfquery>  

    <cfquery name="countries" datasource="agbcodes_ldi">
        SELECT *
        FROM countries
    </cfquery>

    <cfquery name="enquiries" datasource="agbcodes_ldi">
        SELECT enquirys_pump_systems.*
        FROM enquirys_pump_systems
        RIGHT JOIN job_vs_enq 
        ON enquirys_pump_systems.id = job_vs_enq.enquiry_id
        ORDER BY enquirys_pump_systems.enquiry_date
    </cfquery>

    <cfquery name="quotes" datasource="agbcodes_ldi">
        SELECT *
        FROM quotes
    </cfquery>

    <cfquery name="w_orders" datasource="agbcodes_ldi">
        SELECT *
        FROM works_orders
    </cfquery>

    <cfquery name="invoices" datasource="agbcodes_ldi">
        SELECT *
        FROM invoices
    </cfquery>

    <cfquery name="uploaded_docs" datasource="agbcodes_ldi">
        SELECT uploaded_docs.*
        FROM uploaded_docs
        RIGHT JOIN job_vs_docs 
        ON uploaded_docs.id = job_vs_docs.doc_id
    </cfquery>

</head>
<body>

    <cfinclude template="includes/nav.cfm">

      <!-- Page Content -->
      <main role="main" class="container-fluid">

            <cfoutput>
        

                <div class="col-lg-12">
                    <h1 class="page-header">Current Work Overview</h1>
                    <p class="lead">An overview of active work</p>

  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item active">
      <a class="nav-link" id="contracts-tab" data-toggle="tab" href="##contracts" role="tab" aria-controls="contracts" aria-selected="false">contracts <span class="badge badge-light">#all_jobs.RecordCount#</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="quote-tab" data-toggle="tab" href="##quotes" role="tab" aria-controls="quote" aria-selected="true">Quotes <span class="badge badge-light">#quotes.RecordCount#</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="w_order-tab" data-toggle="tab" href="##w_orders" role="tab" aria-controls="w_order" aria-selected="true">Works Orders <span class="badge badge-light">#w_orders.RecordCount#</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="invoice-tab" data-toggle="tab" href="##invoices" role="tab" aria-controls="invoice" aria-selected="true">Invoices <span class="badge badge-light">#invoices.RecordCount#</span></a>
    </li>    
    <li class="nav-item">
      <a class="nav-link" id="invoice-tab" data-toggle="tab" href="##supporting_docs" role="tab" aria-controls="invoice" aria-selected="true">Supporting Docs <span class="badge badge-light">#uploaded_docs.RecordCount#</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="enquiries-tab" data-toggle="tab" href="##enquiries" role="tab" aria-controls="enquiries" aria-selected="true">Enquiries <span class="badge badge-light">#enquiries.RecordCount#</span></a>
    </li>  
  </ul>

</cfoutput>

<div class="tab-content" id="myTabContent">
                   
  
  <div class="tab-pane fade show active" id="contracts" role="tabpanel" aria-labelledby="contracts-tab">

    <cfinclude template="contracts_linked.cfm">
    
  </div><!-- End of Tab -->   

  <div class="tab-pane fade show" id="enquiries" role="tabpanel" aria-labelledby="enquiries-tab">

    <cfinclude template="enquiries_linked.cfm">

  </div><!-- End of Tab -->    

  <div class="tab-pane fade show" id="quotes" role="tabpanel" aria-labelledby="quote-tab">

    <cfinclude template="quotes_linked.cfm">

  </div><!-- End of Tab -->  

  <div class="tab-pane fade show" id="w_orders" role="tabpanel" aria-labelledby="w_order-tab">

    <cfinclude template="w_orders_linked.cfm">

  </div><!-- End of Tab -->  

  <div class="tab-pane fade show" id="invoices" role="tabpanel" aria-labelledby="invoices-tab">

    <cfinclude template="invoices_linked.cfm">

  </div><!-- End of Tab -->  

  <div class="tab-pane fade show" id="supporting_docs" role="tabpanel" aria-labelledby="docs-tab">

    <cfinclude template="docs_linked.cfm">

  </div><!-- End of Tab -->  

                </div>

                


      </main>

    <hr>
    <!-- End of page main content -->

    <cfinclude template="includes/footer.cfm">
    <cfinclude template="includes/java_include.cfm">

</body>
</html>