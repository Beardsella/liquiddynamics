<cfquery name="preventer_type_list" datasource="agbcodes_ldi">
  SELECT DISTINCT preventer_type
  FROM alpha4
  ORDER BY preventer_type
</cfquery>

<cfquery name="volume_list" datasource="agbcodes_ldi">
  SELECT DISTINCT volume
  FROM alpha4
  ORDER BY volume
</cfquery>

<cfquery name="membrane_list" datasource="agbcodes_ldi">
  SELECT DISTINCT membrane
  FROM alpha4
  ORDER BY membrane
</cfquery>

<cfquery name="PRESS_list" datasource="agbcodes_ldi">
  SELECT DISTINCT PRESS
  FROM alpha4
  ORDER BY PRESS
</cfquery>

<cfquery name="part_connection_list" datasource="agbcodes_ldi">
  SELECT DISTINCT connection
  FROM alpha4
  ORDER BY connection
</cfquery>

<cfquery name="soild_material_list" datasource="agbcodes_ldi">
  SELECT DISTINCT soild_material
  FROM alpha4
  ORDER BY soild_material
</cfquery>

<cfquery name="OTHER_list" datasource="agbcodes_ldi">
  SELECT DISTINCT OTHER
  FROM alpha4
  ORDER BY OTHER
</cfquery>

<div class="row mt-5">

  <div class="col-6">
    <cfoutput>
      <h3>Quote Items <span class="badge badge-primary">#this_quotes_items.recordcount#</span></h3>
    </cfoutput>
  </div> 

  <div class="col-6 float-right">
    
    <!-- New Common Job Parameter Modal TRIGGER -->
    <button class="btn btn-outline-success float-right mr-2" type="button" data-toggle="modal" data-target="#new_item_modal">Add a New Quote Item</button>

    <!-- Expand existing Common Job Parameters TRIGGER -->
    <button class="btn btn-outline-info float-right mr-2" type="button" data-toggle="collapse" data-target="#quote_items" aria-expanded="false" aria-controls="quote_items">Expand Quote Items</button>

  </div>

</div>

<hr>

<div class="collapse" id="quote_items">

  <cfset quote_items = '0'>

  <cfoutput query="this_quotes_items">

    <input type="hidden" name="item_#this_quotes_items.CurrentRow#_id" id="item_#this_quotes_items.CurrentRow#_id" value="#id#">

    <cfset quote_items = '#quote_items#' + '1'>

    <cfparam name="this_quotes_items.item_name" default="">
    <cfparam name="this_quotes_items.preventer_type" default="">
    <cfparam name="this_quotes_items.volume" default="">
    <cfparam name="this_quotes_items.membrane" default="">
    <cfparam name="this_quotes_items.pressure" default="">
    <cfparam name="this_quotes_items.connection" default="">
    <cfparam name="this_quotes_items.wetted_material" default="">
    <cfparam name="this_quotes_items.miscellaneous" default="">
    <cfparam name="this_quotes_items.item_qty" default="1">
    <cfparam name="this_quotes_items.item_price" default="">
    <cfparam name="this_quotes_items.currency" default="">
    <cfparam name="this_quotes_items.conversion" default="">
    <cfparam name="this_quotes_items.working_pressure" default="">
    <cfparam name="this_quotes_items.cushion_prefill_pressure" default="">
    <cfparam name="this_quotes_items.design_pressure" default="">
    <cfparam name="this_quotes_items.dimensions" default="">
    <cfparam name="this_quotes_items.item_notes" default="">
    <cfparam name="this_quotes_items.ped" default="">
    <cfparam name="this_quotes_items.ped_price" default="">
    <cfparam name="this_quotes_items.ped_currency" default="">
    <cfparam name="this_quotes_items.ped_conversion" default="">
    <cfparam name="this_quotes_items.ped_notes" default="">

    <!-- GET Item Specific Params -->
    <cfquery name="quote_item_#this_quotes_items.CurrentRow#_params" datasource="agbcodes_ldi">
      SELECT *
      FROM quote_item_params
      WHERE item_id = "#this_quotes_items.id#"
    </cfquery>

    <input type="hidden" name="item_number" id="item_number" value="#CurrentRow#">


    <div class="row">

      <div class="col-3">
        <h4><span class="badge badge-warning">Item ###this_quotes_items.CurrentRow#</span></h4>
      </div> 

      <div class="col-9">
        
        <!-- New Item Parameter Modal TRIGGER -->
        <button class="btn btn-outline-success float-right mr-2" type="button" data-toggle="modal" data-target="##new_item_#CurrentRow#_param_modal">Add parameter to Item ###CurrentRow#</button>        

        <cfset check_param_var = "quote_item_#this_quotes_items.CurrentRow#_params.recordcount">
        <cfset check_param_var = "#Evaluate(check_param_var)#">

        <!-- If there isnt any Specific paramerters yet do not show this -->
        <cfif check_param_var gte 1>
          <!-- Expand existing Item Parameters TRIGGER -->
          <button class="btn btn-outline-info float-right mr-2" type="button" data-toggle="collapse" data-target="##item_#currentrow#_param_details" aria-expanded="false" aria-controls="item_#currentrow#_param_details">Expand Item #CurrentRow# Parameters</button>
        </cfif>

        <!-- Expand existing Item Details TRIGGER -->
        <button class="btn btn-outline-info float-right mr-2" type="button" data-toggle="collapse" data-target="##item_#currentrow#_details" aria-expanded="false" aria-controls="item_#currentrow#_details">Expand Item #CurrentRow# Details</button>

        <!-- Delete existing Common Job Parameters TRIGGER -->
        <a class="btn btn-outline-danger float-right mr-2" href="processing_files/quote_delete_item.cfm?id=#id#">Delete Item ###CurrentRow#</a>

      </div>

    </div>

    <hr>

    <div class="collapse" id="item_#currentrow#_param_details">

      <cfloop query="quote_item_#this_quotes_items.CurrentRow#_params">

      <cfset quote_item_n_query_prefix = "quote_item_#this_quotes_items.CurrentRow#_params">

      <div class="row">
        <div class="col-9">
          <h4><span class="badge badge-info">Item ###this_quotes_items.CurrentRow# Parameter ###CurrentRow# </span></h4>
        </div>
        <div class="col-3 text-right">
          <h6><a href="processing_files/quote_delete_item_param.cfm?id=#id#" class="text-danger">Delete Parameter ###CurrentRow#</a></h6>
        </div>
      </div>

      <cfparam name="param" default="">
      <cfparam name="param_value" default="">
      <cfparam name="param_notes" default="">


      <div class="row">

        <!-- Text input-->
        <div class="col-md-6 form-group">
          <label class="control-label" for="item_#this_quotes_items.CurrentRow#_parameter_#CurrentRow#_name">Parameter Name</label>  
          <div class="">
            <input id="item_#this_quotes_items.CurrentRow#_parameter_#CurrentRow#_name" name="item_#this_quotes_items.CurrentRow#_parameter_#CurrentRow#_name" type="text" placeholder="Working Pressure" class="form-control input-md" value="#URLDecode(param)#">
          </div>
        </div>

        <!-- Text input-->
        <div class="col-md-6 form-group">
          <label class="control-label" for="item_#this_quotes_items.CurrentRow#_parameter_#CurrentRow#_value">Parameter Value</label>  
          <div class="">
          <input id="item_#this_quotes_items.CurrentRow#_parameter_#CurrentRow#_value" name="item_#this_quotes_items.CurrentRow#_parameter_#CurrentRow#_value" type="text" placeholder="100 bar" class="form-control input-md" value="#URLDecode(param_value)#">
            
          </div>
        </div>

      </div>

      <div class="row">
        <!-- Textarea -->
        <div class="form-group col-md-12">
          <label class="control-label" for="item_#this_quotes_items.CurrentRow#_parameter_#CurrentRow#_notes">Parameter Notes</label>
          <div class="">                     
            <textarea class="form-control" id="item_#this_quotes_items.CurrentRow#_parameter_#CurrentRow#_notes" name="item_#this_quotes_items.CurrentRow#_parameter_#CurrentRow#_notes">#URLDecode(param_notes)#</textarea>
          </div>
        </div>
      </div>

      <hr>

    </cfloop>

    </div>

    <div class="collapse" id="item_#CurrentRow#_details">


    <h4><span class="badge badge-dark">Item ###this_quotes_items.CurrentRow# Details</span></h4>


    <div class="row">
    	<!-- Text input-->
    	<div class="col-md-12 form-group">
    		<label class="control-label" for="item_name_#CurrentRow#">Item Name</label>  
    		<div class="">
    			<input id="item_name_#CurrentRow#" name="item_name_#CurrentRow#" type="text" placeholder="Jumboflex Thermal Expansion Compensator" class="form-control input-md" value="#URLDecode(this_quotes_items.item_name)#">
    		</div>
    	</div>
    </div>

    <div class="row">
      <!-- Text input-->
      <div class="col-md-12 form-group">
        <label class="control-label" for="item_qty_#CurrentRow#">Item Quantity</label>  
        <div class="">
        <input id="item_qty_#CurrentRow#" name="item_qty_#CurrentRow#" type="number" class="form-control input-md" required value="#this_quotes_items.item_qty#">
        </div>
      </div>
    </div>

      <div class="row">

        <div class="col-md-3">
          <label for="preventer_type_#CurrentRow#">Preventer Type</label>
          <div class="input-group mb-3">
            <input list="preventer_types" type="text" class="form-control input-group-text" id="preventer_type_#CurrentRow#" name="preventer_type_#CurrentRow#" placeholder="Search for preventer type" value="#URLDecode(this_quotes_items.preventer_type)#">
          </div>
        </div>

        <datalist id="preventer_types">
          <cfloop query="preventer_type_list">
            <option value="#URLDecode(preventer_type)#">
          </cfloop>
        </datalist>

        <div class="col-md-3">
          <label for="volume_#CurrentRow#">Volume</label>
          <div class="input-group mb-3">
            <input list="volume_list" type="text" class="form-control" id="volume_#CurrentRow#" name="volume_#CurrentRow#" placeholder="Search for volume" value="#URLDecode(this_quotes_items.volume)#">
          </div>
        </div>

        <datalist id="volume_list">
          <cfloop query="volume_list">
            <option value="#URLDecode(volume)#">
          </cfloop>
        </datalist>


        <div class="col-md-3">
          <label for="membrane_#CurrentRow#">Membrane</label>
          <div class="input-group mb-3">
            <input list="membrane_list" type="text" class="form-control" id="membrane_#CurrentRow#" name="membrane_#CurrentRow#" placeholder="Search for membrane" value="#URLDecode(this_quotes_items.membrane)#">
          </div>
        </div>

        <datalist id="membrane_list">
          <cfloop query="membrane_list">
            <option value="#URLDecode(membrane)#">
          </cfloop>
        </datalist>

        <div class="col-md-3">
          <label for="PRESS_#CurrentRow#">Pressure</label>
          <div class="input-group mb-3">
            <input list="PRESS_list" type="text" class="form-control" id="PRESS_#CurrentRow#" name="PRESS_#CurrentRow#" placeholder="Search for pressure" value="#URLDecode(this_quotes_items.pressure)#">
          </div>
        </div>

        <datalist id="PRESS_list">
          <cfloop query="PRESS_list">
            <option value="#URLDecode(PRESS)#">
          </cfloop>
        </datalist>

      </div>

      <div class="row">

        <div class="col-md-4">
          <label for="item_connection_#CurrentRow#">Connection</label>
          <div class="input-group mb-3">
            <input list="part_connection_list" type="text" class="form-control" id="item_connection_#CurrentRow#" name="item_connection_#CurrentRow#" placeholder="Search for Connection" value="#URLDecode(this_quotes_items.connection)#">
          </div>
        </div>

        <datalist id="part_connection_list">
          <cfloop query="part_connection_list">
            <option value="#URLDecode(connection)#">
          </cfloop>
        </datalist>


        <div class="col-md-4">
          <label for="soild_material_#CurrentRow#">Wetted Material</label>
          <div class="input-group mb-3">
            <input list="soild_material_list" type="text" class="form-control" id="soild_material_#CurrentRow#" name="soild_material_#CurrentRow#" placeholder="Search for Soild Material" value="#URLDecode(this_quotes_items.wetted_material)#">
          </div>
        </div>


        <datalist id="soild_material_list">
          <cfloop query="soild_material_list">
            <option value="#URLDecode(soild_material)#">
          </cfloop>
        </datalist>


        <div class="col-md-4">
          <label for="item_misc_#CurrentRow#">Miscellaneous</label>
          <div class="input-group mb-3">
            <input list="OTHER_list" type="text" class="form-control" id="item_misc_#CurrentRow#" name="item_misc_#CurrentRow#" placeholder="Search for other" value="#URLDecode(this_quotes_items.miscellaneous)#">
          </div>
        </div>

        <datalist id="OTHER_list">
          <cfloop query="OTHER_list">
            <option value="#URLDecode(OTHER)#">
          </cfloop>
        </datalist>


      </div>

    <div class="row">

      <!-- Text input-->
      <div class="col-md-4 form-group">
        <label class="control-label" for="non_wetted_material_#CurrentRow#">Non Wetted Material</label>  
        <div class="">
        <input id="non_wetted_material_#CurrentRow#" name="non_wetted_material_#CurrentRow#" type="text" placeholder="" class="form-control input-md" value="#this_quotes_items.non_wetted_material#">
          
        </div>
      </div>

      <!-- Text input-->
      <div class="col-md-4 form-group">
        <label class="control-label" for="flexing_membrane_#CurrentRow#">Flexing Membrane</label>  
        <div class="">
        <input id="flexing_membrane_#CurrentRow#" name="flexing_membrane_#CurrentRow#" type="text" placeholder="" class="form-control input-md">
          
        </div>
      </div>

      <!-- Text input-->
      <div class="col-md-4 form-group">
        <label class="control-label" for="other_material_#CurrentRow#">Other Material</label>  
        <div class="">
        <input id="other_material_#CurrentRow#" name="other_material_#CurrentRow#" type="text" placeholder="" class="form-control input-md" value="#this_quotes_items.other_material#">
          
        </div>
      </div>

    </div>
    <div class="row">

    <!-- Text input-->
    <div class="col-12 form-group">
      <label class="control-label" for="item_price_#CurrentRow#">Price Each (default GBP)</label>  
      <div class="">
      <input id="item_price_#CurrentRow#" name="item_price_#CurrentRow#" type="text" placeholder="100" class="form-control input-md" value="#this_quotes_items.item_price#" required>
        
      </div>
    </div>
<hr>

          <button class="btn btn-link btn-block" type="button" data-toggle="collapse" data-target="##cost_details" aria-expanded="false" aria-controls="cost_details">More Cost Details...</button>
        
  </div>

<div id="cost_details">

  <div class="row">



    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="item_currency_#CurrentRow#">Currency (ignore if GBP)</label>  
      <div class="">
      <input id="item_currency_#CurrentRow#" name="item_currency_#CurrentRow#" type="text" placeholder="GBP" class="form-control input-md" value="#URLDecode(this_quotes_items.currency)#">
        
      </div>
    </div>

    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="item_conversion_#CurrentRow#">Conversion rate to GBP</label>  
      <div class="">
      <input id="item_conversion_#CurrentRow#" name="item_conversion_#CurrentRow#" type="text" placeholder="0.678" class="form-control input-md" value="#this_quotes_items.conversion#">
        
      </div>
    </div>


    </div>

<div class="row">
    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="discount_percent_#CurrentRow#">Discount %</label>  
      <div class="">
      <input id="discount_percent_#CurrentRow#" name="discount_percent_#CurrentRow#" type="text" placeholder="100" class="form-control input-md" value="#this_quotes_items.discount_percent#">
        
      </div>
    </div>

        <!-- Textarea -->
        <div class="form-group col-6">
          <label class="control-label" for="discount_notes_#CurrentRow#">Discount Notes</label>
          <div class="">                     
            <textarea class="form-control" id="discount_notes_#CurrentRow#" name="discount_notes_#CurrentRow#">#URLDecode(discount_notes)#</textarea>
          </div>
        </div>
</div>

</div>
<hr>

    <div class="row">

    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="item_working_pressure_#CurrentRow#">Working Pressure</label>  
      <div class="">
      <input id="item_working_pressure_#CurrentRow#" name="item_working_pressure_#CurrentRow#" type="text" placeholder="10 Bar G Max" class="form-control input-md" value="#URLDecode(this_quotes_items.working_pressure)#">
        
      </div>
    </div>

    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="item_cushion_prefill_pressure_#CurrentRow#">Cushion Prefill Pressure</label>  
      <div class="">
      <input id="item_cushion_prefill_pressure_#CurrentRow#" name="item_cushion_prefill_pressure_#CurrentRow#" type="text" placeholder="70% of working pressure" class="form-control input-md" value="#URLDecode(this_quotes_items.cushion_prefill_pressure)#">
        
      </div>
    </div>


    </div>
    <div class="row">

    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="item_design_pressure_#CurrentRow#">Design Pressure</label>  
      <div class="">
      <input id="item_design_pressure_#CurrentRow#" name="item_design_pressure_#CurrentRow#" type="text" placeholder="19.0 Bar G" class="form-control input-md" value="#URLDecode(this_quotes_items.design_pressure)#">
        
      </div>
    </div>

    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="item_dimensions_#CurrentRow#">Nominal Dimensions</label>  
      <div class="">
      <input id="item_dimensions_#CurrentRow#" name="item_dimensions_#CurrentRow#" type="text" placeholder="560mm dia X 4270mm length " class="form-control input-md" value="#URLDecode(this_quotes_items.dimensions)#">
        
      </div>
    </div>


    </div>
    <div class="row">

    <!-- Textarea -->
    <div class="form-group col-md-12">
      <label class="control-label" for="item_notes_#CurrentRow#">Item Notes</label>
      <div class="">                     
        <textarea class="form-control" id="item_notes_#CurrentRow#" name="item_notes_#CurrentRow#" placeholder="Unit designed.....">#URLDecode(this_quotes_items.item_notes)#</textarea>
      </div>
    </div>

    </div>

    <hr>

    <div class="row">

    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="ped_#CurrentRow#">PED</label>  
      <div class="">
      <input id="ped_#CurrentRow#" name="ped_#CurrentRow#" type="text" placeholder="Pressure Equipment Directive" class="form-control input-md" value="#URLDecode(this_quotes_items.ped)#">
        
      </div>
    </div>


    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="ped_price_#CurrentRow#">Extra cost of PED</label>  
      <div class="">
      <input id="ped_price_#CurrentRow#" name="ped_price_#CurrentRow#" type="text" placeholder="" class="form-control input-md" value="#this_quotes_items.ped_price#">
        
      </div>
    </div>


    </div>
    <div class="row">

    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="ped_currency_#CurrentRow#">PED Currency</label>  
      <div class="">
      <input id="ped_currency_#CurrentRow#" name="ped_currency_#CurrentRow#" type="text" placeholder="GBP" class="form-control input-md" value="#URLDecode(this_quotes_items.ped_currency)#">
        
      </div>
    </div>


    <!-- Text input-->
    <div class="col-md-6 form-group">
      <label class="control-label" for="ped_conversion_#CurrentRow#">PED Conversion rate to GBP</label>  
      <div class="">
      <input id="ped_conversion_#CurrentRow#" name="ped_conversion_#CurrentRow#" type="text" placeholder="0.678" class="form-control input-md" value="#DecimalFormat(this_quotes_items.ped_conversion)#">
        
      </div>
    </div>


    </div>

    <div class="row">

    <!-- Textarea -->
    <div class="form-group col-md-12">
      <label class="control-label" for="PED_notes_#CurrentRow#">PED Notes</label>
      <div class="">                     
        <textarea class="form-control" id="PED_notes_#CurrentRow#" name="PED_notes_#CurrentRow#" placeholder="Pressure Equipment Directive...">#URLDecode(this_quotes_items.ped_notes)#</textarea>
      </div>
    </div>

    </div>

    <hr>

    </div>

  </cfoutput>


</div>