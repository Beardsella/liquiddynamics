<cfoutput query="this_quote">
<table style="width:100%;">
      
      <tr>
        <td colspan="5" style="text-align:center;"><h1>#URLDecode(seller)# - #doc_purpose#</h1></td>
      </tr>

      <tr>
        <td colspan="5" style="text-align:center;">"#quote_id_version#"</td>
      </tr>

      <tr>
        <td>&nbsp;</td>
      </tr>

      <tr>
        <th style="width:20%;">Customer</th>
        <td style="width:20%;">#URLDecode(this_quote.company_name)#</td>
        <td style="width:20%;"></td>
        <th style="width:20%;">Date</th>
        <td style="width:20%;">#DateFormat(quote_dated, "dd-mm-YYYY")#</td>
      </tr>

      <tr>
        <th>Attention Of</th>
        <td>#URLDecode(this_quote.first_name)# #URLDecode(this_quote.last_name)#</td>
        <td></td>
        <th>Customer Ref</th>
        <td>#URLDecode(this_quote.cust_enquiry_ref)#</td>
      </tr>

      <tr>
        <th>From</th>
        <td>#URLDecode(this_quote.seller)#</td>
        <td></td>
        <th>Quote No</th>
        <td>#formated_id#</td>
      </tr>

      <tr>
        <td colspan="5"><p>#URLDecode(this_quote.quote_intro)#</p></td>
      </tr>

    </table>

    <hr>

  </cfoutput>