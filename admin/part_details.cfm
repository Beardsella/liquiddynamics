<!DOCTYPE html>
<html lang="en">
<head>

  <cfinclude template="includes/default_header.cfm">

  <title>Liquid Dynamics</title>

  <cfquery name="this_part" datasource="agbcodes_ldi" maxrows="500">
      SELECT id, preventer_type, volume, membrane, press, connection, soild_material, other
      FROM alpha4
      WHERE ID = #URL.id#
  </cfquery>
  <cfquery name="duplicate_parts" datasource="agbcodes_ldi" maxrows="500">
      SELECT id, pr_ea, qty, order_date, inv_no, customer
      FROM alpha4
      WHERE preventer_type = "#URLEncodedFormat(this_part.preventer_type)#"
      AND volume = "#URLEncodedFormat(this_part.volume)#"
      AND membrane = "#URLEncodedFormat(this_part.membrane)#"
      AND PRESS = "#URLEncodedFormat(this_part.PRESS)#"
      AND connection = "#URLEncodedFormat(this_part.connection)#"
      AND soild_material = "#URLEncodedFormat(this_part.soild_material)#"
      AND OTHER = "#URLEncodedFormat(this_part.OTHER)#"
      ORDER BY id DESC
  </cfquery>
  <cfquery name="duplicate_parts_chart_data" datasource="agbcodes_ldi" maxrows="500">
      SELECT id, pr_ea, qty, order_date
      FROM alpha4
      WHERE preventer_type = "#URLEncodedFormat(this_part.preventer_type)#"
      AND volume = "#URLEncodedFormat(this_part.volume)#"
      AND membrane = "#URLEncodedFormat(this_part.membrane)#"
      AND PRESS = "#URLEncodedFormat(this_part.PRESS)#"
      AND connection = "#URLEncodedFormat(this_part.connection)#"
      AND soild_material = "#URLEncodedFormat(this_part.soild_material)#"
      AND OTHER = "#URLEncodedFormat(this_part.OTHER)#"
      ORDER BY id ASC
  </cfquery>

  <cfif duplicate_parts.ORDER_DATE IS NOT "">

    <cfset date_diffrence = '#dateDiff("y", duplicate_parts.ORDER_DATE, now())#'>
    <cfset percentage_to_add = '.006844627' * '#date_diffrence#'>
    <cfset price_increase = '#percentage_to_add#' * ('#duplicate_parts.pr_ea#' / '100')>
    <cfset new_guide_price = '#price_increase#' + '#duplicate_parts.pr_ea#'>

    <cfquery name="max_price" datasource="agbcodes_ldi" maxrows="500">
      SELECT max(pr_ea) AS maxprice
      FROM alpha4
      WHERE preventer_type = "#URLEncodedFormat(this_part.preventer_type)#"
      AND volume = "#URLEncodedFormat(this_part.volume)#"
      AND membrane = "#URLEncodedFormat(this_part.membrane)#"
      AND PRESS = "#URLEncodedFormat(this_part.PRESS)#"
      AND connection = "#URLEncodedFormat(this_part.connection)#"
      AND soild_material = "#URLEncodedFormat(this_part.soild_material)#"
      AND OTHER = "#URLEncodedFormat(this_part.OTHER)#"
    </cfquery>

    <cfquery name="min_price" datasource="agbcodes_ldi" maxrows="500">
      SELECT min(pr_ea) AS minprice
      FROM alpha4
      WHERE preventer_type = "#URLEncodedFormat(this_part.preventer_type)#"
      AND volume = "#URLEncodedFormat(this_part.volume)#"
      AND membrane = "#URLEncodedFormat(this_part.membrane)#"
      AND PRESS = "#URLEncodedFormat(this_part.PRESS)#"
      AND connection = "#URLEncodedFormat(this_part.connection)#"
      AND soild_material = "#URLEncodedFormat(this_part.soild_material)#"
      AND OTHER = "#URLEncodedFormat(this_part.OTHER)#"
      ORDER BY id ASC
    </cfquery>

  </cfif>


</head>
<body>

  <cfinclude template="includes/nav.cfm">

  <!-- Page Content -->
  <div class="container">

<h1 class="page-header">Part Details</h1>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.cfm">Home</a></li>
        <li class="breadcrumb-item"><a href="parts.cfm">Parts DB</a></li>
        <li class="breadcrumb-item active" aria-current="page">Part Details</li>
      </ol>
    </nav>

    <div class="row">
                
        <div class="col-lg-12">

          <cfoutput>
              <h3 class="page-header text-center">

                <cfif this_part.preventer_type IS NOT "">
                    <span class="badge badge-primary">#URLDecode(this_part.preventer_type)#<br><small>Preventer Type</small></span>-
                  <cfelse>
                    <span class="badge badge-danger"><small>No</small><br>"Preventer Type"</span>-
                </cfif>

                <cfif this_part.volume IS NOT "">
                    <span class="badge badge-primary">#URLDecode(this_part.volume)#<br><small>Volume</small></span>-
                  <cfelse>
                    <span class="badge badge-danger"><small>No</small><br>"Volume"</span>-
                </cfif>

                <cfif this_part.membrane IS NOT "">
                    <span class="badge badge-primary">#URLDecode(this_part.membrane)#<br><small>Membrane</small></span>-
                  <cfelse>
                    <span class="badge badge-danger"><small>No</small><br>"Membrane"</span>-
                </cfif>

                <cfif this_part.PRESS IS NOT "">
                    <span class="badge badge-primary">#URLDecode(this_part.PRESS)#<br><small>Pressure</small></span>-
                  <cfelse>
                    <span class="badge badge-danger"><small>No</small><br>"Pressure"</span>-
                </cfif>

                <cfif this_part.connection IS NOT "">
                    <span class="badge badge-primary">#URLDecode(this_part.connection)#<br><small>Connection</small></span>-
                  <cfelse>
                    <span class="badge badge-danger"><small>No</small><br>"Connection"</span>-
                </cfif>

                <cfif this_part.soild_material IS NOT "">
                    <span class="badge badge-primary">#URLDecode(this_part.soild_material)#<br><small>Solid Material</small></span>-
                  <cfelse>
                    <span class="badge badge-danger"><small>No</small><br>"Solid Material"</span>-
                </cfif>

                <cfif this_part.OTHER IS NOT "">
                    <span class="badge badge-primary">#URLDecode(this_part.OTHER)#<br><small>Other</small></span>
                  <cfelse>
                    <span class="badge badge-danger"><small>No</small><br>"Other"</span>
                </cfif>

              </h3>

            </cfoutput>

        </div>

      </div>

      <cfif duplicate_parts.ORDER_DATE IS NOT "">

        <cfoutput>

          <table class="table table-striped">

            <thead>
              <tr>
                <th>Last Sale</th>
                <th>Inflation since last sale</th>
                <th>Price Increase</th>
                <th>New Guide Price</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                  <td>#date_diffrence# Days Ago</td>
                  <td>#DecimalFormat(percentage_to_add)# %</td>
                  <td><span class="badge badge-primary">&pound; #DecimalFormat(price_increase)#</span></td>
                  <td><span class="badge badge-success">&pound; #DecimalFormat(new_guide_price)#</span></td>
              </tr>
            </tbody>

          </table>

        </cfoutput>

      </cfif>


    <cftry>

      <cfchart format="html" chartHeight="500" chartWidth="1100" showLegend="no" title="Price of part over time" scales="x,y" xaxistitle="Order Date" yaxistitle="Price £" scalefrom="#min_price.minprice#" scaleto="#max_price.maxprice#">
          <cfchartseries type="scatter" markerstyle="circle" color="red" serieslabel="price &pound;">
            <cfoutput query="duplicate_parts_chart_data">
              <cfchartdata item="#ORDER_DATE#" value="#pr_ea#" zValue="ID = #id#"/>
            </cfoutput>
          </cfchartseries>
      </cfchart>

      <cfcatch>
        <h2>Chart failed to load</h2>
        <cfoutput>
        <p class="lead">
        </cfoutput>
      </cfcatch>

    </cftry>

      <table class="table table-striped" id="ss_table">

        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Invoice Number</th>
            <th scope="col">Customer</th>
            <th scope="col">Price Each</th>
            <th scope="col">Quantity Sold</th>
            <th scope="col">Date Orderd</th>

          </tr>
        </thead>
        <tbody>

            <cfoutput query="duplicate_parts">
              <tr>
                <td>#id#</td>
                <td><a href="">#inv_no#</a></td>
                <td><a href="">#customer#</a></td>
                <td><cfif isDefined("pr_ea")>&pound;#pr_ea#</cfif></td>
                <td><cfif isDefined("qty")>#qty#</cfif></td>
                <td><cfif isDefined("ORDER_DATE")>#ORDER_DATE#</cfif></td>
              </tr>
            </cfoutput>

        </tbody>

      </table>

    </div>
  
  <!-- End of page main content -->

  <cfinclude template="includes/footer.cfm">
  <cfinclude template="includes/java_include.cfm">

  <script type="text/JavaScript">
    $(document).ready(function(){ 
      $("#ss_table").tablesorter(); 
    }); 
  </script>

</body>
</html>