<cfoutput query="this_quote">
<div>

<h1>Quote - #URLDecode(this_quote.seller)#</h1>
<cfif this_quote_version NEQ '1'>
  <h3>#formated_id# Version ###this_quote_version#</h3>
  <cfelse>
  <h3>#formated_id#</h3>
</cfif>

<div class="clearfix"></div>

  <div style="display:inline; float:right; width: 45%;">
    #URLDecode(this_quote.seller)#<br>
    716 Greg Street, Stockport<br>
    Manchester, UK<br>
    SK2 LDI
  </div>

  <div class="col-left">

    <strong>ID : </strong> "UK-#URL.ID#" <br>
    <strong>Email : </strong> info@ldi.co.uk
    <br />
    <strong>Call : </strong> +44 161 430 9625
    <br>
    <strong>Date : </strong>#quote_date_formated#
  </div>

</div>

<div class="clearfix"></div>
<hr />

<div class="row">

  <div style="display:inline; float:right; width: 45%;">
    <h3>Client Contact</h3>
    <strong>#URLDecode(this_quote.first_name)#, #URLDecode(this_quote.last_name)#</strong><br>
    Tel: #URLDecode(this_quote.telephone)#<br>
    Email: #URLDecode(this_quote.email)#
  </div>

  <div>
    <h3>Client Details</h3>
    #URLDecode(this_quote.company_name)#<br> 789/90 , Lane Here, New York,
    <br /> United States
  </div>

</div>

<h3>Common Parameters</h3>

<cfloop query="this_quote_params">
  <div class="row" style="width:50%;">

    <div>
      <p><strong>#URLDecode(param_value)#</strong> #URLDecode(param)#</p>
    </div>

  </div>  
  <div class="row">

    <div>
      <p>#URLDecode(param_notes)#</p>
    </div>

  </div>

  <hr>

</cfloop>

<h3>#this_quotes_items.RecordCount# Quoted Items</h3>

<cfloop query="this_quotes_items">
  <div class="row">

    <div>
      <p><strong>#URLDecode(item_name)#</strong>  Qty: #item_qty#</p>
      Price Each &pound; #item_price#
    </div>

  </div>  
  <div class="row">

    <div>
      <p>#URLDecode(item_notes)#</p>
    </div>

  </div>

  <cfquery name="this_items_params" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_item_params
    WHERE item_id = "#id#"
  </cfquery>

  <cfloop query="this_items_params">
    
  <div class="row">

    <div>
      <p><strong>#URLDecode(param_value)#</strong> #URLDecode(param)#</p>
    </div>

  </div>  
  <div class="row">

    <div>
      <p>#URLDecode(param_notes)#</p>
    </div>

  </div>

  <hr>
  </cfloop>


  <hr>

</cfloop>


<cfloop query="this_quote_extra_items">
  <div class="row" style="width:50%;">

    <div>
      <p><strong>#URLDecode(item_refrence)#</strong> #URLDecode(currency)# #URLDecode(item_price)#</p>
    </div>

  </div>  
  <div class="row">

    <div>
      <p>#URLDecode(item_notes)#</p>
    </div>

  </div>

  <hr>

</cfloop>






    <h3 class="mt-5">Final Quote Details</h3>
    <hr> 

    


      
        <label class="control-label" for="freebies">Price Includes</label>
        <div class="">                     
          <textarea class="form-control" id="freebies" name="freebies">#URLDecode(freebies)#</textarea>
        </div>
      </div>


      
        <label class="control-label" for="not_included">Not Included</label>
        <div class="">                     
          <textarea class="form-control" id="not_included" name="not_included">#URLDecode(not_included)#</textarea>
        </div>
      </div>

    </div>

    

      
        <label class="control-label" for="sale_condtions">Conditions of sale</label>
        <div class="">                     
          <textarea class="form-control" id="sale_condtions" name="sale_condtions">#URLDecode(sale_condtions)#</textarea>
        </div>
      </div>


      
        <label class="control-label" for="terms_of_payment">Terms of Payment</label>
        <div class="">                     
          <textarea class="form-control" id="terms_of_payment" name="terms_of_payment">#URLDecode(terms_of_payment)#</textarea>
        </div>
      </div>
    </div>

    <cfset exp_date_formated = "#DateFormat(quote_vaild_for, 'dd/mm/yyyy')#">

    

      
        <label class="control-label" for="quote_expiration_date">Quote valid until</label>  
        <input type="text" id="quote_expiration_date" name="quote_expiration_date" class="form-control" placeholder="An estimate for the customer" value="#exp_date_formated#">
      </div>

      
        <label class="control-label" for="dispatch_estimate">Dispatch Estimate</label>  
        <input type="text" id="dispatch_estimate" name="dispatch_estimate" class="form-control" placeholder="An estimate for the customer" value="#dispatch_date_formated#">
      </div>

    </div>



  </cfoutput>