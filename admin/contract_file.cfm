<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <title>Manage Contract File - Liquid Dynamics Group Ltd</title>

    <cfquery name="this_job" datasource="agbcodes_ldi">
        SELECT *
        FROM jobs
        WHERE id = "#URL.id#"
    </cfquery> 

    <cfquery name="this_company" datasource="agbcodes_ldi">
        SELECT *
        FROM companies
        WHERE id = "#this_job.company_id#"
    </cfquery>      

    <cfquery name="countries" datasource="agbcodes_ldi">
        SELECT *
        FROM countries
    </cfquery>

    <cfquery name="staff" datasource="agbcodes_ldi">
        SELECT *
        FROM contacts
        WHERE id = "#this_job.contact_id#"
    </cfquery>

    <cfquery name="enquiries" datasource="agbcodes_ldi">
        SELECT enquirys_pump_systems.*
        FROM enquirys_pump_systems
        RIGHT JOIN job_vs_enq 
        ON enquirys_pump_systems.id = job_vs_enq.enquiry_id
        WHERE job_vs_enq.job_id = "#URL.id#"
        ORDER BY enquirys_pump_systems.enquiry_date
    </cfquery>

    <cfquery name="quotes" datasource="agbcodes_ldi">
        SELECT quotes.*
        FROM quotes
        RIGHT JOIN job_vs_quote 
        ON quotes.id = job_vs_quote.quote_id
        WHERE job_vs_quote.job_id = "#URL.id#"
    </cfquery>

    <cfquery name="w_orders" datasource="agbcodes_ldi">
        SELECT works_orders.*
        FROM works_orders
        RIGHT JOIN job_vs_works_order 
        ON works_orders.id = job_vs_works_order.w_order_id
        WHERE job_vs_works_order.job_id = "#URL.id#"
    </cfquery>

    <cfquery name="invoices" datasource="agbcodes_ldi">
        SELECT invoices.*
        FROM invoices
        RIGHT JOIN job_vs_invoice 
        ON invoices.id = job_vs_invoice.invoice_id
        WHERE job_vs_invoice.job_id = "#URL.id#"
    </cfquery>

    <cfquery name="contract_pdfs" datasource="agbcodes_ldi">
        SELECT *
        FROM pdfs_db
        WHERE db_id = "#this_job.quote_id#" AND db_table = "quotes"
        ORDER BY id DESC
    </cfquery>

    <cfquery name="uploaded_files" datasource="agbcodes_ldi">
        SELECT *
        FROM uploaded_files
        WHERE linked_job = "#this_job.formatted_id#"
    </cfquery>

    <cfquery name="job_notes" datasource="agbcodes_ldi">
        SELECT *
        FROM job_notes
        WHERE job_id = "#URL.id#"
        ORDER BY id DESC
    </cfquery>

</head>
<body>

    <cfinclude template="includes/nav.cfm">

      <!-- Page Content -->
      <main role="main" class="container-fluid">

            <cfoutput>

                <nav aria-label="breadcrumb" role="navigation">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.cfm">Home</a></li>
                    <li class="breadcrumb-item"><a href="contracts_overview.cfm">Contracts Overview</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contract Details</li>
                  </ol>
                </nav>            

                <div class="col-lg-12">
                    <h1 class="page-header">Contract File ###this_job.quote_id#</h1>
                    <p class="lead">Here you can view details and related data to "#URLDecode(this_job.job_title)#". Click to <a href="job_edit.cfm">Edit Contract File</a></p>

  <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item active">
      <a class="nav-link" id="quote-tab" data-toggle="tab" href="##quotes" role="tab" aria-controls="quote" aria-selected="true">Quotes <span class="badge badge-light">#quotes.RecordCount#</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="w_order-tab" data-toggle="tab" href="##w_orders" role="tab" aria-controls="w_order" aria-selected="true">Works Orders <span class="badge badge-light">#w_orders.RecordCount#</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="invoice-tab" data-toggle="tab" href="##invoices" role="tab" aria-controls="invoice" aria-selected="true">Invoices <span class="badge badge-light">#invoices.RecordCount#</span></a>
    </li>    
    <li class="nav-item">
      <a class="nav-link" id="invoice-tab" data-toggle="tab" href="##supporting_docs" role="tab" aria-controls="invoice" aria-selected="true">Supporting Docs <span class="badge badge-light">#contract_pdfs.RecordCount#</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="company-tab" data-toggle="tab" href="##company" role="tab" aria-controls="company" aria-selected="true">Company Details <span class="badge badge-light">#this_company.RecordCount#</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="contacts-tab" data-toggle="tab" href="##contacts" role="tab" aria-controls="contacts" aria-selected="false">Contacts <span class="badge badge-light">#staff.RecordCount#</span></a>
    </li>    
    <li class="nav-item">
      <a class="nav-link" id="enquiries-tab" data-toggle="tab" href="##enquiries" role="tab" aria-controls="enquiries" aria-selected="true">Enquiries <span class="badge badge-light">#enquiries.RecordCount#</span></a>
    </li>  
  </ul>

<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show" id="company" role="tabpanel" aria-labelledby="company-tab">

    <cfinclude template="company_vs_job_linked.cfm">

  </div><!-- End of Tab -->                    
  
  <div class="tab-pane fade show" id="contacts" role="tabpanel" aria-labelledby="contacts-tab">

    <cfinclude template="contact_vs_job_linked.cfm">
    
  </div><!-- End of Tab -->   

  <div class="tab-pane fade show" id="enquiries" role="tabpanel" aria-labelledby="enquiries-tab">

    <cfinclude template="enquiries_vs_job_linked.cfm">

  </div><!-- End of Tab -->    

  <div class="tab-pane fade show active" id="quotes" role="tabpanel" aria-labelledby="quote-tab">

    <cfinclude template="quotes_vs_job_linked.cfm">

  </div><!-- End of Tab -->  

  <div class="tab-pane fade show" id="w_orders" role="tabpanel" aria-labelledby="w_order-tab">

    <cfinclude template="w_orders_vs_job_linked.cfm">

  </div><!-- End of Tab -->  

  <div class="tab-pane fade show" id="invoices" role="tabpanel" aria-labelledby="invoices-tab">

    <cfinclude template="invoices_vs_job_linked.cfm">

  </div><!-- End of Tab -->  

  <div class="tab-pane fade show" id="supporting_docs" role="tabpanel" aria-labelledby="docs-tab">

    <cfinclude template="docs_vs_job_linked.cfm">

  </div><!-- End of Tab -->  

                </div>

                

            </cfoutput>

<hr>

<div class="col-12">

<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#new_job_note_modal">
 Add new note
</button>

  <h5>Contract Notes</h5>
        
  <p class="">This is for admin use only</p>
      <!-- Button trigger modal -->
<hr>
</div>

<cfoutput query="job_notes">
  <div class="row">
    
    <div class="col-2">
      <p class="lead">#created_by#</p>
      <p>#job_notes.comment_created#</p>
    </div>
    <div class="col-10">
      <p>#URLDecode(job_notes.comment)#</p>
    </div>

  </div>
<hr>
</cfoutput>

</div>

<!-- New Note Modal -->
  <div class="modal fade" id="new_job_note_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add a new note</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <form action="processing_files/job_add_note.cfm" method="post">

          <cfoutput>
            <input type="hidden" name="job_id" id="job_id" value="#URL.id#">
          </cfoutput>

          <div class="modal-body">

            <p class="lead">Here you can add admin notes on the contract</p>

              <div class="row">

                <div class="col-md-12 form-group">
                  <label class="control-label" for="new_job_note">Note</label>
                    <textarea class="form-control" id="new_job_note" name="new_job_note"></textarea>
                </div>

              </div>

          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-outline-success">Add a new note to this Contract</button>
          </div>

        </form>
      </div>
    </div>
  </div>
  <!-- End of new note modal -->


      </main>

    <!-- End of page main content -->

    <cfinclude template="includes/footer.cfm">
    <cfinclude template="includes/java_include.cfm">

</body>
</html>