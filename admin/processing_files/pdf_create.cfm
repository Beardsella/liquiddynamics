<!-- Custom styles for this template -->
<link href="css/custom_pdf.css" rel="stylesheet">

<cfquery name="this_quote" datasource="agbcodes_ldi">
  SELECT *
  FROM quotes
  WHERE id = "#URL.id#"
</cfquery>

<cfquery name="this_job" datasource="agbcodes_ldi">
  SELECT *
  FROM jobs
  WHERE quote_id = "#URL.id#"
</cfquery>

<cfif URL.type eq "wo">

  <cfset doc_purpose = "Works Order">
  <cfset doc_purpose_preped = "WO">
  <cfset doc_purpose_abbrevation = "W">
  <cfset job_status = "Working">        

<cfelseif  URL.type eq "q">

  <cfset doc_purpose = "Quote">
  <cfset doc_purpose_preped = "Quote">
  <cfset doc_purpose_abbrevation = "Q">
  <cfset job_status = "Quoted">        

<cfelseif  URL.type eq "i">

  <cfset doc_purpose = "Invoice">
  <cfset doc_purpose_preped = "Invoice">
  <cfset doc_purpose_abbrevation = "I">
  <cfset job_status = "Invoiced">        

</cfif>

<cfquery name="last_quote" datasource="agbcodes_ldi">
  SELECT MAX(doc_version) as last_version
  FROM pdfs_db
  WHERE linked_job_id = "#this_job.id#" AND doc_subject = "#doc_purpose#"
</cfquery>

<cfquery name="this_quote_params" datasource="agbcodes_ldi">
  SELECT *
  FROM quote_params
  WHERE quote_id = "#URL.id#"
</cfquery>

<cfquery name="this_quotes_items" datasource="agbcodes_ldi">
  SELECT *
  FROM quote_items
  WHERE quote_id = "#URL.id#"
</cfquery>

<cfquery name="this_quote_extra_items" datasource="agbcodes_ldi">
  SELECT *
  FROM quote_extra_items
  WHERE quote_id = "#URL.id#"
</cfquery>

<cfquery name="this_works_order" datasource="agbcodes_ldi">
  SELECT *
  FROM job_vs_works_order
  WHERE job_id = "#this_job.id#"
</cfquery>

<cfquery name="this_invoice" datasource="agbcodes_ldi">
  SELECT *
  FROM job_vs_invoice
  WHERE job_id = "#this_job.id#"
</cfquery>


<cfset quote_date_formated = "#DateFormat(this_quote.quote_dated, 'dd/mm/yyyy')#">
<cfset dispatch_date_formated = "#DateFormat(this_quote.dispatch_estimate, 'dd/mm/yyyy')#">
<cfset selling_company = "#URLDecode(this_quote.seller)#">

<cfif selling_company eq "Liquid Dynamics International">
  <cfset file_name_prep = "LDI-UK-#doc_purpose_preped#-#this_quote.id#">
  <cfset formated_id = "LDI-UK-#doc_purpose_abbrevation#-#this_quote.id#">
<cfelseif selling_company eq "Pulse Gard">
  <cfset file_name_prep = "PG-UK-#doc_purpose_preped#-#this_quote.id#">
  <cfset formated_id = "PG-UK-#doc_purpose_abbrevation#-#this_quote.id#">
<cfelseif selling_company eq "Shock Gard">
  <cfset file_name_prep = "SG-UK-#doc_purpose_preped#-#this_quote.id#">
  <cfset formated_id = "SG-UK-#doc_purpose_abbrevation#-#this_quote.id#">
<cfelseif selling_company eq "Hydrotrole">
  <cfset file_name_prep = "HT-UK-#doc_purpose_preped#-#this_quote.id#">
  <cfset formated_id = "HT-UK-#doc_purpose_abbrevation#-#this_quote.id#">
</cfif>

<cfif last_quote.last_version NEQ ''>

  <cfset this_quote_version =  '#last_quote.last_version#'>
  <cfset this_quote_version =  '#last_quote.last_version#' + '1'>
  <cfset quote_id_version =  "#formated_id#-Ver-#this_quote_version#">
  <cfset file_name =  "#file_name_prep#-Ver-#this_quote_version#.pdf">

<cfelse>

  <cfset this_quote_version =  "1">
  <cfset quote_id_version =  "#formated_id#">
  <cfset file_name =  "#file_name_prep#.pdf">

</cfif>

<cfif URL.type EQ 'q'>
  <cfset file_path = "quotes/#this_quote.formatted_id#/">
  <cfset relative_path = ExpandPath("../quotes/#this_quote.formatted_id#/")>
<cfelse>
  <cfset file_path = "contracts/#this_job.formatted_id#/">
  <cfset relative_path = ExpandPath("../contracts/#this_job.formatted_id#/")>
</cfif>

<cfif DirectoryExists(relative_path)>
  <h1>Yay it already exists</h1>
<cfelse>
  <cfset DirectoryCreate(relative_path)>
</cfif>

<cfdocument
  format = "PDF"
  backgroundVisible = "no"
  bookmark = "no"
  overwrite = "yes"
  encryption = "none"
  filename = "#relative_path##file_name#"
  fontEmbed = "true"
  localUrl = "true"
  pagetype="a4">
   
  <cfdocumentitem type="footer" evalatprint="true"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
  <tr><td align="center"><cfoutput>#cfdocument.currentpagenumber# of 
  #cfdocument.totalpagecount#</cfoutput></td></tr> 
  </table> 
  </cfdocumentitem> 

  <cfdocumentsection> 

    <cfinclude template="../quote_pdf_template_part_1.cfm">

    <cfinclude template="../quote_pdf_template_part_2.cfm">
  
    <cfinclude template="../quote_pdf_template_part_3.cfm">
  
  </cfdocumentsection>

</cfdocument>

<cfquery datasource="agbcodes_ldi">
  INSERT INTO pdfs_db 
            (linked_job_id, 
            file_name, 
            doc_subject, 
            doc_version, 
            db_id, 
            db_table, 
            location,
            file_type,
            formatted_id)
  VALUES ("#this_job.id#", 
      "#file_name#", 
      "#doc_purpose#",
      "#this_quote_version#",
      '#this_quote.id#',
      "quotes",
      "#file_path#",
      "pdf",
      "#formated_id#")
</cfquery>

<cfquery datasource="agbcodes_ldi">
    UPDATE quotes
    SET q_status = "#job_status#"
    WHERE id = "#URL.id#"
</cfquery>

<cfquery datasource="agbcodes_ldi">
    UPDATE jobs
    SET job_status = "#job_status#"
    WHERE quote_id = "#URL.id#"
</cfquery>

<cfif  URL.type eq "i">

  <cfif this_invoice.RecordCount GTE '1'>

    <cfquery datasource="agbcodes_ldi">
      INSERT INTO invoices (company_id, company_name, contact_id, first_name, last_name, telephone, email, seller, linked_quote_id)
      VALUES ('#this_quote.company_id#', "#this_quote.company_name#", '#this_quote.contact_id#', "#this_quote.first_name#", "#this_quote.last_name#", "#this_quote.telephone#", "#this_quote.email#", "#this_quote.seller#", "#URL.id#")
    </cfquery>

    <cfquery name="new_invoice" datasource="agbcodes_ldi">
      SELECT MAX(id) AS lastid
      FROM invoices
    </cfquery>

    <cfquery datasource="agbcodes_ldi">
      INSERT INTO job_vs_invoice (job_id, invoice_id)
      VALUES ("#this_job.id#", "#new_invoice.lastid#")
    </cfquery>

    <cfquery datasource="agbcodes_ldi">
        UPDATE quotes
        SET q_status = "#job_status#",
        linked_invoice_id = "#new_invoice.lastid#"
        WHERE id = #URL.id#
    </cfquery>

  </cfif>     

  <cflocation url="../quote_management.cfm?id=#URL.id#" addtoken="no">

<cfelseif URL.type eq "wo">

  <cfif this_works_order.RecordCount EQ '0'>

    <cfquery datasource="agbcodes_ldi">
      INSERT INTO works_orders (company_id, company_name, contact_id, first_name, last_name, telephone, email, seller, linked_quote_id)
      VALUES ('#this_quote.company_id#', "#this_quote.company_name#", '#this_quote.contact_id#', "#this_quote.first_name#", "#this_quote.last_name#", "#this_quote.telephone#", "#this_quote.email#", "#this_quote.seller#", "#URL.id#")
    </cfquery>

    <cfquery name="new_wo" datasource="agbcodes_ldi">
      SELECT MAX(id) AS lastid
      FROM works_orders
    </cfquery>

    <cfquery datasource="agbcodes_ldi">
      INSERT INTO job_vs_works_order (job_id, w_order_id)
      VALUES ("#this_job.id#", "#new_wo.lastid#")
    </cfquery>

    <cfquery datasource="agbcodes_ldi">
        UPDATE quotes
        SET q_status = "#job_status#",
        linked_wo_id = "#new_wo.lastid#"
        WHERE id = #URL.id#
    </cfquery>    

  </cfif>

  <cflocation url="../works_order.cfm?id=#URL.id#" addtoken="no">

</cfif>