<cfif isDefined("URL.quote_id") AND URL.quote_id neq "">

    <cfquery name="this_quote" datasource="agbcodes_ldi">
        SELECT company_id, contact_id
        FROM quotes
        WHERE id = "#URL.quote_id#"
    </cfquery>

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO jobs (quote_id, company_id, contact_id)
		VALUES ("#URL.quote_id#", "#this_quote.company_id#", "#this_quote.contact_id#")
	</cfquery>

	<cfquery name="job" datasource="agbcodes_ldi">
		SELECT MAX(id) AS lastid
		FROM jobs
	</cfquery>

	<cfquery datasource="agbcodes_ldi">
		INSERT INTO job_vs_quote (quote_id, job_id)
		VALUES ("#URL.quote_id#", "#job.lastid#")
	</cfquery>

	<cflocation url="../contract_details.cfm?id=#job.lastid#" addtoken="no">

</cfif>
