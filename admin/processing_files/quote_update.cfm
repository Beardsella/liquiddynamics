<cfdump var="#form#">

<cfset quote_date_formated = "#DateFormat(FORM.quote_dated, 'yyyy-mm-dd')#">
<cfset exp_date_formated = "#DateFormat(FORM.quote_expiration_date, 'yyyy-mm-dd')#">

<cfquery name="this_quote_params" datasource="agbcodes_ldi">
	SELECT *
	FROM quote_params
	WHERE quote_id = '#FORM.quote_id#'
</cfquery>

<cfquery name="this_quote_items" datasource="agbcodes_ldi">
	SELECT *
	FROM quote_items
	WHERE quote_id = '#FORM.quote_id#'
</cfquery>

<cfquery name="this_quote_extra_items" datasource="agbcodes_ldi">
	SELECT *
	FROM quote_extra_items
	WHERE quote_id = '#FORM.quote_id#'
</cfquery>

<cfquery datasource="agbcodes_ldi">
	UPDATE quotes
	SET cust_enquiry_ref = "#URLEncodedFormat(FORM.cust_enquiry_ref)#",
		quote_intro = "#URLEncodedFormat(FORM.quote_intro)#",
		notes = "#URLEncodedFormat(FORM.notes)#",
		freebies = "#URLEncodedFormat(FORM.freebies)#",
		not_included = "#URLEncodedFormat(FORM.not_included)#",
		sale_condtions = "#URLEncodedFormat(FORM.sale_condtions)#",
		terms_of_payment = "#URLEncodedFormat(FORM.terms_of_payment)#",
		summary = "#URLEncodedFormat(FORM.quote_summary)#"

		<cfif FORM.quote_dated NEQ "">
			, quote_dated = <cfqueryparam value="#quote_date_formated#" cfsqltype="cf_sql_timestamp">
		</cfif>
		<cfif FORM.quote_expiration_date NEQ "">
			, quote_vaild_for = <cfqueryparam value="#exp_date_formated#" cfsqltype="cf_sql_timestamp">
		</cfif>

	WHERE id = '#form.quote_id#'
</cfquery>

<!-- CHECK IF Quote ITEMS need inserting or updating -->
<cfif this_quote_items.recordcount NEQ "" OR this_quote_items.recordcount NEQ '0'>

	<cfoutput>

		<h3> this_quote_items.recordcount (#this_quote_items.recordcount#) IS NOT NULL OR 0 </h3>

		<cfloop from="1" to="#FORM.number_of_items#" index="i">

		<cfset this_current_item_name = "FORM.item_#i#_damper_type">
		<cfset this_current_item_name = "#Evaluate(this_current_item_name)#">
		<cfset this_current_item_name = "#URLEncodedFormat(this_current_item_name)#">


		<cfset this_current_item_qty = "FORM.item_#i#_qty">
		<cfset this_current_item_qty = "#Evaluate(this_current_item_qty)#">


		<cfset this_current_discount = "FORM.discount_percent_#i#">
		<cfset this_current_discount = "#Evaluate(this_current_discount)#">


		<cfset this_current_discount_notes = "FORM.discount_notes_#i#">
		<cfset this_current_discount_notes = "#Evaluate(this_current_discount_notes)#">
		<cfset this_current_discount_notes = "#URLEncodedFormat(this_current_discount_notes)#">

		<cfset this_current_item_price = "FORM.item_price_#i#">
		<cfset this_current_item_price = "#Evaluate(this_current_item_price)#">
		<cfset this_current_item_price = "#this_current_item_price#">


		<cfset this_current_item_currency = "FORM.item_currency_#i#">
		<cfset this_current_item_currency = "#Evaluate(this_current_item_currency)#">
		<cfset this_current_item_currency = "#URLEncodedFormat(this_current_item_currency)#">


		<cfset this_current_item_conversion = "FORM.item_conversion_#i#">
		<cfset this_current_item_conversion = "#Evaluate(this_current_item_conversion)#">
		<cfset this_current_item_conversion = "#DecimalFormat(this_current_item_conversion)#">


		<cfset this_current_item_working_pressure = "FORM.item_#i#_working_pressure">
		<cfset this_current_item_working_pressure = "#Evaluate(this_current_item_working_pressure)#">
		<cfset this_current_item_working_pressure = "#URLEncodedFormat(this_current_item_working_pressure)#">

		<cfset this_current_item_working_pressure_units = "FORM.item_#i#_working_pressure_units">
		<cfset this_current_item_working_pressure_units = "#Evaluate(this_current_item_working_pressure_units)#">
		<cfset this_current_item_working_pressure_units = "#URLEncodedFormat(this_current_item_working_pressure_units)#">


		<cfset this_current_item_cushion_prefill_pressure = "FORM.item_#i#_cushion_prefill_pressure">
		<cfset this_current_item_cushion_prefill_pressure = "#Evaluate(this_current_item_cushion_prefill_pressure)#">
		<cfset this_current_item_cushion_prefill_pressure = "#URLEncodedFormat(this_current_item_cushion_prefill_pressure)#">


		<cfset this_current_item_design_pressure = "FORM.item_#i#_design_pressure">
		<cfset this_current_item_design_pressure = "#Evaluate(this_current_item_design_pressure)#">
		<cfset this_current_item_design_pressure = "#URLEncodedFormat(this_current_item_design_pressure)#">

		<cfset this_current_item_design_pressure_units = "FORM.item_#i#_design_pressure_units">
		<cfset this_current_item_design_pressure_units = "#Evaluate(this_current_item_design_pressure_units)#">
		<cfset this_current_item_design_pressure_units = "#URLEncodedFormat(this_current_item_design_pressure_units)#">


		<cfset this_current_item_length = "FORM.item_#i#_length">
		<cfset this_current_item_length = "#Evaluate(this_current_item_length)#">
		<cfset this_current_item_length = "#URLEncodedFormat(this_current_item_length)#">

		<cfset this_current_item_width = "FORM.item_#i#_width">
		<cfset this_current_item_width = "#Evaluate(this_current_item_width)#">
		<cfset this_current_item_width = "#URLEncodedFormat(this_current_item_width)#">


			<cfset this_current_preventer_type = "FORM.item_#i#_preventer_type">
			<cfset this_current_preventer_type = "#Evaluate(this_current_preventer_type)#">
			<cfset this_current_preventer_type = "#URLEncodedFormat(this_current_preventer_type)#">


			<cfset this_current_volume = "FORM.item_#i#_volume">
			<cfset this_current_volume = "#Evaluate(this_current_volume)#">
			<cfset this_current_volume = "#URLEncodedFormat(this_current_volume)#">


			<cfset this_current_membrane = "FORM.item_#i#_membrane">
			<cfset this_current_membrane = "#Evaluate(this_current_membrane)#">
			<cfset this_current_membrane = "#URLEncodedFormat(this_current_membrane)#">


			<cfset this_current_PRESS = "FORM.item_#i#_pressure">
			<cfset this_current_PRESS = "#Evaluate(this_current_PRESS)#">
			<cfset this_current_PRESS = "#URLEncodedFormat(this_current_PRESS)#">


			<cfset this_current_item_connection = "FORM.item_#i#_part_connection">
			<cfset this_current_item_connection = "#Evaluate(this_current_item_connection)#">
			<cfset this_current_item_connection = "#URLEncodedFormat(this_current_item_connection)#">


			<cfset this_current_soild_material = "FORM.item_#i#_soild_material">
			<cfset this_current_soild_material = "#Evaluate(this_current_soild_material)#">
			<cfset this_current_soild_material = "#URLEncodedFormat(this_current_soild_material)#">


			<cfset this_current_item_misc = "FORM.item_#i#_other">
			<cfset this_current_item_misc = "#Evaluate(this_current_item_misc)#">
			<cfset this_current_item_misc = "#URLEncodedFormat(this_current_item_misc)#">


			<cfset this_current_wetted_material = "FORM.item_#i#_wetted_material">
			<cfset this_current_wetted_material = "#Evaluate(this_current_wetted_material)#">
			<cfset this_current_wetted_material = "#URLEncodedFormat(this_current_wetted_material)#">


			<cfset this_current_non_wetted_material = "FORM.item_#i#_non_wetted_material">
			<cfset this_current_non_wetted_material = "#Evaluate(this_current_non_wetted_material)#">
			<cfset this_current_non_wetted_material = "#URLEncodedFormat(this_current_non_wetted_material)#">


			<cfset this_current_flexing_membrane = "FORM.item_#i#_flexing_membrane">
			<cfset this_current_flexing_membrane = "#Evaluate(this_current_flexing_membrane)#">
			<cfset this_current_flexing_membrane = "#URLEncodedFormat(this_current_flexing_membrane)#">


			<cfset this_current_paint_spec = "FORM.item_#i#_paint_spec">
			<cfset this_current_paint_spec = "#Evaluate(this_current_paint_spec)#">
			<cfset this_current_paint_spec = "#URLEncodedFormat(this_current_paint_spec)#">


			<cfset this_currrent_damper_type = "FORM.item_#i#_damper_type">
			<cfset this_currrent_damper_type = "#Evaluate(this_currrent_damper_type)#">
			<cfset this_currrent_damper_type = "#URLEncodedFormat(this_currrent_damper_type)#">


		<cfset this_current_item_id = "FORM.item_#i#_id">
		<cfset this_current_item_id = "#Evaluate(this_current_item_id)#">
		<cfset this_current_item_id = '#NumberFormat(this_current_item_id)#'>


		<cfset this_current_design_code = "FORM.item_#i#_design_code">
		<cfset this_current_design_code = "#Evaluate(this_current_design_code)#">
		<cfset this_current_design_code = "#URLEncodedFormat(this_current_design_code)#">

		<cfset this_current_item_row = '#i#'>


		<!-- COUNT NUMBER OF ITEMS AND LOOP -->
		<cfquery datasource="agbcodes_ldi">
			UPDATE quote_items
			SET quote_id = '#FORM.quote_id#',
				item_name = "#this_current_item_name#",
				item_qty = '#this_current_item_qty#',
				item_price = '#this_current_item_price#',
				currency = "#this_current_item_currency#",
				conversion = '#this_current_item_conversion#',
				discount_percent = '#this_current_discount#',
				discount_notes = "#this_current_discount_notes#",
				working_pressure = "#this_current_item_working_pressure#",
				working_pressure_units = "#this_current_item_working_pressure_units#",
				design_pressure = "#this_current_item_design_pressure#",
				design_pressure_units = "#this_current_item_design_pressure_units#",
				cushion_prefill_pressure = "#this_current_item_cushion_prefill_pressure#",
				length = "#this_current_item_length#",
				width = "#this_current_item_width#",
				preventer_type = "#this_current_preventer_type#",
				volume = "#this_current_volume#",
				membrane = "#this_current_membrane#",
				pressure = "#this_current_PRESS#",
				connection = "#this_current_item_connection#",
				solid_material ="#this_current_soild_material#",
				miscellaneous = "#this_current_item_misc#",
				wetted_material = "#this_current_wetted_material#",
				non_wetted_material = "#this_current_non_wetted_material#",
				flexing_membrane = "#this_current_flexing_membrane#",
				paint_spec = "#this_current_paint_spec#",
				damper_type = "#this_currrent_damper_type#",
				design_code = "#this_current_design_code#"
			WHERE id = '#this_current_item_id#'
		</cfquery>

		</cfloop>

	</cfoutput>

</cfif>
<!-- END OF ITEMS updating -->


<!-- CHECK IF Quote has Maintenance ITEMS that need updating -->
<cfif this_quote_extra_items.recordcount NEQ "" OR this_quote_extra_items.recordcount NEQ '0'>

	<cfloop query="this_quote_extra_items">


		<cfset this_maintenance_item_refrence = "FORM.maintenance_item_#CurrentRow#">
		<cfset this_maintenance_item_refrence = "#Evaluate(this_maintenance_item_refrence)#">
		<cfset this_maintenance_item_refrence = "#URLEncodedFormat(this_maintenance_item_refrence)#">


		<cfset this_maintenance_item_notes = "FORM.maintenance_item_notes_#CurrentRow#">
		<cfset this_maintenance_item_notes = "#Evaluate(this_maintenance_item_notes)#">
		<cfset this_maintenance_item_notes = "#URLEncodedFormat(this_maintenance_item_notes)#">


		<cfset this_maintenance_item_price = 'FORM.maintenance_price_each_#CurrentRow#'>
		<cfset this_maintenance_item_price = '#Evaluate(this_maintenance_item_price)#'>
		<cfset this_maintenance_item_price = '#DecimalFormat(this_maintenance_item_price)#'>


		<cfset this_maintenance_item_currency = "FORM.maintenance_currency_#CurrentRow#">
		<cfset this_maintenance_item_currency = "#Evaluate(this_maintenance_item_currency)#">
		<cfset this_maintenance_item_currency = "#URLEncodedFormat(this_maintenance_item_currency)#">


		<cfset this_maintenance_item_conversion = 'FORM.maintenance_conversion_rate_#CurrentRow#'>
		<cfset this_maintenance_item_conversion = '#Evaluate(this_maintenance_item_conversion)#'>
		<cfset this_maintenance_item_conversion = '#DecimalFormat(this_maintenance_item_conversion)#'>


		<cfset this_maintenance_item_id = 'FORM.maintenance_item_#CurrentRow#_id'>
		<cfset this_maintenance_item_id = '#Evaluate(this_maintenance_item_id)#'>


		<cfquery datasource="agbcodes_ldi">
			UPDATE quote_extra_items
			SET quote_id = '#FORM.quote_id#',
				item_refrence = '#this_maintenance_item_refrence#',
				item_notes = "#this_maintenance_item_notes#",
				item_price = '#this_maintenance_item_price#',
				currency = "#this_maintenance_item_currency#",
				conversion = '#this_maintenance_item_conversion#'
			WHERE id = '#this_maintenance_item_id#'
		</cfquery>

	</cfloop>

</cfif>

<cflocation url="../quote_management.cfm?id=#form.quote_id#" addtoken="no">