<!DOCTYPE html>
<html lang="en">
<head>

  <cfquery name="this_contact"  datasource="agbcodes_ldi">
    SELECT *
    FROM contacts
    WHERE last_name = "#URLEncodedFormat(URL.q)#"
  </cfquery>

  <cfquery name="these_contacts"  datasource="agbcodes_ldi">
    SELECT *
    FROM contacts
    WHERE company = "#this_contact.company#"
  </cfquery>


</head>
<body>
    <cfoutput>

<div class="row">

<cfif this_contact.recordcount GT 0>
  <div class="col-12 alert alert-success alert-dismissible fade show" role="alert">
    The contact details for "#URLDecode(this_contact.first_name)# #URLDecode(this_contact.last_name)#" has been populated
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  </div>
<cfelse>

  <div class="alert alert-info col-12  alert-dismissible fade show" role="alert">
    New contact detected!
  </div>
</cfif>
           
  <input type="hidden" name="contact_id" id="contact_id" value="#this_contact.id#">

            <div class="col-lg-6">
              <div class="form-group">
              <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" value="#URLDecode(this_contact.first_name)#" list="first_name_list">
                </div>
            </div>

            <datalist id="first_name_list">
              <cfloop query="these_contacts">
                <option value="#URLDecode(first_name)#">
              </cfloop>
            </datalist>

            <div class="col-lg-6">
              <div class="form-group">
              <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" value="#URLDecode(URL.q)#" list="last_name_list" onchange="fill_in_contact(this.value)">
              </div>
            </div>

            <datalist id="last_name_list">
              <cfloop query="these_contacts">
                <option value="#URLDecode(last_name)#">
              </cfloop>
            </datalist>

        </div>

        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_email">Email Address</label>
              <input type="email" class="form-control" id="contact_email" name="contact_email" value="#URLDecode(this_contact.email)#">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_telephone">Phone Number</label>
              <input type="text" class="form-control" id="contact_telephone" name="contact_telephone" value="#URLDecode(this_contact.telephone)#">
              </div>
            </div>
        </div> 

          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="##contact_details" aria-expanded="false" aria-controls="contact_details">
            More Contact Details...
          </button>
        </p>
        
        <div class="collapse" id="contact_details">
        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="staff_role">Staff Role</label>
              <input type="text" class="form-control" id="staff_role" name="staff_role" value="#URLDecode(this_contact.staff_role)#">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_country_based">Country Based</label>
              <input type="text" class="form-control" id="contact_country_based" name="contact_country_based" value="#URLDecode(this_contact.country_based)#">
              </div>
            </div>
        </div> 

        <div class="form-group">
        <label for="contact_notes">Contact Notes</label>
          <textarea class="form-control" id="contact_notes" name="contact_notes">#URLDecode(this_contact.notes)#</textarea>
        </div>

        </div>

        </cfoutput>

</body>
</html>