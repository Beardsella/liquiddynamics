<!-- Logout -->
<cfif IsDefined ("session.userinitials")>

  <!--  write away logout time  -->
	<cfquery datasource="agbcodes_ldi">
		INSERT INTO login_log (login_by, login_ip, logged_in_out) 
		VALUES ('#session.username#', '#CGI.REMOTE_ADDR#', 'OUT')
	</cfquery>

</cfif>

<cfset session.username = "">
<cfset session.slevel = '999'>
<cfset session.userid = "">
<cfset session.userpic = "">
<cfset session.sessionid = "">

<cflogout>

<cflocation url="../../index.cfm?action=logged_out" addtoken="no">