<cfdump var="#form#">

<cfquery datasource="agbcodes_ldi">
  INSERT INTO quote_item_params (quote_id, item_id, param, param_value, param_notes)
  VALUES ('#FORM.quote_id#', '#FORM.item_id#', "#URLEncodedFormat(FORM.NEW_ITEM_PARAMETER_NAME)#", "#URLEncodedFormat(FORM.NEW_ITEM_PARAMETER_VALUE)#", "#URLEncodedFormat(FORM.NEW_ITEM_PARAMETER_NOTES)#")
</cfquery>

<cflocation URL="../quote_management.cfm?id=#FORM.quote_id#" addtoken="no">