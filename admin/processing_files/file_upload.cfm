<cfoutput>

  <cfquery name="job" datasource="agbcodes_ldi">
    SELECT *
    FROM jobs
    WHERE id = '#URL.id#'
  </cfquery>

  <cfset relative_path = ExpandPath("../contracts/#job.formatted_id#/")>

  <cfset path = "../contracts/#job.formatted_id#/">

  <!-- Accepted File types -->
  <cfset accepted_file_types ="application/pdf">

  <!-- Start of Secure upload -->
  <cfsetting requesttimeout="3600">

  <cfset accept_upload = 1>

  <!-- upload new file -->
  <cfif IsDefined("FORM.new_file") AND FORM.new_file NEQ "">


      <!-- only allow these mime types. Edit as required -->
    <cffile accept="#accepted_file_types#"
            action="upload"
            destination="#getTempDirectory()#"
            file="#FORM.new_file#"
            nameconflict="makeunique"
            result="temp_uploadResult">

    <cfset new_name_with_no_spaces = #REReplace("#FORM.new_file_name#","[^0-9A-Za-z &/.]","","all")# >

    <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', ' ', '-', 'All')>

    <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '/', '-', 'All')>

    <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '--', '', 'All')>

    <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '--', '', 'All')>

    <cfset new_name_with_no_spaces = Replace('#new_name_with_no_spaces#', '&', 'and', 'All')>

    <cfset new_name_with_no_spaces = "#LCase(new_name_with_no_spaces)#">

    <cfset file_ext="#ListLast('#temp_uploadResult.clientFile#' , '.')#">

   <cfquery name="dupe_check" datasource="agbcodes_ldi">
      SELECT file_name
      FROM uploaded_files
    </cfquery>

  <cfif dupe_check.file_name eq "#new_name_with_no_spaces#">

  <cflocation url="#CGI.http_referer#?error=dupe_file_name" addtoken="false">

  </cfif>

  <cftry>

    <!-- Move the file. The expanded file path is normally already set in the page header -->
    <cffile action="move"
          destination="#relative_path#/#new_name_with_no_spaces#.#file_ext#"
          source="#getTempDirectory()##temp_uploadResult.clientFile#"
          nameconflict="error"
          result="final_file">

      <cfcatch>
      
      <!-- If the file name aready exists -->
      <cfset file_name_append = timeformat(now(),'hh_mm_ss_L')>       
      <cfset new_name_with_no_spaces = "#new_name_with_no_spaces##file_name_append#">

          <!-- Move the file. The expanded file path is normally already set in the page header -->
        <cffile action="move"
            destination="#relative_path##new_name_with_no_spaces#.#file_ext#"
            source="#getTempDirectory()##temp_uploadResult.clientFile#"
            nameconflict="error"
            result="final_file">

      </cfcatch>
  
  </cftry>
  
    <cfquery datasource="agbcodes_ldi">
      INSERT INTO  uploaded_files (file_name, location, file_type, linked_job, doc_purpose)
      VALUES ("#new_name_with_no_spaces#.#file_ext#", "#path#", "#file_ext#", "#job.formatted_id#", "#URLEncodedFormat(FORM.doc_purpose)#")
    </cfquery>

    <cfquery name="fresh_file" datasource="agbcodes_ldi">
      SELECT MAX(id) AS last_id
      FROM uploaded_files
    </cfquery>

    <cfquery name="test_fresh_file" datasource="agbcodes_ldi">
      SELECT *
      FROM uploaded_files
      WHERE id = "#fresh_file.last_id#"
    </cfquery>


    <h1>#relative_path##new_name_with_no_spaces#.#file_ext#</h1>
    <a href="#relative_path##new_name_with_no_spaces#.#file_ext#">#test_fresh_file.file_name#</a>

  </cfif>

</cfoutput>

<cflocation url="CGI.httpreferrer" addtoken="false">
