<cfdump var="#form#">

<cfparam name="FORM.company_id" default="null">
<cfparam name="FORM.contact_id" default="null">
<cfparam name="FORM.ENQUIRY_EMAIL" default="null">

<cfif NOT isDefined(FORM.company_id) OR FORM.company_id EQ "null">

  <cfquery datasource="agbcodes_ldi">
    INSERT INTO companies 
      (company_name, 
      email, 
      telephone, 
      mail_1, 
      town, 
      postcode, 
      country, 
      notes)
    VALUES ("#FORM.company#", 
        "#FORM.company_email#", 
        "#FORM.company_telephone#",
        "#FORM.company_address#",
        '#FORM.company_town#',
        "#FORM.company_postcode#",
        "#FORM.company_country#",
        "#FORM.company_notes#")
  </cfquery>

  <cfquery name="companies" datasource="agbcodes_ldi">
    SELECT MAX(id) AS lastid
    FROM companies
  </cfquery>

<cfelse>

  <cfquery name="companies" datasource="agbcodes_ldi">
    SELECT id AS lastid
    FROM companies
    WHERE id = "#FORM.company_id#"
  </cfquery>

</cfif>


<cfif FORM.contact_id EQ "null">

  <cfquery datasource="agbcodes_ldi">
    INSERT INTO contacts 
      (first_name, 
      last_name, 
      email, 
      telephone, 
      staff_role, 
      country_based, 
      notes, 
      company,
      company_id)
    VALUES ("#FORM.first_name#", 
        "#FORM.last_name#", 
        "#FORM.enquiry_email#",
        "#FORM.contact_telephone#",
        '#FORM.staff_role#',
        "#FORM.contact_country_based#",
        "#FORM.contact_notes#",
        "#FORM.company#",
        "#companies.lastid#")
  </cfquery>

  <cfquery name="contacts" datasource="agbcodes_ldi">
    SELECT MAX(id) AS lastid
    FROM contacts
  </cfquery>

<cfelse>

  <cfquery name="contacts" datasource="agbcodes_ldi">
    SELECT id AS lastid
    FROM contacts
    WHERE id = "#FORM.contact_id#"
  </cfquery>

</cfif>



<cfif isDefined("FORM.seller") AND FORM.seller neq "">

  <cfset selling_company = "#URLDecode(FORM.seller)#">

  <cfif selling_company eq "Liquid Dynamics International">
    <cfset company_prefix = "LDI-UK-ENQ-">
  <cfelseif selling_company eq "Pulse Gard">
    <cfset company_prefix = "PG-UK-ENQ-">
  <cfelseif selling_company eq "Shock Gard">
    <cfset company_prefix = "SG-UK-ENQ-">
  <cfelseif selling_company eq "Hydrotrole">
    <cfset company_prefix = "HT-UK-ENQ-">
  </cfif>

</cfif>


<cfquery datasource="agbcodes_ldi">
  INSERT INTO enquirys_pump_systems 
    (seller,
    company_id,
    contact_id,
    problem,
    pump_type, 
    num_of_displacers, 
    flow_rate, 
    flow_rate_units, 
    speed_value,
    SPMorRPM,
    stroke_displacement,
    piston_diameter,
    stroke_length,
    damping_degree,
    material_wetted,
    material_non_wetted,
    plastic_system_materials,
    pumped_liquid,
    pipe_size,
    connection_type,
    connection_type_2,
    system_temp, 
    system_temp_units, 
    system_design_temp, 
    system_design_temp_units, 
    max_system_pressure, 
    max_system_pressure_units, 
    system_design_pressure,
    system_design_pressure_units, 
    actioned,
    action_date,
    notes)
  VALUES ("#FORM.seller#",
      "#companies.lastid#",
      "#contacts.lastid#",
      "#URLEncodedFormat(FORM.problem)#",
      "#URLEncodedFormat(FORM.pump_type)#",
      "#URLEncodedFormat(FORM.num_of_displacers)#",
      '#FORM.flow_rate#',
      "#URLEncodedFormat(FORM.flow_rate_units)#",
      "#URLEncodedFormat(FORM.speed_value)#",
      "#URLEncodedFormat(FORM.SPMorRPM)#",
      "#URLEncodedFormat(FORM.stroke_displacement)#",
      "#URLEncodedFormat(FORM.piston_diameter)#",
      "#URLEncodedFormat(FORM.stroke_length)#",
      "#URLEncodedFormat(FORM.damping_degree)#",
      "#URLEncodedFormat(FORM.material_wetted)#",
      "#URLEncodedFormat(FORM.material_non_wetted)#",
      "#URLEncodedFormat(FORM.plastic_system_materials)#",
      "#URLEncodedFormat(FORM.pumped_liquid)#",
      "#URLEncodedFormat(FORM.pipe_size)#",
      "#URLEncodedFormat(FORM.connection_type)#",
      "#URLEncodedFormat(FORM.connection_type_2)#",
      "#URLEncodedFormat(FORM.system_temp)#",
      "#URLEncodedFormat(FORM.system_temp_units)#",
      "#URLEncodedFormat(FORM.system_design_temp)#",
      "#URLEncodedFormat(FORM.system_design_temp_units)#",
      "#URLEncodedFormat(FORM.max_system_pressure)#",
      "#URLEncodedFormat(FORM.max_system_pressure_units)#",
      "#URLEncodedFormat(FORM.system_design_pressure)#",
      "#URLEncodedFormat(FORM.system_design_pressure_units)#",
      "#URLEncodedFormat(FORM.actioned)#",
      "#URLEncodedFormat(FORM.action_date)#",
      "#URLEncodedFormat(FORM.enquiry_notes)#")
</cfquery>

<cfquery name="last_enq" datasource="agbcodes_ldi">
  SELECT MAX(id) AS lastid
  FROM enquirys_pump_systems
</cfquery>

<cfset selling_company = "#URLDecode(FORM.seller)#">

<cfif selling_company eq "Liquid Dynamics International">
  <cfset formatted_id = "LDI-UK-ENQ-#last_enq.lastid#">
<cfelseif selling_company eq "Pulse Gard">
  <cfset formatted_id = "PG-UK-ENQ-#last_enq.lastid#">
<cfelseif selling_company eq "Shock Gard">
  <cfset formatted_id = "SG-UK-ENQ-#last_enq.lastid#">
<cfelseif selling_company eq "Hydrotrole">
  <cfset formatted_id = "HT-UK-ENQ-#last_enq.lastid#">
</cfif>


<cfquery datasource="agbcodes_ldi">
  UPDATE enquirys_pump_systems
  SET formatted_id = "#formatted_id#"
  WHERE id = "#last_enq.lastid#"
</cfquery>


<!--- <cfmail to="#FORM.enquiry_email#" bcc="beardsella@gmail.com" from="adam@publicquest.co.uk" server="mail.publicquest.co.uk" username="steve@publicquest.co.uk" password="$9559$68" useSSL="no" port="25" subject="Liquid Dynamics Enquiry" type="html">

  <p>Thanks #FORM.enquiry_name#</p>
  <p>We've recieved your enquiry at #now()#, we'll try and get back to you ASAP.</p>
  <p>Liquid Dynamics</p>

</cfmail> --->

<cflocation	url="../enquiries-overview.cfm" addtoken="no">