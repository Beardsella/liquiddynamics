<cfdump var="#form#">

<cfif FORM.company_id EQ "null">

  <cfquery datasource="agbcodes_ldi">
    INSERT INTO companies 
      (company_name, 
      email, 
      telephone, 
      mail_1, 
      town, 
      postcode, 
      country, 
      notes)
    VALUES ("#FORM.company#", 
        "#FORM.company_email#", 
        "#FORM.company_telephone#",
        "#FORM.company_address#",
        '#FORM.company_town#',
        "#FORM.company_postcode#",
        "#FORM.company_country#",
        "#FORM.company_notes#")
  </cfquery>

  <cfquery name="companies" datasource="agbcodes_ldi">
    SELECT MAX(id) AS lastid
    FROM companies
  </cfquery>

<cfelse>

  <cfquery name="companies" datasource="agbcodes_ldi">
    SELECT id AS lastid
    FROM companies
    WHERE id = "#FORM.company_id#"
  </cfquery>

</cfif>


<cfif FORM.contact_id EQ "null">

  <cfquery datasource="agbcodes_ldi">
    INSERT INTO contacts 
      (first_name, 
      last_name, 
      email, 
      telephone, 
      staff_role, 
      country_based, 
      notes, 
      company,
      company_id)
    VALUES ("#FORM.first_name#", 
        "#FORM.last_name#", 
        "#FORM.enquiry_email#",
        "#FORM.contact_telephone#",
        '#FORM.staff_role#',
        "#FORM.contact_country_based#",
        "#FORM.contact_notes#",
        "#FORM.company#",
        "#companies.lastid#")
  </cfquery>

  <cfquery name="contacts" datasource="agbcodes_ldi">
    SELECT MAX(id) AS lastid
    FROM contacts
  </cfquery>

<cfelse>

  <cfquery name="contacts" datasource="agbcodes_ldi">
    SELECT id AS lastid
    FROM contacts
    WHERE id = "#FORM.contact_id#"
  </cfquery>

</cfif>

<cfquery datasource="agbcodes_ldi">
  INSERT INTO enquirys_pump_systems 
    (problem,
    pump_type, 
    num_of_displacers, 
    flow_rate, 
    flow_rate_units, 
    speed_value,
    SPMorRPM,
    stroke_displacement,
    piston_diameter,
    stroke_length,
    stroke_displacement,
    material_wetted,
    material_non_wetted,
    plastic_system_materials,
    pumped_liquid, 
    system_temp, 
    system_temp_units, 
    system_design_temp, 
    system_design_temp_units, 
    max_system_pressure, 
    max_system_pressure_units, 
    system_design_pressure,
    system_design_pressure_units, 
    pipe_size, 
    connection_type, 
    connection_type_2, 
    metalic_system_materials, 
    plastic_system_materials,
    contact_id,
    company_id,
    actioned,
    action_date,
    notes)
  VALUES ("#FORM.problem#",
      "#FORM.pump_type#",
      "#FORM.num_of_displacers#",
      '#FORM.flow_rate#',
      "#FORM.flow_rate_units#",
      "#FORM.speed_value#",
      "#FORM.SPMorRPM#",
      "#FORM.stroke_displacement#"
      "#FORM.piston_diameter#",
      "#FORM.stroke_length#",
      "#FORM.stroke_displacement#",
      "#FORM.material_wetted#",
      "#FORM.material_non_wetted#",
      "#FORM.plastic_system_materials#",
      "#FORM.pumped_liquid#",
      '#FORM.system_temp#',
      '#FORM.system_temp_units#',
      '#FORM.system_design_temp#',
      '#FORM.system_design_temp_units#',
      '#FORM.max_system_pressure#',
      '#FORM.max_system_pressure_units#',
      '#FORM.system_design_pressure#',
      '#FORM.system_design_pressure_units#',
      "#FORM.pipe_size#",
      "#FORM.connection_type#",
      "#FORM.connection_type_2#",
      "#FORM.metalic_system_materials#",
      "#FORM.plastic_system_materials#",
      "#contacts.lastid#",
      "#companies.lastid#",
      "#FORM.actioned#",
      "#FORM.action_date#",
      "#FORM.enquiry_notes#")
</cfquery>

<!--- <cfmail to="#FORM.enquiry_email#" bcc="beardsella@gmail.com" from="adam@publicquest.co.uk" server="mail.publicquest.co.uk" username="steve@publicquest.co.uk" password="$9559$68" useSSL="no" port="25" subject="Liquid Dynamics Enquiry" type="html">

  <p>Thanks #FORM.enquiry_name#</p>
  <p>We've recieved your enquiry at #now()#, we'll try and get back to you ASAP.</p>
  <p>Liquid Dynamics</p>

</cfmail> --->

<cflocation	url="../thanks.cfm" addtoken="no">