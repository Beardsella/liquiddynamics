<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favicon.ico">
  <meta name="author" content="Adam Beardsell - http://agb.codes">
  <meta name="description" content="Liquid Dynamics International - Engineering and manufacturing services">
  <meta name="keywords" content="Liquid Dynamics, Engineering, Oil Rig Manufacturing Specialists">

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/custom_pdf.css" rel="stylesheet">

  <cfquery name="this_quote" datasource="agbcodes_ldi">
    SELECT *
    FROM quotes
    WHERE id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quote_params" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_params
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quotes_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_items
    WHERE quote_id = "#URL.id#"
  </cfquery>
  
  <cfquery name="this_quote_extra_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_extra_items
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfset quote_date_formated = "#DateFormat(this_quote.quote_dated, 'dd/mm/yyyy')#">
  <cfset dispatch_date_formated = "#DateFormat(this_quote.dispatch_estimate, 'dd/mm/yyyy')#">

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>



<script type="text/javascript" src="js/html2canvas.js"></script>
  <script type="text/javascript" src="js/jspdf.min.js"></script>
  <script type="text/javascript">

  var url_string = window.location.href; 
  var url = new URL(url_string);
  var quote_file_name = url.searchParams.get("id");
  var quote_file_name = "LDI-Quote-UK-" + quote_file_name + ".pdf";
  console.log(quote_file_name);
  console.log(window.devicePixelRatio);

  function genPDF()
  {
   html2canvas(document.body,{
   onrendered:function(canvas){

   var img=canvas.toDataURL("image/png");
   var doc = new jsPDF();
   doc.addImage(img,'JPEG',10,10);
   doc.save(quote_file_name);
   }

   });

  }
</script>

</head>
<body>
<div id="main_content" html2canvas-container="true">

<cfoutput query="this_quote">

      <div class="row color-invoice">
      <div class="col-6">
        <h5>Quote Refrence##: LDI-UK-#id#</h5>
      </div>
      <div class="col-6 text-right">
        <p class="lead">#quote_dated#</p>
      </div>
      <div class="col-12">
        <div class="row">
          <div class="col-lg-7 col-md-7 col-sm-7">
            <h1>LDI QUOTE</h1>
            <br />
            <strong>Email : </strong> info@ldi.co.uk
            <br />
            <strong>Call : </strong> +44 161 480 9625
          </div>
          <div class="col-lg-5 col-md-5 col-sm-5">

            <h2>Liquid Dynamics International</h2> 
            716 Greg Street, Reddish
            <br> Manchester, UK.
            <br> SK5 LDI

          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-lg-7 col-md-7 col-sm-7">
            <h3>Client Details : </h3>
            <h5>#URLDecode(first_name)# - #URLDecode(last_name)#</h5> 789/90 , Lane Here, New York,
            <br /> United States
          </div>
          <div class="col-lg-5 col-md-5 col-sm-5">
            <h3>Client Contact :</h3> Mob: +1-99-88-77-55
            <br> email: info@domainname.com
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <strong>ITEM DESCRIPTION & DETAILS :</strong>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="table-responsive">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>S. No.</th>
                    <th>Perticulars</th>
                    <th>Quantity.</th>
                    <th>Unit Price</th>
                    <th>Sub Total</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>Website Design</td>
                    <td>1</td>
                    <td>5000 USD</td>
                    <td>5000 USD</td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>Website Development</td>
                    <td>2</td>
                    <td>5000 USD</td>
                    <td>10000 USD</td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>Customization</td>
                    <td>1</td>
                    <td>4000 USD</td>
                    <td>4000 USD</td>
                  </tr>
                  <tr>
                    <td>4</td>
                    <td>Plugin Setup</td>
                    <td>1</td>
                    <td>3000 USD</td>
                    <td>3000 USD</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <hr>
            <div>
              <h4>  Total : 22000 USD </h4>
            </div>
            <hr>
            <div>
              <h4>  Taxes : 4400 USD ( 20 % on Total Bill ) </h4>
            </div>
            <hr>
            <div>
              <h3>  Bill Amount : 26400 USD </h3>
            </div>
            <hr />
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <strong> Important: </strong>
            <ol>
              <li>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit.

              </li>
              <li>
                Nulla eros eros, laoreet non pretium sit amet, efficitur eu magna.
              </li>
              <li>
                Curabitur efficitur vitae massa quis molestie. Ut quis porttitor justo, sed euismod tortor.
              </li>
            </ol>
          </div>
        </div>
        <hr />
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <a href="##" class="btn btn-success btn-sm">Print Invoice</a>    
            <a href="##" class="btn btn-info btn-sm">Download In Pdf</a>
          </div>
        </div>
        
        <hr>
        <div class="row">
  

</div>
      </div>
    </div>

  </div>

</cfoutput>
<hr data-html2canvas-ignore="true">

  <div class="col-12 text-center" data-html2canvas-ignore="true">

    <p class="lead">Once your happy with your PDF preview click the button below to generate you PDF</p>

    <a href="javascript:genPDF()" class="btn btn-success">Generate PDF</a>

  </div>

</div>
</body>
</html>