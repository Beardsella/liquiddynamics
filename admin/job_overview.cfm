<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <title>Manage Contracts - Liquid Dynamics Group Ltd</title>

    <cfquery name="all_jobs" datasource="agbcodes_ldi">
        SELECT *
        FROM jobs
    </cfquery>    

    <cfquery name="countries" datasource="agbcodes_ldi">
        SELECT *
        FROM countries
    </cfquery>


</head>
<body>

    <cfinclude template="includes/nav.cfm">

      <!-- Page Content -->
      <main role="main" class="container-fluid">

            <cfoutput>

                <nav aria-label="breadcrumb" role="navigation">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.cfm">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="jobs_overview.cfm">Contracts Overview</a></li>
                  </ol>
                </nav>            

                <div class="col-lg-12">
                    <h1 class="page-header">Contracts Overview</h1>
                    <p class="lead">Here you can see all current Contracts</p>

                <table class="table table-striped table-dark" id="ss_table">

                  <thead>
                      <th>Contract Refrence##</th>
                      <th>Buyer</th>
                      <th>Contact</th>
                      <th>Enquiry</th>
                      <th>Quote</th>
                      <th>Works Order</th>
                      <th>Invoice</th>
                  </thead>

                  <tbody>

                    <cfloop query="all_jobs">




    <cfquery name="this_company" datasource="agbcodes_ldi">
        SELECT *
        FROM companies
        WHERE id = "#company_id#"
    </cfquery>      

    <cfquery name="staff" datasource="agbcodes_ldi">
        SELECT *
        FROM contacts
        WHERE id = "#contact_id#"
    </cfquery>

    <cfquery name="enquiries" datasource="agbcodes_ldi">
        SELECT enquirys_pump_systems.*
        FROM enquirys_pump_systems
        RIGHT JOIN job_vs_enq 
        ON enquirys_pump_systems.id = job_vs_enq.enquiry_id
        WHERE job_vs_enq.job_id = "#id#"
    </cfquery>

    <cfquery name="quotes" datasource="agbcodes_ldi">
        SELECT quotes.*
        FROM quotes
        RIGHT JOIN job_vs_quote 
        ON quotes.id = job_vs_quote.quote_id
        WHERE job_vs_quote.job_id = "#id#"
    </cfquery>

    <cfquery name="w_orders" datasource="agbcodes_ldi">
        SELECT works_orders.*
        FROM works_orders
        RIGHT JOIN job_vs_works_order 
        ON works_orders.id = job_vs_works_order.w_order_id
        WHERE job_vs_works_order.job_id = "#id#"
    </cfquery>

    <cfquery name="invoices" datasource="agbcodes_ldi">
        SELECT invoices.*
        FROM invoices
        RIGHT JOIN job_vs_invoice 
        ON invoices.id = job_vs_invoice.invoice_id
        WHERE job_vs_invoice.job_id = "#id#"
    </cfquery>

    <cfquery name="uploaded_docs" datasource="agbcodes_ldi">
        SELECT uploaded_docs.*
        FROM uploaded_docs
        RIGHT JOIN job_vs_docs 
        ON uploaded_docs.id = job_vs_docs.doc_id
        WHERE job_vs_docs.job_id = "#id#"
    </cfquery>

      <cfif isDefined("quotes.seller") AND quotes.seller neq "">

        <cfset selling_company = "#URLDecode(quotes.seller)#">

        <cfif selling_company eq "Liquid Dynamics International">
          <cfset company_prefix = "LDI-UK">
        <cfelseif selling_company eq "Pulse Gard">
          <cfset company_prefix = "PG-UK">
        <cfelseif selling_company eq "Shock Gard">
          <cfset company_prefix = "SG-UK">
        <cfelseif selling_company eq "Hydrotrole">
          <cfset company_prefix = "HT-UK">
        </cfif>

      </cfif>

                      <tr>
                          <td><a href="contract_details.cfm?id=#id#" class="agb-table-link">#refrence_id#</a></td>
                          <td>#URLDecode(this_company.company_name)#</td>
                          <td>#URLDecode(staff.first_name)# #URLDecode(staff.last_name)#</td>
                          <td><cfloop query="enquiries"><a href="customer-enquiry-edit.cfm?id=#enquiries.id#" class="agb-table-link">#company_prefix#-ENQ-#enquiries.id#</a></cfloop></td>
                          <td><cfloop query="quotes"><a href="quote_management.cfm?id=#quotes.id#" class="agb-table-link">#company_prefix#-QUO-#quotes.id#</a></cfloop></td>
                          <td><cfloop query="w_orders"><a href="works_order.cfm?id=#w_orders.id#" class="agb-table-link">#company_prefix#-WO-#w_orders.id#</a></cfloop></td>
                          <td><cfloop query="invoices"><a href="invoice.cfm?id=#invoices.id#" class="agb-table-link">#company_prefix#-INV-#invoices.id#</a></cfloop></td>

                        </tr>

                      </cfloop>

                  </tbody>                

                </table>


                </div>

                

            </cfoutput>

      </main>

    <hr>
    <!-- End of page main content -->

    <cfinclude template="includes/footer.cfm">
    <cfinclude template="includes/java_include.cfm">

</body>
</html>