<cfquery name="all_order_details">
	SELECT orders.*, order_lines.*
	FROM orders
	JOIN order_lines
	ON orders.id = order_lines.order_id
</cfquery>

<cfoutput query="all_order_details" maxrows="3">

	<cftry>

		<cfmail server="secure.emailsrvr.com" useSSL="yes" port="465" failto="sentinel@publicquest.co.uk" from="info@liquiddynamics.co" to="adam@publicquest.co.uk" bcc="sentinel@publicquest.co.uk" password="rIv@BL3VO" username="info@publicquest.co.uk" subject="liquiddynamics Invoice: #id#" type="html">

			<cfinclude template="email_template.cfm">

		</cfmail>		
		<h1>Success ID: #id#</h1>

		<p>#TimeFormat(now())#</p>

		<cfcatch>
			<h1>Failed ID: #id#</h1>
			<p>#TimeFormat(now())#</p>
		</cfcatch>

	</cftry>

</cfoutput>