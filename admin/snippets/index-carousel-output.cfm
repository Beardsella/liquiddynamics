<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <meta name="description" content="Discover the liquiddynamics, the latest innovation in the protein shaker bottle world. Practical and easily cleaned. Personalise your own liquiddynamics now!">
    <meta name="keywords" content="liquiddynamics,shaker bottle,liquiddynamics shaker bottle">
    
    <title>Crafty Fox: Revolutionary Easy Clean Protein Shaker Bottle</title>



</head>
<body>

<cfinclude template="includes/nav.cfm">

<div id="main_carousel" class="carousel slide col-offset-3 col-span-9" data-ride="carousel">

  <!-- Indicators -->
  <ol class="carousel-indicators">

    <li data-target="##main_carousel" data-slide-to="0" class="active"></li>

    <cfset row_count = '1'>

    <cfoutput query="this_prod" startRow="2">
      <cfset row_count = #row_count# + '1'>
      <li data-target="##main_carousel" data-slide-to="#row_count#"></li>
    </cfoutput>

  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
   

      <cfoutput query="this_prod" maxrows="1">
     
          <div class="item active">
            <a href="#link#"><img src="../#img#" class="img-responsive" alt="#URLDecode(title)#" title="#URLDecode(title)#"></a>
          </div>

      </cfoutput>


        <cfoutput query="this_prod" startRow="2">
     
          <div class="item">
            <a href="#link#"><img src="../#img#" class="img-responsive" alt="#URLDecode(title)#" title="#URLDecode(title)#"></a>
          </div>

        </cfoutput>

  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#main_carousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#main_carousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  
</div>


<div id="two_tone_blue_orange">
    <div class="container">
            <div class="col-xs-6 col-sm-6 col-md-6 dark_bg">
    <h1><a href="liquiddynamics.cfm">liquiddynamics Original</a></h1>
            <div class="col-xs-6 col-sm-6 col-md-6 dark_bg">
    <p>The liquiddynamics original has <strong>700ml</strong> liquid and <strong>150g</strong> supplement capacities, a pill section with a <strong>hinge door</strong>, a <strong>stay away cap</strong> and is available in lots of colours. View more info <strong>here</strong>.</p>

</div>
            <div class="col-xs-6 col-sm-6 col-md-6 dark_bg">

    <img src="images/products/liquiddynamics.gif" class="img-responsive" alt="">
      </div>

<div class="clearfix"></div>
        <a href="liquiddynamics.cfm" class="btn btn-success btn-block btn-lg">Buy now</a>

      </div>

      <div class="col-xs-6 col-sm-6 col-md-6 orange_bg">

          <h1><a href="liquiddynamics-pro.cfm">liquiddynamics Pro</a></h1>

      <div class="col-xs-6 col-sm-6 col-md-6">
    <p>The liquiddynamics pro has <strong>1000ml</strong> capacity and <strong>200g</strong> supplement capacities, a pill section with a <strong>sliding door</strong>, a <strong>hinged cap</strong> and is available in lots of colours. View more info <strong>here</strong>.</p>
    </div>


            <div class="col-xs-6 col-sm-6 col-md-6 orange_bg">

    <img src="images/products/liquiddynamics-pro.gif" class="img-responsive" alt="">
      </div>

            <div class="clearfix"></div>

          <a href="liquiddynamics-pro.cfm" class="btn btn-success btn-block btn-lg">Buy now</a>

      </div>
      
      <div class="clearfix"></div>
      <div class="space_20px"></div>


      </div>


      </div>

    <div class="container">

<div class="space_30px"></div>

      
      <cfinclude template="includes/features_breakdown.cfm">
    </div>
      
    <div class="container">

 
</div>

<div class="clearfix"></div>
    <div class="space_30px"></div>

<div id="two_tone_blue_orange">
<div class="container">
    <div class="dark_bg col-xs-6 col-sm-6 col-md-6 txt_center">
      <h2>Trade</h2>
      <p>For all trade enquires including bespoke branded liquiddynamics bottles in your dedicated colours, please click the link below.</p> 
      <a href="trade.cfm" class="btn btn-success btn-lg text-center btn-block">Trade Enquires</a>
    </div>
    <div class="orange_bg col-xs-6 col-sm-6 col-md-6 txt_center">
      <h2>Affiliate</h2>
      <p>liquiddynamics offers a specialised affiliate program enabling our partners to profit share to find out more please click here.</p> 
      <a href="affiliates.cfm" class="btn btn-success btn-lg text-center btn-block">Affiliate Enquires</a>
    </div>
</div>
</div>

<div class="clearfix"></div>

<cfinclude template="includes/footer.cfm">
<cfinclude template="includes/java_include.cfm">

</body>
</html>