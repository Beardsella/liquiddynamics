<!DOCTYPE html>
<html lang="en">
<head>

  <cfinclude template="includes/default_header.cfm">

  <title>Edit Quote - Liquid Dynamics Group Ltd</title>

  <!-- CSS for autocomplete -->
  <link rel="stylesheet" href="css/jquery-ui-1.12.1.css">

  <cfquery name="this_quote" datasource="agbcodes_ldi">
    SELECT *
    FROM quotes
    WHERE id = "#URL.id#"
  </cfquery>

  <cfquery name="site_defaults" datasource="agbcodes_ldi">
    SELECT *
    FROM site_defaults
  </cfquery>

  <cfquery name="linked_enq" datasource="agbcodes_ldi">
    SELECT *
    FROM enquirys_pump_systems
    WHERE id = "#this_quote.linked_enq_id#"
  </cfquery>

  <cfquery name="this_job" datasource="agbcodes_ldi">
    SELECT *
    FROM jobs
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quote_params" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_params
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfquery name="all_quotes_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_items
  </cfquery>

  <cfquery name="this_quotes_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_items
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quote_extra_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_extra_items
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfquery name="linked_pdf_quotes" datasource="agbcodes_ldi">
    SELECT *
    FROM pdfs_db
    WHERE db_id = "#URL.id#" AND doc_subject = "Quote"
    ORDER BY id DESC
  </cfquery>

  <cfquery name="linked_pdf_wo" datasource="agbcodes_ldi">
    SELECT *
    FROM pdfs_db
    WHERE db_id = "#URL.id#" AND doc_subject = "Works Order"
    ORDER BY id DESC
  </cfquery>

  <cfquery name="linked_pdf_invoices" datasource="agbcodes_ldi">
    SELECT *
    FROM pdfs_db
    WHERE db_id = "#URL.id#" AND doc_subject = "Invoice"
    ORDER BY id DESC
  </cfquery>

  <cfquery name="all_parts_data" datasource="agbcodes_ldi" cachedWithin="#createTimeSpan( 0, 1, 0, 0 )#">
    SELECT preventer_type, volume, membrane, PRESS, connection, soild_material, OTHER
    FROM alpha4
  </cfquery>


  <cfset quote_date_formated = "#DateFormat(this_quote.quote_dated, 'dd/mm/yyyy')#">
  <cfset dispatch_date_formated = "#DateFormat(this_quote.dispatch_estimate, 'dd/mm/yyyy')#">

  <style>
    .row{
      margin-top: 5px;
    }
  </style>

</head>

<body>

  <cfinclude template="includes/nav.cfm">

  <!-- Page Content -->
  <div class="container">

      <cfoutput>

        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.cfm">Home</a></li>
            <li class="breadcrumb-item"><a href="quote_overview.cfm">Quotes Overview</a></li>
            <li class="breadcrumb-item active" aria-current="page">Manage Quote "#this_quote.formatted_id#"</li>
          </ol>
        </nav>   


      <div class="row">
          <div class="col-12">
            <h1 class="page-header text-center" id="content">Edit Quote - "#this_quote.formatted_id#"</h1>
          </div>
      </div>


  <hr>

<div class="row">
  <div class="col-6">

    <h5>Quote PDF's </h5>

    <cfif "#linked_pdf_quotes.recordcount#" GT "0">

      <cfloop query="linked_pdf_quotes">
        <cfset absolute_path_and_file = "#location##linked_pdf_quotes.file_name#">
        <a href="#linked_pdf_quotes.location##linked_pdf_quotes.file_name#" target="_blank">#linked_pdf_quotes.file_name#</a><br>
      </cfloop>

      <br>

    </cfif>

    <cfif "#linked_pdf_quotes.recordcount#" eq "0">
      <a href="quote_pdf_preview.cfm?id=#this_quote.id#&type=q" class="btn btn-outline-success btn-block">Preview Quote PDF</a>
    <cfelse>
      <a href="quote_pdf_preview.cfm?id=#this_quote.id#&type=q" class="btn btn-outline-danger btn-block">Requote</a>  
    </cfif>

  </div>

  <div class="col-6">

    <h5>Works Order PDF's </h5>

    <cfloop query="linked_pdf_wo">
      <cfset absolute_path_and_file = "#location##linked_pdf_wo.file_name#">
      <a href="contracts/#this_job.formatted_id#/#linked_pdf_wo.file_name#" target="_blank">#linked_pdf_wo.file_name#</a><br>
    </cfloop>

  </div>

</div>

      </cfoutput>

  <!-- Displaying quote contact details -->
  <cfoutput query="this_quote">

  <form class="form-horizontal" action="processing_files/quote_update.cfm" method="post">
    <table class="table mt-5">
      <tbody>
        <tr>

          <th>Customer</th> 
          <td><a href="company_detail.cfm?id=#this_quote.company_id#">#URLDecode(this_quote.company_name)#</a></td>
          <th>Date</th> 
          <td><div class="form-group">
        <input type="text" id="quote_dated" name="quote_dated" class="form-control" placeholder="What date will show up on the quote" value="#quote_date_formated#">
      </div></td>
        </tr>

        <tr>
          <th>Attention of</th> 
          <td><a href="contact_detail.cfm?id=#this_quote.contact_id#">#URLDecode(this_quote.first_name)#, #URLDecode(this_quote.last_name)#</a></td>
          <th>Customer Ref</th>
          <td><div class="form-group">
        <div class="">
        <input id="cust_enquiry_ref" name="cust_enquiry_ref" type="text" placeholder="Thing we're quoting for..." class="form-control input-md disable" required="" value="#URLDecode(cust_enquiry_ref)#">
          
        </div>
      </div></td>
        </tr>

        <tr>
          <th>From</th> 
          <td>#URLDecode(this_quote.seller)#</td>
          <th>Quote Number</th> 
          <td>#formatted_id#</td>
        </tr>

      </tbody>
    </table>

   <hr>       

<fieldset>

    <input type="hidden" name="quote_id" id="quote_id" value="#this_quote.id#">

    <cfparam name="this_quote.quote_intro" DEFAULT="Thank you for you recent enquiry.">

    <div class="row">

      <!-- Textarea -->
      <div class="col-md-12 form-group">
        <label class="control-label" for="quote_intro">Quote Intro</label>
        <div class="">                     
          <textarea class="form-control" id="quote_intro" name="quote_intro">#URLDecode(quote_intro)#</textarea>
        </div>
      </div>

    </div>

    <cfif linked_enq.id NEQ "">
      <cfinclude template="includes/enquiry_linked_2_quote.cfm">
    </cfif>

    <cfinclude template="quote_item_form.cfm">

    <cfinclude template="quote_maintenance_item_form.cfm">


    <h3 class="mt-5">Final Quote Details</h3>
    <hr> 

    <div class="row">

      <!-- Textarea -->
      <div class="col-md-6 form-group">
        <label class="control-label" for="freebies">Price Includes</label>
        <div class="">                     
          <textarea class="form-control" id="freebies" name="freebies">#URLDecode(freebies)#</textarea>
        </div>
      </div>

      <!-- Textarea -->
      <div class="col-md-6 form-group">
        <label class="control-label" for="not_included">Not Included</label>
        <div class="">                     
          <textarea class="form-control" id="not_included" name="not_included">#URLDecode(not_included)#</textarea>
        </div>
      </div>

    </div>

    <div class="row">
      <!-- Textarea -->
      <div class="col-md-6 form-group">
        <label class="control-label" for="sale_condtions">Conditions of sale</label>
        <div class="">                     
          <textarea class="form-control" id="sale_condtions" name="sale_condtions">#URLDecode(sale_condtions)#</textarea>
        </div>
      </div>

      <!-- Textarea -->
      <div class="col-md-6 form-group">
        <label class="control-label" for="terms_of_payment">Terms of Payment</label>
        <div class="">                     
          <textarea class="form-control" id="terms_of_payment" name="terms_of_payment">#URLDecode(terms_of_payment)#</textarea>
        </div>
      </div>
    </div>

    <cfset exp_date_formated = "#DateFormat(quote_vaild_for, 'dd/mm/yyyy')#">

    <div class="row">

      <div class="col-12 form-group">
        <label class="control-label" for="quote_expiration_date">Quote valid until</label>  
        <input type="text" id="quote_expiration_date" name="quote_expiration_date" class="form-control" placeholder="An estimate for the customer" value="#exp_date_formated#">
      </div>

    </div>



    <div class="row">
      
      <!-- Textarea -->
      <div class="col-md-12 form-group">
        <label class="control-label" for="notes">Notes</label>
        <div class="">                     
          <textarea class="form-control" id="notes" name="notes" placeholder="Unit design...">#URLDecode(notes)#</textarea>
        </div>
      </div>  

    </div>

    <div class="row">

      <!-- Textarea -->
      <div class="col-md-12 form-group">
        <label class="control-label" for="quote_summary">Quote Summary</label>
        <div class="">                     
          <textarea class="form-control" id="quote_summary" name="quote_summary">#URLDecode(summary)#</textarea>
        </div>
      </div>

    </div>

    <input type="hidden" name="number_of_items" id="number_of_items" value="#quote_items#">
    <input type="hidden" name="number_of_maintenance_items" id="number_of_maintenance_items" value="#maintenance_items#">

    <div class="row">
      <button type="submit" class="btn btn-success btn-block">Update Quote</button>
    </div>

  </cfoutput>

</fieldset>
</form>

</div>
<!-- End of container -->

<hr>

<!-- MODALS -->

<!-- New Common Job Parameter Modal -->
<div class="modal fade" id="new_common_param_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add a New job Parameter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="processing_files/quote_add_common_param.cfm" method="post">

        <cfoutput>
          <input type="hidden" name="quote_id" id="quote_id" value="#URL.id#">
        </cfoutput>

        <div class="modal-body">

            <div class="row">

              <div class="col-md-12 form-group">
                <label class="control-label" for="new_common_parameter_name">Parameter Name</label>  
                <input id="new_common_parameter_name" name="new_common_parameter_name" type="text" placeholder="Working Pressure" class="form-control input-md">
              </div>

            </div>

            <div class="row">

              <div class="col-md-12 form-group">
                <label class="control-label" for="new_common_parameter_value">Parameter Value</label>  
                <input id="new_common_parameter_value" name="new_common_parameter_value" type="text" placeholder="100 bar" class="form-control input-md">
              </div>

            </div>

            <div class="row">

              <div class="col-md-12 form-group">
                <label class="control-label" for="new_common_parameter_notes">Parameter Notes</label>
                  <textarea class="form-control" id="new_common_parameter_notes" name="new_common_parameter_notes"></textarea>
              </div>

            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-outline-success">Add Parameter</button>
        </div>

      </form>

    </div>
  </div>
</div>
<!-- END OF New Common Job Parameter Modal -->

<!-- New Item Modal -->
<div class="modal fade" id="new_item_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add a New Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="processing_files/quote_add_item.cfm" method="post">
        
        <cfoutput>
          <input type="hidden" name="quote_id" id="quote_id" value="#URL.id#">
        </cfoutput>

        <div class="modal-body">

            <div class="row">

              <div class="col-md-12 form-group">
                <label class="control-label" for="new_item_name">Item Name</label>  
                <input id="new_item_name" name="new_item_name" type="text" placeholder="Jumboflex Somthing somthing..." class="form-control input-md">
              </div>

            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-outline-success">Add Item</button>
        </div>

      </form>

    </div>
  </div>
</div>
<!-- END OF New Item Modal -->

<!-- New Maintenance Item Modal -->
<div class="modal fade" id="new_item_maintenance_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add a New Maintenance Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="processing_files/quote_add_maintenance_item.cfm" method="post">

        <cfoutput>
          <input type="hidden" name="quote_id" id="quote_id" value="#URL.id#">
        </cfoutput>

        <div class="modal-body">

          <div class="row">

            <!-- Select Basic -->
            <div class="form-group col-md-6">
              <label class="control-label" for="new_maintenance_item">Maintenance Item</label>
              <div class="">
                <input id="new_maintenance_item" name="new_maintenance_item" type="text" placeholder="Gear Replacement" class="form-control input-md">
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group col-md-6">
              <label class="control-label" for="new_maintenance_price_each">Price Each</label>  
              <div class="">
              <input id="new_maintenance_price_each" name="new_maintenance_price_each" type="text" placeholder="100.00" class="form-control input-md">
                
              </div>
            </div>

          </div>

          <div class="row">
            <!-- Text input-->
            <div class="col-md-6 form-group">
              <label class="control-label" for="new_maintenance_currency">Currency</label>  
              <div class="">
              <input id="new_maintenance_currency" name="new_maintenance_currency" type="text" placeholder="GBP" class="form-control input-md">
                
              </div>
            </div>

            <!-- Text input-->
            <div class="col-md-6 form-group">
              <label class="control-label" for="new_maintenance_conversion_rate">Conversion rate to GBP</label>  
              <div class="">
              <input id="new_maintenance_conversion_rate" name="new_maintenance_conversion_rate" type="text" placeholder="0.657" class="form-control input-md">
              </div>
            </div>
          </div>

          <div class="row">
            <!-- Textarea -->
            <div class="col-md-12 form-group">
              <label class="control-label" for="new_maintenance_item_notes">Item Notes</label>
              <div class="">                     
                <textarea class="form-control" id="new_maintenance_item_notes" name="new_maintenance_item_notes"></textarea>
              </div>
            </div>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-outline-success">Add a Maintenance Item</button>
        </div>

      </form>

    </div>
  </div>
</div>
<!-- END OF New Maintenance Item Modal -->

<!-- New Maintenance Item Param Modals -->
<cfoutput query="this_quotes_items">

  <div class="modal fade" id="new_item_#CurrentRow#_param_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add a new parameter to item ###CurrentRow# </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <form action="processing_files/quote_add_item_param.cfm" method="post">

          <input type="hidden" name="quote_id" id="quote_id" value="#URL.id#">
          <input type="hidden" name="item_id" id="item_id" value="#id#">

          <div class="modal-body">

              <div class="row">

                <div class="col-md-12 form-group">
                  <label class="control-label" for="new_item_parameter_name">Parameter Name</label>  
                  <input id="new_item_parameter_name" name="new_item_parameter_name" type="text" placeholder="Working Pressure" class="form-control input-md">
                </div>

            </div>

            <div class="row">

                <div class="col-md-12 form-group">
                  <label class="control-label" for="new_item_parameter_value">Parameter Value</label>  
                  <input id="new_item_parameter_value" name="new_item_parameter_value" type="text" placeholder="100 bar" class="form-control input-md">
                </div>

              </div>

              <div class="row">

                <div class="col-md-12 form-group">
                  <label class="control-label" for="new_item_parameter_notes">Parameter Notes</label>
                    <textarea class="form-control" id="new_item_parameter_notes" name="new_item_parameter_notes"></textarea>
                </div>

              </div>

          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-outline-success">Add parameter to item ###CurrentRow#</button>
          </div>

        </form>

      </div>
    </div>
  </div>

</cfoutput>
<!-- END OF New Maintenance Item Param Modals -->

<!-- END OF MODALS -->

<cfinclude template="includes/footer.cfm">
<cfinclude template="includes/java_include.cfm">

<!-- JQuery for autocomplete -->
<script src="js/jquery-ui-1.12.1.min.js"></script>
<script>
  $(function() {
    $( "#quote_dated, #dispatch_estimate, #quote_expiration_date" ).datepicker({
        dateFormat : 'dd/mm/yy',
        changeMonth : true,
        changeYear : true,
    });
  });

  $(function () {
    $('[data-toggle="popover"]').popover()
  })
</script>
<!-- End of autocomplete JQuery -->

</body>
</html>