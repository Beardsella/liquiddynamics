<cfquery name="w_orders" datasource="agbcodes_ldi">
    SELECT *
    FROM works_orders
</cfquery>

<table class="table table-striped table-dark" id="ss_table">

  <thead>
      <th>ID#</th>
      <th>Current Staus</th>
      <th>Quote Created</th>
      <th>Contact</th>
      <th>Refrence</th>
  </thead>

  <tbody>

    <cfoutput query="w_orders">

      <cfif isDefined("w_orders.seller") AND w_orders.seller neq "">
        <cfset selling_company = "#URLDecode(seller)#">

        <cfif selling_company eq "Liquid Dynamics International">
          <cfset company_prefix = "LD-UK-WO">
        <cfelseif selling_company eq "Pulse Gard">
          <cfset company_prefix = "PG-UK-WO">
        <cfelseif selling_company eq "Shock Gard">
          <cfset company_prefix = "SG-UK-WO">
        <cfelseif selling_company eq "Hydrotrole">
          <cfset company_prefix = "HT-UK-WO">
        </cfif>

      </cfif>


        <cfif "#URLDecode(wo_status)#" eq "wip">
          <cfset current_badge = "warning">
          <cfset current_status = "Work In Progress">
        <cfelseif "#URLDecode(wo_status)#" eq "invoiced">
          <cfset current_badge = "success">
          <cfset current_status = "Converted to Works Order">
        </cfif>

      <tr>
          <td><a href="quote_management.cfm?id=#id#" class="agb-table-link">#company_prefix#-#id#</a></td>
          <td><a href="" class="badge badge-#current_badge#">#current_status#</a></td>
          <td>#DateFormat("#quote_created#", "dd / mmm / yyyy")#</td>
          <td>#URLDecode(first_name)# #URLDecode(last_name)#</td>
          <td><a href="quote_management.cfm?id=#id#" class="agb-table-link">#URLDecode(cust_enquiry_ref)#</a></td>

          <cfif "#URLDecode(wo_status)#" neq "quoted">
            <td><a href="processing_files/quote_status_update.cfm?id=#id#&job=#all_jobs.id#&status=quoted" class="btn btn-outline-success btn-sm">Convert to Invoice</a></td>

          <cfelse>

            <td>
              <a href="invoice.cfm?id=#linked_invoice_id#" class="badge badge-success">Invoice - "#linked_invoice_id#"</a>
            </td>

          </cfif>

        </tr>

      </cfoutput>

  </tbody>                

</table>