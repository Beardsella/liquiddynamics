<table class="table table-striped table-dark" id="ss_table">

  <thead>
      <th>ID#</th>
      <th>Quote Created</th>
      <th>Contact</th>
      <th>Refrence</th>
  </thead>

  <tbody>

    <cfoutput query="invoices">

      <cfif isDefined("invoices.seller") AND invoices.seller neq "">
        <cfset selling_company = "#URLDecode(seller)#">

        <cfif selling_company eq "Liquid Dynamics International">
          <cfset company_prefix = "LD-UK-INV">
        <cfelseif selling_company eq "Pulse Gard">
          <cfset company_prefix = "PG-UK-INV">
        <cfelseif selling_company eq "Shock Gard">
          <cfset company_prefix = "SG-UK-INV">
        <cfelseif selling_company eq "Hydrotrole">
          <cfset company_prefix = "HT-UK-INV">
        </cfif>

      </cfif>

      <tr>
          <td><a href="quote_management.cfm?id=#id#" class="agb-table-link">#company_prefix#-#id#</a></td>
          <td>#DateFormat("#quote_created#", "dd / mmm / yyyy")#</td>
          <td>#URLDecode(first_name)# #URLDecode(last_name)#</td>
          <td><a href="quote_management.cfm?id=#id#" class="agb-table-link">#URLDecode(cust_enquiry_ref)#</a></td>
        </tr>

      </cfoutput>

  </tbody>                

</table>