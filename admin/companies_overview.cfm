<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <title>Companies Overview - Liquid Dynamics Group Ltd</title>

    <cfquery name="companies" datasource="agbcodes_ldi">
        SELECT *
        FROM companies
        ORDER BY company_name
    </cfquery>

    <cfquery name="countries" datasource="agbcodes_ldi">
        SELECT *
        FROM countries
    </cfquery>

</head>
<body>

    <cfinclude template="includes/nav.cfm">

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            
            <cfoutput>
                <div class="col-lg-12">
                  <h1 class="page-header">Companies Overview  <span class="badge badge-info">#companies.recordcount#</span></h1>
                                <!-- Button trigger modal -->
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="##AddCompany">
                    Add Company
                  </button>
                  <hr>
                </div>
            </cfoutput>

        </div>
        <div class="row">

            <!-- Modal -->
            <div class="modal fade" id="AddCompany" tabindex="-1" role="dialog" aria-labelledby="AddCompanyLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="AddCompanyLabel">Add a new company</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">

                <cfoutput>
                  <form class="form-horizontal" action="processing_files/company_insert.cfm" method="post" enctype="multipart/form-data">

                  <div class="col-lg-12">

                      <!-- System Operating Temperature -->
                      <div class="form-group">
                        <label for="company_name">Company Name</label>
                        <input type="text" class="form-control" id="company_name" name="company_name">
                      </div>

                      <!-- System Operating Temperature -->
                      <div class="form-group">
                        <label for="telephone">Telephone</label>
                        <input type="text" class="form-control" id="telephone" name="telephone">
                      </div>

                      <!-- System Design Temperature -->
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email">
                      </div>

                      <!-- System Operating Temperature -->
                      <div class="form-group">
                        <label for="mail_1">Address</label>
                        <input type="text" class="form-control" id="mail_1" name="mail_1">
                        <input type="text" class="form-control" id="mail_2" name="mail_2">
                        <input type="text" class="form-control" id="mail_3" name="mail_3">
                        <input type="text" class="form-control" id="county" name="county" placeholder="County">
                        <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Post Code">
                      </div>
                      <!-- System Operating Temperature -->
                      <div class="form-group">
                        <label for="fax_number">Fax</label>
                        <input type="text" class="form-control" id="fax_number" name="fax_number">
                      </div>
                      
                      <!-- Connection Type Required (Flange, NPT, Tri Clamp etc) -->
                      <div class="form-group">
                         <label for="country">Country</label>
                          <select id="country" name="country" class="form-control">
                              <cfloop query="countries">
                                    <option value="#country#">#country#</option>
                              </cfloop>
                          </select>
                      </div>


                    </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Add Company</button>
                  </div>

                  </form>

              </cfoutput>

                </div>
              </div>
            </div>
            <!-- End of Row -->
            </div>

            <table class="table table-hover table-striped">

              <tr>
                <th>Name</th>
                <th>Telephone</th>
                <th>Email</th>
                <th>Country</th>
              </tr>

              <cfoutput query="companies">
              
                <tr>
                  <td><a href="company_detail.cfm?id=#companies.id#">#URLDecode(companies.company_name)#</a></td>
                  <td><a href="tel:#URLDecode(companies.telephone)#">#URLDecode(companies.telephone)#</a></td>
                  <td><a href="mail:#URLDecode(companies.email)#">#URLDecode(companies.email)#</a></td>
                  <td>#URLDecode(companies.country)#</td>
                </tr>

              </cfoutput>
              
            </table>

        </div>                

    </div>

    <hr><!-- End of page main content -->

    <cfinclude template="includes/footer.cfm">
    <cfinclude template="includes/java_include.cfm">

</body>
</html>