<!DOCTYPE html>
<html lang="en">
<head>

  <cfinclude template="includes/default_header.cfm">

  <title>Liquid Dynamics</title>

  <!-- Insert Querys ect here -->

</head>
<body>

  <cfinclude template="includes/nav.cfm">

  <!-- Page Content -->
  <div class="container">

    <div class="row">
                
      <cfoutput>
        <div class="col-lg-12">

          <h1 class="page-header">Thank You</h1>

          <p class="lead">We'll deal with this enquiry and get back to you ASAP.</p>

        </div>
      </cfoutput>

    </div>

  </div>
  
  <hr>
  <!-- End of page main content -->

  <cfinclude template="includes/footer.cfm">
  <cfinclude template="includes/java_include.cfm">

</body>
</html>