<div class="row mt-5">

  <div class="col-6">
    <cfoutput>
      <h3>Maintenance Items <span class="badge badge-primary">#this_quote_extra_items.recordcount#</span></h3>
    </cfoutput>
  </div> 

  <div class="col-6">
    
    <!-- New Common Job Parameter Modal TRIGGER -->
    <button class="btn btn-outline-success float-right mr-2" type="button" data-toggle="modal" data-target="#new_item_maintenance_modal">Add a Maintenance item</button>

    <cfif '#this_quote_extra_items.recordcount#' GTE '1'>
      <!-- Expand existing Common Job Parameters TRIGGER -->
      <button class="btn btn-outline-info float-right mr-2" type="button" data-toggle="collapse" data-target="#maintenance_items_div" aria-expanded="false" aria-controls="maintenance_items_div">Expand Maintenance Items</button>
    </cfif>
    
  </div>

</div>

<hr>

<cfparam name="this_quote_extra_items.item_refrence" default="">
<cfparam name="this_quote_extra_items.item_notes" default="">
<cfparam name="this_quote_extra_items.item_price" default="">
<cfparam name="this_quote_extra_items.currency" default="">
<cfparam name="this_quote_extra_items.conversion" default="">

<div class="collapse" id="maintenance_items_div">

  <cfset maintenance_items = '0'>

  <cfoutput query="this_quote_extra_items">

    <input type="hidden" name="maintenance_item_#CurrentRow#_id" id="maintenance_item_#CurrentRow#_id" value="#this_quote_extra_items.id#">

    <cfset maintenance_items = '#maintenance_items#' + '1'>

    <div class="row">
      <div class="col-9">
        <h4><span class="badge badge-warning">Maintenance Item ###CurrentRow#</span></h4>
      </div>
      <div class="col-3 text-right">
        <h6><a href="processing_files/quote_delete_maintenance_item.cfm?id=#id#" class="text-danger">Delete Maintenance Item ###CurrentRow#</a></h6>
      </div>
    </div>
    

    <div class="row">

    <!-- Select Basic -->
    <div class="form-group col-md-6">
      <label class="control-label" for="maintenance_item">Maintenance Item</label>
      <div class="">
        <input id="maintenance_item_#CurrentRow#" name="maintenance_item_#CurrentRow#" type="text" placeholder="Gear Replacement" class="form-control input-md" value="#URLDecode(this_quote_extra_items.item_refrence)#">
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group col-md-6">
      <label class="control-label" for="maintenance_price_each">Price Each</label>  
      <div class="">
      <input id="maintenance_price_each_#CurrentRow#" name="maintenance_price_each_#CurrentRow#" type="text" placeholder="100.00" class="form-control input-md" value="#DecimalFormat(this_quote_extra_items.item_price)#">
        
      </div>
    </div>

    </div>

    <div class="row">
      <!-- Text input-->
      <div class="col-md-6 form-group">
        <label class="control-label" for="maintenance_currency">Currency</label>  
        <div class="">
        <input id="maintenance_currency_#CurrentRow#" name="maintenance_currency_#CurrentRow#" type="text" placeholder="GBP" class="form-control input-md" value="#URLDecode(this_quote_extra_items.currency)#">
          
        </div>
      </div>

      <!-- Text input-->
      <div class="col-md-6 form-group">
        <label class="control-label" for="maintenance_conversion_rate">Conversion rate to GBP</label>  
        <div class="">
        <input id="maintenance_conversion_rate_#CurrentRow#" name="maintenance_conversion_rate_#CurrentRow#" type="text" placeholder="0.657" class="form-control input-md" value="#DecimalFormat(this_quote_extra_items.conversion)#">
        </div>
      </div>
    </div>

    <div class="row">
      <!-- Textarea -->
      <div class="col-md-12 form-group">
        <label class="control-label" for="maintenance_item_notes_#CurrentRow#">Item Notes</label>
        <div class="">                     
          <textarea class="form-control" id="maintenance_item_notes_#CurrentRow#" name="maintenance_item_notes_#CurrentRow#">#URLDecode(this_quote_extra_items.item_notes)#</textarea>
        </div>
      </div>
    </div>

    <hr>

  </cfoutput>

</div>