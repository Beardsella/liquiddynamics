<table class="table table-striped table-dark" id="ss_table">


   <thead>
                  <th>#</th>
                  <th>Current Status</th>
                  <th>Client</th>
                  <th>Contact</th>
                  <th>Date Created</th>
                  <th>Quote Refrence Name</th>
                  <th>Convert to WO</th>
              </thead>

              <tbody>

                  <cfoutput query="quotes">

              <cfif isDefined("quotes.seller") AND quotes.seller neq "">

                <cfset selling_company = "#URLDecode(quotes.seller)#">

                <cfif selling_company eq "Liquid Dynamics International">
                  <cfset company_prefix = "LD-UK-Q-">
                <cfelseif selling_company eq "Pulse Gard">
                  <cfset company_prefix = "PG-UK-Q-">
                <cfelseif selling_company eq "Shock Gard">
                  <cfset company_prefix = "SG-UK-Q-">
                <cfelseif selling_company eq "Hydrotrole">
                  <cfset company_prefix = "HT-UK-Q-">
                </cfif>

              </cfif> 


              <cfif "#this_job.job_status#" eq "Quoting">
                <cfset current_badge = "warning">
                <cfset current_status = "Work In Progress">
              <cfelseif "#this_job.job_status#" eq "Quoted">
                <cfset current_badge = "primary">
                <cfset current_status = "Waiting on customer">
              <cfelseif "#this_job.job_status#" eq "Working">
                <cfset current_badge = "warning">
                <cfset current_status = "Converted to Works Order">
              <cfelseif "#this_job.job_status#" eq "Invoiced">
                <cfset current_badge = "success">
                <cfset current_status = "Marked as inactive">
              </cfif>


                <cfquery name="quotes" datasource="agbcodes_ldi">
                    SELECT quotes.*
                    FROM quotes
                    RIGHT JOIN job_vs_quote 
                    ON quotes.id = job_vs_quote.quote_id
                    WHERE job_vs_quote.job_id = "#id#"
                </cfquery>

                <cfquery name="linked_job" datasource="agbcodes_ldi">
                    SELECT *
                    FROM jobs
                    WHERE quote_id = "#id#"                                   
                </cfquery>

                <cfquery name="linked_wo" datasource="agbcodes_ldi">
                    SELECT works_orders.*
                    FROM job_vs_works_order
                    RIGHT JOIN works_orders
                    ON works_orders.id = job_vs_works_order.job_id
                    WHERE job_vs_works_order.job_id = "#URL.id#"                                   
                </cfquery>

                      <tr>
                        <td><a href="quote_management.cfm?id=#id#" class="btn btn-block btn-outline-info" title="Click to view quote">#formatted_id#</a></td>

                        <td><span class="badge badge-#current_badge#">#URLDecode(q_status)#</span></td>

                        <td><a href="company_detail.cfm?id=#company_id#" class="agb-table-link" title="Click to view company details">#URLDecode(company_name)#</a></td>
                        <td><a href="contact_detail.cfm?id=#contact_id#" class="agb-table-link">#URLDecode(first_name)# #URLDecode(last_name)#</a></td>
                        <td>#DateFormat("#quote_created#", "dd / mmm / yyyy")#</td>
                        <td><a href="quote_management.cfm?id=#id#" class="agb-table-link" title="Click to view quote">#URLDecode(cust_enquiry_ref)#</a></td>
                        <td>
                          <cfif linked_wo.recordcount GTE 1>
                            <a href="works_order.cfm?id=#linked_wo.id#" class="btn btn-block btn-outline-info" title="Click to cview linked works order">#linked_wo.formatted_id#</a>
                          <cfelse>
                            <a href="" class="btn btn-block btn-outline-warning" title="Click to convert quote to a works order">Convert to WO</a>
                          </cfif>
                        </td>

                        </tr>

                  </cfoutput>
              </tbody>          

</table>