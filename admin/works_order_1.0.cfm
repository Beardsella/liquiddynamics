<!DOCTYPE html>
<html lang="en">
<head>

  <cfinclude template="includes/default_header.cfm">

  <title>Works Order - Liquid Dynamics Group Ltd</title>

  <!-- CSS for autocomplete -->
  <link rel="stylesheet" href="css/jquery-ui-1.12.1.css">

  <cfquery name="this_quote" datasource="agbcodes_ldi">
    SELECT *
    FROM quotes
    WHERE id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quote_params" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_params
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quotes_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_items
    WHERE quote_id = "#URL.id#"
  </cfquery>
  
  <cfquery name="this_quote_extra_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_extra_items
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfset quote_date_formated = "#DateFormat(this_quote.quote_dated, 'dd/mm/yyyy')#">
  <cfset dispatch_date_formated = "#DateFormat(this_quote.dispatch_estimate, 'dd/mm/yyyy')#">

</head>

<body>

  <cfinclude template="includes/nav.cfm">

  <!-- Page Content -->
  <div class="container">

  <div class="col-12">
    <cfoutput>
      <h1 class="page-header" id="content">View Works Order ###URL.id#</h1>
      <p class="lead">This is uneditable as the job should of already been checked.</p>
    </cfoutput>
  </div>

  <!-- Displaying quote contact details -->
  <cfoutput>
    <table class="table">
      <tbody>
        <tr>
          <th>Quote ID##</th> 
          <td>#this_quote.id#</td>
          <th>Company Name</th> 
          <td><a href="company_detail.cfm?id=#this_quote.company_id#">#URLDecode(this_quote.company_name)#</a></td>
          <th>Contact Name</th> 
          <td><a href="contact_detail.cfm?id=#this_quote.contact_id#">#URLDecode(this_quote.first_name)#, #URLDecode(this_quote.last_name)#</a></td>
        </tr>
      </tbody>
    </table>
  </cfoutput>

   <hr>       

<form class="form-horizontal" action="processing_files/quote_update.cfm" method="post">
<fieldset>

  <cfoutput query="this_quote">

    <input type="hidden" name="quote_id" id="quote_id" value="#this_quote.id#">

    <div class="row">

      <!-- Text input-->
      <div class="col-md-12 form-group">
        <label class="control-label" for="customer_reference">Customer Enquiry Reference</label>  
        <div class="">
        <input id="cust_enquiry_ref" name="cust_enquiry_ref" type="text" placeholder="Thing we're quoting for..." class="form-control input-md" required="" value="#URLDecode(cust_enquiry_ref)#">
          
        </div>
      </div>

    </div>

    <div class="row">

      <!-- Textarea -->
      <div class="col-md-12 form-group">
        <label class="control-label" for="quote_intro">Quote Intro</label>
        <div class="">                     
          <textarea class="form-control" id="quote_intro" name="quote_intro">#URLDecode(quote_intro)#</textarea>
        </div>
      </div>

    </div>

    <div class="row">

      <div class="col-md-12 form-group">
        <label class="control-label" for="quote_dated">Quote Date</label>  
        <input type="text" id="quote_dated" name="quote_dated" class="form-control" placeholder="What date will show up on the quote" value="#quote_date_formated#">
      </div>

    </div>

    <div class="row">
      
      <!-- Textarea -->
      <div class="col-md-12 form-group">
        <label class="control-label" for="notes">Notes</label>
        <div class="">                     
          <textarea class="form-control" id="notes" name="notes" placeholder="Unit design...">#URLDecode(notes)#</textarea>
        </div>
      </div>  

    </div>


    <cfinclude template="common_parameter_form.cfm">


    <cfinclude template="quote_item_form.cfm">


    <cfinclude template="maintenance_item_form.cfm">


    <h3 class="mt-5">Final Quote Details</h3>
    <hr> 

    <div class="row">

      <!-- Textarea -->
      <div class="col-md-6 form-group">
        <label class="control-label" for="freebies">Price Includes</label>
        <div class="">                     
          <textarea class="form-control" id="freebies" name="freebies">#URLDecode(freebies)#</textarea>
        </div>
      </div>

      <!-- Textarea -->
      <div class="col-md-6 form-group">
        <label class="control-label" for="not_included">Not Included</label>
        <div class="">                     
          <textarea class="form-control" id="not_included" name="not_included">#URLDecode(not_included)#</textarea>
        </div>
      </div>

    </div>

    <div class="row">
      <!-- Textarea -->
      <div class="col-md-6 form-group">
        <label class="control-label" for="sale_condtions">Conditions of sale</label>
        <div class="">                     
          <textarea class="form-control" id="sale_condtions" name="sale_condtions">#URLDecode(sale_condtions)#</textarea>
        </div>
      </div>

      <!-- Textarea -->
      <div class="col-md-6 form-group">
        <label class="control-label" for="terms_of_payment">Terms of Payment</label>
        <div class="">                     
          <textarea class="form-control" id="terms_of_payment" name="terms_of_payment">#URLDecode(terms_of_payment)#</textarea>
        </div>
      </div>
    </div>

    <cfset exp_date_formated = "#DateFormat(quote_vaild_for, 'dd/mm/yyyy')#">

    <div class="row">

      <div class="col-md-6 form-group">
        <label class="control-label" for="quote_expiration_date">Quote valid until</label>  
        <input type="text" id="quote_expiration_date" name="quote_expiration_date" class="form-control" placeholder="An estimate for the customer" value="#exp_date_formated#">
      </div>

      <div class="col-md-6 form-group">
        <label class="control-label" for="dispatch_estimate">Dispatch Estimate</label>  
        <input type="text" id="dispatch_estimate" name="dispatch_estimate" class="form-control" placeholder="An estimate for the customer" value="#dispatch_date_formated#">
      </div>

    </div>

    <input type="hidden" name="number_of_common_params" id="number_of_common_params" value="#common_params#">
    <input type="hidden" name="number_of_items" id="number_of_items" value="#quote_items#">
    <input type="hidden" name="number_of_maintenance_items" id="number_of_maintenance_items" value="#maintenance_items#">

    <div class="row">
      <button type="submit" class="btn btn-success btn-block">Update Quote</button>
    </div>

  </cfoutput>

</fieldset>
</form>

</div>
<!-- End of container -->

<hr>

<!-- MODALS -->

<!-- New Common Job Parameter Modal -->
<div class="modal fade" id="new_common_param_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add a New job Parameter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="processing_files/quote_add_common_param.cfm" method="post">

        <cfoutput>
          <input type="hidden" name="quote_id" id="quote_id" value="#URL.id#">
        </cfoutput>

        <div class="modal-body">

            <div class="row">

              <div class="col-md-6 form-group">
                <label class="control-label" for="new_common_parameter_name">Parameter Name</label>  
                <input id="new_common_parameter_name" name="new_common_parameter_name" type="text" placeholder="Working Pressure" class="form-control input-md">
              </div>

              <div class="col-md-6 form-group">
                <label class="control-label" for="new_common_parameter_value">Parameter Value</label>  
                <input id="new_common_parameter_value" name="new_common_parameter_value" type="text" placeholder="100 bar" class="form-control input-md">
              </div>

            </div>

            <div class="row">

              <div class="col-md-12 form-group">
                <label class="control-label" for="new_common_parameter_notes">Parameter Notes</label>
                  <textarea class="form-control" id="new_common_parameter_notes" name="new_common_parameter_notes"></textarea>
              </div>

            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-outline-success">Add Parameter</button>
        </div>

      </form>

    </div>
  </div>
</div>
<!-- END OF New Common Job Parameter Modal -->

<!-- New Item Modal -->
<div class="modal fade" id="new_item_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add a New Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="processing_files/quote_add_item.cfm" method="post">
        
        <cfoutput>
          <input type="hidden" name="quote_id" id="quote_id" value="#URL.id#">
        </cfoutput>

        <div class="modal-body">

            <div class="row">

              <div class="col-md-12 form-group">
                <label class="control-label" for="new_item_name">Item Name</label>  
                <input id="new_item_name" name="new_item_name" type="text" placeholder="Jumboflex Somthing somthing..." class="form-control input-md">
              </div>

            </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-outline-success">Add Item</button>
        </div>

      </form>

    </div>
  </div>
</div>
<!-- END OF New Item Modal -->

<!-- New Maintenance Item Modal -->
<div class="modal fade" id="new_item_maintenance_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content ">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add a New Maintenance Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="processing_files/quote_add_maintenance_item.cfm" method="post">

        <cfoutput>
          <input type="hidden" name="quote_id" id="quote_id" value="#URL.id#">
        </cfoutput>

        <div class="modal-body">

          <div class="row">

            <!-- Select Basic -->
            <div class="form-group col-md-6">
              <label class="control-label" for="new_maintenance_item">Maintenance Item</label>
              <div class="">
                <input id="new_maintenance_item" name="new_maintenance_item" type="text" placeholder="Gear Replacement" class="form-control input-md">
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group col-md-6">
              <label class="control-label" for="new_maintenance_price_each">Price Each</label>  
              <div class="">
              <input id="new_maintenance_price_each" name="new_maintenance_price_each" type="text" placeholder="100.00" class="form-control input-md">
                
              </div>
            </div>

          </div>

          <div class="row">
            <!-- Text input-->
            <div class="col-md-6 form-group">
              <label class="control-label" for="new_maintenance_currency">Currency</label>  
              <div class="">
              <input id="new_maintenance_currency" name="new_maintenance_currency" type="text" placeholder="GBP" class="form-control input-md">
                
              </div>
            </div>

            <!-- Text input-->
            <div class="col-md-6 form-group">
              <label class="control-label" for="new_maintenance_conversion_rate">Conversion rate to GBP</label>  
              <div class="">
              <input id="new_maintenance_conversion_rate" name="new_maintenance_conversion_rate" type="text" placeholder="0.657" class="form-control input-md">
              </div>
            </div>
          </div>

          <div class="row">
            <!-- Textarea -->
            <div class="col-md-12 form-group">
              <label class="control-label" for="new_maintenance_item_notes">Item Notes</label>
              <div class="">                     
                <textarea class="form-control" id="new_maintenance_item_notes" name="new_maintenance_item_notes"></textarea>
              </div>
            </div>
          </div>

        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-outline-success">Add a Maintenance Item</button>
        </div>

      </form>

    </div>
  </div>
</div>
<!-- END OF New Maintenance Item Modal -->

<!-- New Maintenance Item Param Modals -->
<cfoutput query="this_quotes_items">

  <div class="modal fade" id="new_item_#CurrentRow#_param_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add a New Item ###CurrentRow# Parameter</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <form action="processing_files/quote_add_item_param.cfm" method="post">

          <input type="hidden" name="quote_id" id="quote_id" value="#URL.id#">
          <input type="hidden" name="item_id" id="item_id" value="#id#">

          <div class="modal-body">

              <div class="row">

                <div class="col-md-6 form-group">
                  <label class="control-label" for="new_item_parameter_name">Parameter Name</label>  
                  <input id="new_item_parameter_name" name="new_item_parameter_name" type="text" placeholder="Working Pressure" class="form-control input-md">
                </div>

                <div class="col-md-6 form-group">
                  <label class="control-label" for="new_item_parameter_value">Parameter Value</label>  
                  <input id="new_item_parameter_value" name="new_item_parameter_value" type="text" placeholder="100 bar" class="form-control input-md">
                </div>

              </div>

              <div class="row">

                <div class="col-md-12 form-group">
                  <label class="control-label" for="new_item_parameter_notes">Parameter Notes</label>
                    <textarea class="form-control" id="new_item_parameter_notes" name="new_item_parameter_notes"></textarea>
                </div>

              </div>

          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-outline-success">Add Item ###CurrentRow# Parameter</button>
          </div>

        </form>

      </div>
    </div>
  </div>

</cfoutput>
<!-- END OF New Maintenance Item Param Modals -->

<!-- END OF MODALS -->

<cfinclude template="includes/footer.cfm">
<cfinclude template="includes/java_include.cfm">

<!-- JQuery for autocomplete -->
<script src="js/jquery-ui-1.12.1.min.js"></script>
<script>
  $(function() {
    $( "#quote_dated, #dispatch_estimate, #quote_expiration_date" ).datepicker({
        dateFormat : 'dd/mm/yy',
        changeMonth : true,
        changeYear : true,
    });
  });

  $(function () {
    $('[data-toggle="popover"]').popover()
  })
</script>
<!-- End of autocomplete JQuery -->

</body>
</html>