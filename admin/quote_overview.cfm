<!DOCTYPE html>
<html lang="en">
<head>

	<cfinclude template="includes/default_header.cfm">

	<title>Quotes - Liquid Dynamics</title>

	<!-- CSS for autocomplete -->
	<link rel="stylesheet" href="css/jquery-ui-1.12.1.css">

    <cfquery name="quotes" datasource="agbcodes_ldi">
        SELECT *
        FROM quotes
    </cfquery>

</head>
<body>

	<cfinclude template="includes/nav.cfm">

	<div class="container-fluid">

	    <!-- Start of the main page content -->
        <h1 class="page-header">Customer quotes</h1>
        <p class="lead">These are quote forms filled out by customers that havent been marked as actioned. They can be sorted by column and clicked on for more information. There are <cfoutput><span class="badge badge-pill badge-primary" title="quotes needing attention">#quotes.recordcount#</span></cfoutput> quotes needing attention.</p> <p class="lead">This is a link to the <a href="create-quote.cfm">main quote form</a></strong></p>


		<div class="tab-content">

			<!-- New quotes -->
			<div role="tabpanel" class="tab-pane active" id="new_quotes">

				<table class="table table-striped table-dark" id="ss_table">

			        <thead>
			            <th>Quote ID #</th>
			            <th>Company</th>
			            <th>Contact</th>
			            <th>Date Created</th>
			            <th>Quote Refrence Name</th>
			            <th>Linked Enquiry</th>
			            <th>Linked Contract</th>
			            <th>PDF</th>
			        </thead>

			        <tbody>

			            <cfoutput query="quotes">

							<cfquery name="linked_job" datasource="agbcodes_ldi">
							    SELECT *
							    FROM jobs
						        WHERE quote_id = "#id#"	    
							</cfquery>

							<cfquery name="linked_enq" datasource="agbcodes_ldi">
							    SELECT *
							    FROM enquirys_pump_systems
						        WHERE id = "#quotes.linked_enq_id#"	    
							</cfquery>

			                <tr>
			                	<td><a href="quote_management.cfm?id=#id#" class="btn btn-outline-info" title="Click to view this quote #formatted_id#">#formatted_id#</a></td>
			                    <td><a href="company_detail.cfm?id=#company_id#" class="agb-table-link" title="Click to view company details">#URLDecode(company_name)#</a></td>
			                    <td><a href="contact_detail.cfm?id=#contact_id#" class="agb-table-link">#URLDecode(first_name)# #URLDecode(last_name)#</a></td>
			                    <td>#DateFormat("#quote_created#", "dd / mmm / yyyy")#</td>
			                    <td><a href="quote_management.cfm?id=#id#" class="agb-table-link" title="Click to view quote">#URLDecode(cust_enquiry_ref)#</a></td>

			                    <td class="text-center">
									<cfif isDefined("linked_enq.id") AND linked_enq.id neq "">
										<a href="enquiry_management.cfm?id=#linked_enq.id#" class="btn btn-outline-info btn-block" title="View linked Enquiry" target="_blank">#linked_enq.formatted_id#</a>								
									<cfelse>
										<span class="text-danger">No linked Enquiry</span>
									</cfif>
			                    </td>
			                    <td>
									<cfif isDefined("linked_job.job_title") AND linked_job.job_title neq "">
										<a href="contract_details.cfm?id=#linked_job.id#" class="btn btn-outline-success btn-block" title="">#URLDecode(linked_job.job_title)#</a>
									<cfelseif isDefined("linked_job.id") AND linked_job.id neq "">
										<a href="contract_details.cfm?id=#linked_job.id#" class="btn btn-outline-success btn-block" title="View linked contract file">#linked_job.formatted_id#</a>									
									<cfelse>

										<!-- Button trigger new contract modal -->
										<button type="button" class="btn btn-outline-warning btn-block" data-toggle="modal" data-target="##new_contract_modal">
										  Start a new Contract
										</button>
										
									</cfif>
			                    </td>
			                	
			                	<td>
			                		<a href="quote_pdf_preview.cfm?id=#id#&type=q" class="btn btn-outline-warning btn-block" title="Click to view quote">PDF Preview</a></td>
		                    </tr>





<!-- Modal -->
<div class="modal fade" id="new_contract_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create a new Contact</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       	<p class="lead">This action will create a new "contract" and generate a works order based off the current quote "#formatted_id#".</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="processing_files/contract_create.cfm?quote_id=#id#" class="btn btn-primary">Create new Contract</a>
      </div>
    </div>
  </div>
</div>

			            </cfoutput>
			        </tbody>                

		        </table>

		    </div>
			<!-- End of Tab -->


		</div>
		<!-- End of Tab -->
		</div>        

	<cfinclude template="includes/footer.cfm">
	<cfinclude template="includes/java_include.cfm">

	<!-- Extra table sorting for 4 extra tables -->
	<script type="text/JavaScript">
		$(document).ready(function(){ 
			$("#ss_table_1, #ss_table_2, #ss_table_3, #ss_table_4").tablesorter(); 
		}); 
	</script>

</body>
</html>