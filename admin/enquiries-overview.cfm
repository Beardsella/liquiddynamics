<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <title>Enquirys Overview - Liquid Dynamics Group Ltd</title>


    <cfquery name="enquiries" datasource="agbcodes_ldi">
        SELECT *
        FROM enquirys_pump_systems
    </cfquery>

    <cfquery name="countries" datasource="agbcodes_ldi">
        SELECT *
        FROM countries
    </cfquery>


</head>
<body>

    <cfinclude template="includes/nav.cfm">

      <!-- Page Content -->
      <main role="main" class="container-fluid">

            <cfoutput>

                <nav aria-label="breadcrumb" role="navigation">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.cfm">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><a href="jobs_overview.cfm">Enquiries Overview</a></li>
                  </ol>
                </nav>            

                <div class="col-lg-12">
                    <h1 class="page-header">Enquiries Overview</h1>
                    <p class="lead">Here you can see all current Enquirys</p>

                <table class="table table-striped table-dark" id="ss_table">

                  <thead>
                    <th>##</th>
                    <th>Company</th>
                    <th>Contact</th>
                    <th>Email</th>
                    <th>Enquiry Date</th>
                    <th></th>
                  </thead>

                  <tbody>

                    <cfloop query="enquiries">

                      <cfquery name="this_company" datasource="agbcodes_ldi">
                          SELECT *
                          FROM companies
                          WHERE id = "#company_id#"
                      </cfquery>      

                      <cfquery name="staff" datasource="agbcodes_ldi">
                          SELECT *
                          FROM contacts
                          WHERE id = "#contact_id#"
                      </cfquery>

                      <tr>
                          <td><a href="enquiry_management.cfm?id=#id#" class="agb-table-link">#formatted_id#</a></td>
                          <td><a href="company_detail.cfm?id=#this_company.id#" class="agb-table-link">#URLDecode(this_company.company_name)#</a></td>
                          <td><a href="contact_detail.cfm?id=#staff.id#" class="agb-table-link">#URLDecode(staff.first_name)# #URLDecode(staff.last_name)#</a></td>
                          <td><a href="mailto:#URLDecode(staff.email)#" class="agb-table-link">#URLDecode(staff.email)#</a></td>
                          <td>#DateFormat(enquiries.enquiry_date, "DD / MMM / YYYY")#</td>
                  
                          <cfquery name="linked_quote" datasource="agbcodes_ldi">
                              SELECT id, linked_enq_id
                              FROM quotes
                              WHERE linked_enq_id = "#id#"
                          </cfquery>

                          <cfif linked_quote.recordcount GTE '1'>
                            <cfloop query="linked_quote">
                              <td><a href="quote_management.cfm?id=#linked_quote.id#" class="btn btn-outline-success btn-block">View linked Quote ###linked_quote.id#</a></td>
                            </cfloop>
                          <cfelse>
                            <td><a href="processing_files/enquiry_2_quote.cfm?id=#id#" class="btn btn-outline-warning btn-block">Convert to Quote</a></td>
                          </cfif>

                        </tr>

                      </cfloop>

                  </tbody>                

                </table>


                </div>

                

            </cfoutput>

      </main>

    <hr>
    <!-- End of page main content -->

    <cfinclude template="includes/footer.cfm">
    <cfinclude template="includes/java_include.cfm">

</body>
</html>