<cfquery name="quote_items" datasource="agbcodes_ldi">
  SELECT *
  FROM quote_items
  WHERE quote_id = '#this_quote.id#'
</cfquery>

<cfoutput query="quote_items">

<cfif quote_items.recordcount gte '1'>


<table style="width:100%;">
      
      <tr>
        <td colspan="5" style="text-align:center;"><h3>Total Items: #quote_items.RecordCount#</h3></td>
      </tr>

      <tr>
        <th style="width:20%;">Item ## #quote_items.currentRow#</th>
        <td style="width:20%;">"#URLDecode(quote_items.item_name)#"</td>
        <td style="width:20%;"></td>
        <th style="width:20%;">Item Qty</th>
        <td style="width:20%;">"#quote_items.item_qty#"</td>
      </tr>
</table>

      <table style="width:100%; text-align:center;">

        <tr style="text-align:center;">
          <td></td>
          <th>Preventer Type</th>
          <th>Volume</th>
          <th>Membrane</th>
          <th>Pressure</th>
          <th>Connection</th>
          <th>Wetted Material</th>
          <th>Miscellaneous</th>
        </tr>

        <tr>
          <td>Unit Refrence:</td>
          <td>"#URLDecode(quote_items.preventer_type)#"</td>
          <td>"#URLDecode(quote_items.volume)#"</td>
          <td>"#URLDecode(quote_items.membrane)#"</td>
          <td>"#URLDecode(quote_items.pressure)#"</td>
          <td>"#URLDecode(quote_items.connection)#"</td>
          <td>"#URLDecode(quote_items.wetted_material)#"</td>
          <td>"#URLDecode(quote_items.miscellaneous)#"</td>
        </tr>

        <tr>
          <td>&nbsp;</td>
        </tr>

      </table>

      <table style="width:100%;">

        <tr>
          <th>Working Pressure</th>
          <td>"#quote_items.working_pressure# #URLDecode(quote_items.working_pressure_units)#"</td>
          <td></td>
          <td style="text-align:center;" colspan="2"><h3>Materials of construction</h3></td>
        </tr>

        <tr>
          <th style="width:20%;">Design Pressure</th>
          <td style="width:20%;">"#URLDecode(quote_items.design_pressure)# #URLDecode(quote_items.design_pressure_units)#"</td>
          <td style="width:20%;"></td>
          <th style="width:20%;">Wetted Matallics</th>
          <td style="width:20%;">"#URLDecode(quote_items.wetted_material)#"</td>
        </tr>

        <tr>
          <th>Pre-fill Pressure</th>
          <td>"#URLDecode(quote_items.miscellaneous)#"</td>
          <td></td>
          <th>Non-Wetted Matallics</th>
          <td>"#URLDecode(quote_items.non_wetted_material)#"</td>
        </tr>

        <tr>
          <th>Nominal Length</th>
          <td>"#URLDecode(quote_items.length)#"</td>
          <td></td>
          <th>Flexing membrane &amp; sealing</th>
          <td>"#URLDecode(quote_items.flexing_membrane)#"</td>
        </tr>

        <tr>
          <th>Nominal Width</th>
          <td>"#URLDecode(quote_items.width)#"</td>
          <td></td>
          <th>Painting Specifications</th>
          <td>"#URLDecode(quote_items.paint_spec)#"</td>
        </tr>

        <tr>
          <th>Design Code</th>
          <td colspan="4">"#URLDecode(quote_items.design_code)#"</td>
        </tr>

        <tr>
          <th>Price per Unit</th>
          <td colspan="4">&pound; #URLDecode(quote_items.item_price)#</td>
        </tr>

      </table>


    </cfif>

  <hr>

</cfoutput>