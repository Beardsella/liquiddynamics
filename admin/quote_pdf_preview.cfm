<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="images/favicon.ico">
  <meta name="author" content="Adam Beardsell - http://agb.codes">
  <meta name="description" content="Liquid Dynamics International - Engineering and manufacturing services">
  <meta name="keywords" content="Liquid Dynamics, Engineering, Oil Rig Manufacturing Specialists">


  <!-- Custom styles for this template -->
  <link href="css/custom_pdf.css" rel="stylesheet">

  <cfquery name="this_quote" datasource="agbcodes_ldi">
    SELECT *
    FROM quotes
    WHERE id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quote_params" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_params
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfquery name="this_quotes_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_items
    WHERE quote_id = "#URL.id#"
  </cfquery>
  
  <cfquery name="this_quote_extra_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_extra_items
    WHERE quote_id = "#URL.id#"
  </cfquery>

  <cfquery name="this_job" datasource="agbcodes_ldi">
    SELECT *
    FROM job_vs_quote
    WHERE quote_id = "#URL.id#"
  </cfquery>

 <cfif URL.type eq "wo">

    <cfset doc_purpose = "Works Order">
    <cfset doc_purpose_abbrevation = "W">

  <cfelseif URL.type eq "q">

    <cfset doc_purpose = "Quote">
    <cfset doc_purpose_abbrevation = "Q">

  <cfelseif URL.type eq "i">

    <cfset doc_purpose = "Invoice">
    <cfset doc_purpose_abbrevation = "i">

  </cfif>

  <cfquery name="last_quote" datasource="agbcodes_ldi">
    SELECT MAX(doc_version) as last_version
    FROM pdfs_db
    WHERE linked_job_id = "#this_job.job_id#" AND doc_subject = "#doc_purpose#"
  </cfquery>


  <cfif isDefined("this_quote.seller") AND this_quote.seller neq "">

    <cfset selling_company = "#URLDecode(this_quote.seller)#">

    <cfif selling_company eq "Liquid Dynamics International">
      <cfset formated_id = "LDI-UK-#doc_purpose_abbrevation#-#this_quote.id#">
    <cfelseif selling_company eq "Pulse Gard">
      <cfset formated_id = "PG-UK-#doc_purpose_abbrevation#-#this_quote.id#">
    <cfelseif selling_company eq "Shock Gard">
      <cfset formated_id = "SG-UK-#doc_purpose_abbrevation#-#this_quote.id#">
    <cfelseif selling_company eq "Hydrotrole">
      <cfset formated_id = "HT-UK-#doc_purpose_abbrevation#-#this_quote.id#">
    </cfif>

  </cfif>  

  <cfif last_quote.last_version NEQ ''>
    <cfset this_quote_version =  '#last_quote.last_version#'>

    <cfset this_quote_version =  '#this_quote_version#' + '1'>
    <cfset quote_id_version =  "#formated_id#-Ver-#this_quote_version#">
  <cfelse>
      <cfset this_quote_version =  "1">
      <cfset quote_id_version =  "#formated_id#">
  </cfif>

  <cfset quote_date_formated = "#DateFormat(this_quote.quote_dated, 'dd/mm/yyyy')#">
  <cfset dispatch_date_formated = "#DateFormat(this_quote.dispatch_estimate, 'dd/mm/yyyy')#">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</head>
<body>
<cfoutput>
  <div class="col-12 text-center">

    <p class="lead">Once your happy with your PDF preview click the button below to generate you PDF</p>

    <a href="processing_files/pdf_create.cfm?id=#url.id#&type=#URL.type#" class="btn btn-success">Generate PDF</a>

  </div>
<hr>
</cfoutput>
<div id="container">

  <row class="col-12">

    <cfinclude template="quote_pdf_template_part_1.cfm">

    <cfinclude template="quote_pdf_template_part_2.cfm">

    <cfinclude template="quote_pdf_template_part_3.cfm">

  </row>
  
</div>
</body>
</html>