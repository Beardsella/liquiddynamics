<!DOCTYPE html>
<html lang="en">
<head>

    <cfinclude template="includes/default_header.cfm">

    <title>Contact Detail - Liquid Dynamics Group Ltd</title>

    <cfquery name="this_contact" datasource="agbcodes_ldi">
        SELECT *
        FROM contacts
        WHERE id = #URL.id#
    </cfquery>

    <cfquery name="countries" datasource="agbcodes_ldi">
        SELECT *
        FROM countries
    </cfquery>

    <cfquery name="this_company" datasource="agbcodes_ldi">
        SELECT id, company_name
        FROM companies
        WHERE id = "#this_contact.company_id#"
    </cfquery>

</head>
<body>

  <cfinclude template="includes/nav.cfm">

    <!-- Page Content -->
    <main role="main" class="container">

          <cfoutput>

            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.cfm">Home</a></li>
                <li class="breadcrumb-item"><a href="contacts_overview.cfm">Contacts Overview</a></li>
                <li class="breadcrumb-item active" aria-current="page">Contact Detail</li>
              </ol>
            </nav>            
          

          <cfoutput>
          <div class="row">
            <div class="col-md-12">
                <h1 class="page-header">Contact Detail</h1>
            </div>
            <div class="col-md-6">
                <p class="lead">Here you can view and edit details of this contact</p>
            </div>
            <div class="col-md-6">
                <p class="text-right lead">Company: <a href="company_detail.cfm?id=#this_company.id#"><span class="badge badge-primary">#URLDecode(this_company.company_name)#</span></a></p>
            </div>
          </div>
          </cfoutput>

          <form class="form-horizontal" action="processing_files/contact_update.cfm" method="post" enctype="multipart/form-data">

            <input type="hidden" name="contact_id" id="contact_id" value="#this_contact.id#">

                <div class="row">
                    <div class="col">
                      <label for="title">Title</label>
                      <input type="text" class="form-control" id="title" name="title" placeholder="Mr / Mrs / Miss" value="#this_contact.title#">
                    </div>

                    <div class="col">
                      <label for="first_name">First Name</label>
                      <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Joe" value="#URLDecode(this_contact.first_name)#">
                    </div>

                    <div class="col">
                      <label for="last_name">Last Name</label>
                      <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Bloggs" value="#URLDecode(this_contact.last_name)#">
                    </div>
                </div>

                <!-- System Operating Temperature -->
                <div class="form-group">
                  <label for="telephone">Telephone</label>
                  <input type="text" class="form-control" id="telephone" name="telephone" value="#URLDecode(this_contact.telephone)#">
                </div>

                <!-- System Design Temperature -->
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email" value="#URLDecode(this_contact.email)#">
                </div>
                
                <!-- System Maximum Working Pressure -->
                <div class="form-group">
                  <label for="staff_role">Staff Role</label>
                  <input type="text" class="form-control" id="staff_role" name="staff_role" value="#URLDecode(this_contact.staff_role)#">
                </div>
                
                <!-- Connection Type Required (Flange, NPT, Tri Clamp etc) -->
                <div class="form-group">
                   <label for="country_based">Country Based</label>
                    <select id="country_based" name="country_based" class="form-control">
                      <option value="">#URLDecode(this_contact.country_based)#</option>
                        <cfloop query="countries">
                              <option value="#country#">#country#</option>
                        </cfloop>
                    </select>
                </div>

                <div class="form-group">
                  <label for="contact_notes">Notes</label>
                  <textarea class="form-control" id="contact_notes" name="contact_notes" rows="3">#URLDecode(this_contact.notes)#</textarea>
                </div>

                <button type="submit" class="btn btn-success btn-block">Update Contact</button>
              </form>

              </cfoutput>

      </main>

    <hr>
    <!-- End of page main content -->

    <cfinclude template="includes/footer.cfm">
    <cfinclude template="includes/java_include.cfm">

</body>
</html>