<!DOCTYPE html>
<html lang="en">
<head>

  <cfinclude template="includes/default_header.cfm">

  <cfoutput>
    <title>Customer Enquiry ###URL.id# - Liquid Dynamics Group Ltd</title>
  </cfoutput>

  <!-- CSS for autocomplete -->
  <link rel="stylesheet" href="css/jquery-ui-1.12.1.css">

  <cfquery name="this_enquiry" datasource="agbcodes_ldi">
      SELECT *
      FROM enquirys_pump_systems
      WHERE id = #URL.id#
  </cfquery>
  <cfquery name="this_contact" datasource="agbcodes_ldi">
      SELECT *
      FROM contacts
      WHERE id = #this_enquiry.contact_id#
  </cfquery>
  <cfquery name="this_company" datasource="agbcodes_ldi">
      SELECT *
      FROM companies
      WHERE id = #this_enquiry.company_id#
  </cfquery>

  <cfquery name="all_quotes_items" datasource="agbcodes_ldi">
    SELECT *
    FROM quote_items
  </cfquery>

  <cfquery name="all_enq_items" datasource="agbcodes_ldi">
    SELECT pumped_liquid
    FROM enquirys_pump_systems
  </cfquery>

  <cfquery name="linked_quote" datasource="agbcodes_ldi">
      SELECT id, linked_enq_id
      FROM quotes
      WHERE linked_enq_id = "#id#"
  </cfquery>

</head>

<body>

  <cfinclude template="includes/nav.cfm">

        <cfoutput>


  <!-- Page Content -->
  <main role="main" class="container">

  <div class="col">
    <cfoutput>
      <h1 class="page-header" id="content">Customer Enquiry - "#this_enquiry.seller#"</h1> 

      <span class="float-right">
        <span class="badge badge-primary">ID: ###this_enquiry.formatted_id#</span> <br>
        <span class="badge badge-info">Enquiry Date: #DateFormat("#this_enquiry.enquiry_date#", "dd / mmm / yyyy")#</span>
      </span>



    <cfif linked_quote.recordcount GTE '1'>
      <cfloop query="linked_quote">
        <a href="quote_management.cfm?id=#linked_quote.id#" class="btn btn-outline-success">View linked Quote</a>
      </cfloop>
    <cfelse>
      <a href="processing_files/enquiry_2_quote.cfm?id=#id#" class="btn btn-outline-warning">Convert to Quote</a>
    </cfif>

    </cfoutput>
  </div>

  <hr>

  <form class="form" action="processing_files/enquirys_pump_systems_insert.cfm" method="post" enctype="multipart/form-data">


        <h3>Contact Details - <small><a href="contact_detail.cfm?id=#this_contact.id#">#this_contact.first_name# #this_contact.last_name#</a></small></h3>
        <hr>


          <div class="row">
            <div class="col">
              <div class="form-group">
              <label for="first_name">First Name</label>
                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Joe" value="#this_contact.first_name#">
                </div>
            </div>
            <div class="col">
              <div class="form-group">
              <label for="last_name">Last Name</label>
                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Bloggs" value="#this_contact.last_name#">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col">
            <div class="form-group">
            <label for="enquiry_email">Email Address</label>
              <input type="email" class="form-control" id="enquiry_email" name="enquiry_email" placeholder="Email Address" value="#URLDecode(this_contact.email)#">
              </div>
            </div>

            <div class="col">
            <div class="form-group">
            <label for="contact_telephone">Phone Number</label>
              <input type="text" class="form-control" id="contact_telephone" name="contact_telephone" placeholder="Phone Number" value="#this_contact.telephone#">
              </div>
            </div>
          </div>
 

          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="##contact_details" aria-expanded="false" aria-controls="contact_details">
            More Contact Details...
          </button>
        </p>
        <div class="collapse" id="contact_details">
        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="staff_role">Staff Role</label>
              <input type="text" class="form-control" id="staff_role" name="staff_role" placeholder="Purchasing Manager" value="#this_contact.staff_role#">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="contact_country_based">Country Based</label>
              <input type="text" class="form-control" id="contact_country_based" name="contact_country_based" placeholder="UK" value="#this_contact.country_based#">
              </div>
            </div>
        </div> 

        <div class="form-group">
        <label for="contact_notes">Contact Notes</label>
          <textarea class="form-control" id="contact_notes" name="contact_notes">#URLDecode(this_contact.notes)#</textarea>
        </div>

        </div>

        <h3>Company Details - <small><a href="company_detail.cfm?id=#this_company.id#">#URLDecode(this_company.company_name)#</a></small></h3>
        <hr>

            <div class="form-group">
            <label for="company">Company Name</label>
              <input type="text" class="form-control" id="company" name="company" placeholder="Company" value="#URLDecode(this_company.company_name)#">
            </div>

          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="##company_details" aria-expanded="false" aria-controls="company_details">More Company Details...</button>
        
        <div class="collapse" id="company_details">

        <div class="row">
            <div class="col-lg-6">
            <div class="form-group">
            <label for="company_email">Email Address</label>
              <input type="email" class="form-control" id="company_email" name="company_email" placeholder="Email Address" value="#URLDecode(this_company.email)#">
              </div>
            </div>

            <div class="col-lg-6">
            <div class="form-group">
            <label for="company_telephone">Phone Number</label>
              <input type="text" class="form-control" id="company_telephone" name="company_telephone" placeholder="Phone Number" value="#this_company.telephone#">
              </div>
            </div>
        </div> 

        <div class="row">
            <div class="col-lg-12">
            <div class="form-group">
            <label for="company_address">Address</label>
              <input type="text" class="form-control" id="company_address" name="company_address" placeholder="Email Address" value="#URLDecode(this_company.mail_1)#">
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
              <label for="company_town">Town</label>
                <input type="text" class="form-control" id="company_town" name="company_town" placeholder="Town" value="#this_company.town#">
                </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
              <label for="company_postcode">Post Code</label>
                <input type="text" class="form-control" id="company_postcode" name="company_postcode" placeholder="Post Code" value="#URLDecode(this_company.postcode)#">
                </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
              <label for="company_country">Country</label>
                <input type="text" class="form-control" id="company_country" name="company_country" placeholder="Country" value="#this_company.country#">
                </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
              <label for="company_notes">Company Notes</label>
                <textarea class="form-control" id="company_notes" name="company_notes">#URLDecode(this_company.notes)#</textarea>
              </div>
            </div>

        </div> 

        </div>

        <h3>Enquiry Details</h3>
        <hr>

      <div class="row">
        
        <div class="col-4">
          <div class="form-group">
           <label for="problem">Type of Problem?</label>
            <select id="problem" name="problem" class="form-control">
              <option value="#this_enquiry.problem#">#URLDecode(this_enquiry.problem)#</option>
              <option value="Other / Not Sure">Other / Not Sure</option>
              <option value="Plusation Suction">Plusation Suction</option>
              <option value="Plusation Discharge">Plusation Discharge</option>
              <option value="Frequency">Frequency</option>
              <option value="Thermal Expansion">Thermal Expansion</option>
              <option value="Accumulation">Accumulation</option>
              <option value="Shock Alleviation / Water Hammer">Shock Alleviation / Water Hammer</option>
            </select>
          </div>
        </div>

        <div class="col-4">
          <div class="form-group">
           <label for="pump_type">Type of Pump</label>
            <select id="pump_type" name="pump_type" class="form-control">
              <option value="#this_enquiry.pump_type#">#URLDecode(this_enquiry.pump_type)#</option>
              <option value="Other">Other</option>
              <option value="Centrifugal Pumps">Centrifugal Pumps</option>
              <option value="AODD">AODD</option>
              <option value="Hose Pump">Hose Pump</option>
              <option value="Gear Pump">Gear Pump</option>
              <option value="Vane Pump">Vane Pump</option>
              <option value="Packed Plunger Pump">Packed Plunger Pump</option>
              <option value="Metering Pump">Metering Pump</option>
              <option value="Progressive Cavity">Progressive Cavity</option>
              <option value="Power Pump">Power Pump</option>
            </select>
          </div>
        </div>

        <div class="col-4">
          <!-- Number of Displacers (Simplex, Duplex, Triplex etc) -->
          <div class="form-group">
           <label for="num_of_displacers">Number of Displacers</label>
            <select id="num_of_displacers" name="num_of_displacers" class="form-control">
              <option value="#this_enquiry.num_of_displacers#">#this_enquiry.num_of_displacers#</option>
              <option value="Simplex">1 - (Simplex)</option>
              <option value="Duplex">2 - (Duplex)</option>
              <option value="Triplex">3 - (Triplex)</option>
              <option value="Other">Other</option>
            </select>
          </div>
        </div>

      </div>



        

        
        <div class="row">
            <div class="col-lg-3">
            <div class="form-group">
            <label for="speed_value">Flow Rate</label>
                  <input type="number" class="form-control" id="flow_rate" name="flow_rate" value="#this_enquiry.flow_rate#">
            </div>
            </div>

            <div class="col-lg-3">
            <div class="form-group">
            <label for="flow_rate_units">Flow Rate Units</label>
                <select id="flow_rate_units" name="flow_rate_units" class="form-control">
                  <option value="#this_enquiry.flow_rate_units#">#URLDecode(this_enquiry.flow_rate_units)#</option>
                  <option value="Litres per minute">Litres per minute</option>
                  <option value="Litres per hour">Litres per hour</option>
                </select>
            </div>
            </div>


            <div class="col-lg-3">
            <div class="form-group">
            <label for="speed_value">Speed Value</label>
                  <input type="number" class="form-control" id="speed_value" name="speed_value" value="#this_enquiry.speed_value#">
            </div>
            </div>

            <div class="col-lg-3">
            <div class="form-group">
              <label for="SPMorRPM">Units of measurement for speed</label>
              <select id="SPMorRPM" name="SPMorRPM" class="form-control">
                <option value="#URLDecode(this_enquiry.SPMorRPM)#">#URLDecode(this_enquiry.SPMorRPM)#</option>
                <option value="SPM">SPM <small>(strokes per minute)</small></option>
                <option value="RPM">RPM <small>(revs per minute)</small></option>
              </select>
            </div>
            </div>
          </div>          




        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="##stroke_displacement_box" aria-expanded="false" aria-controls="stroke_displacement_box">Please click here if you don't know your SPM  / RPM</button>

        <div class="collapse" id="stroke_displacement_box">
          <div class="form-group">
            <label for="stroke_displacement">What is you displacement per stroke?</label>
            <input type="text" class="form-control" id="stroke_displacement" name="stroke_displacement" value="#URLDecode(this_enquiry.stroke_displacement)#">
          </div>     
          <hr>   
        </div>
        
        <div class="row">
          
          <div class="col-4">
            <label for="piston_diameter">Piston Diameter</label>
            <input type="text" class="form-control" id="piston_diameter" name="piston_diameter" value="#URLDecode(this_enquiry.piston_diameter)#">
          </div>

          
          <div class="col-4">
            <label for="stroke_length">Stroke Length</label>
            <input type="text" class="form-control" id="stroke_length" name="stroke_length" value="#URLDecode(this_enquiry.stroke_length)#">
          </div>

          
          <div class="col-4">
            <label for="damping_degree">Degree of damping %</label>
            <input type="text" class="form-control" id="damping_degree" name="damping_degree" value="#URLDecode(this_enquiry.damping_degree)#">
          </div>

        </div>

        <div class="row">
          
          <div class="col-4">
            
            <div class="form-group">
                <label for="material_wetted">MOC - Wetted Metallics WIP</label>
                <button type="button" class="btn btn-link pull-right" data-toggle="popover" data-placement="left" title="Materials of Construction - Metalics" data-content="Please enter whatever metalic materials you need here">?</button>
                <input type="text" class="form-control" id="material_wetted" name="material_wetted" list="wetted_material_list" value="#URLDecode(this_enquiry.material_wetted)#">
            </div>
          
          </div>  

          <datalist id="wetted_material_list">
            <cfloop query="all_quotes_items" group="wetted_material">
              <option value="#URLDecode(wetted_material)#">
            </cfloop>
          </datalist>

          <div class="col-4">     

            <div class="form-group">
                <label for="material_non_wetted">MOC - Non Wetted Metallics WIP</label>
                <button type="button" class="btn btn-link pull-right" data-toggle="popover" data-placement="left" title="Materials of Construction - Metalics" data-content="Please enter whatever metalic materials you need here">?</button>
                <input type="text" class="form-control" id="material_non_wetted" name="material_non_wetted" list="non_wetted_material_list" value="#URLDecode(this_enquiry.material_non_wetted)#">
            </div>  
          
          </div> 

      <datalist id="non_wetted_material_list">
        <cfloop query="all_quotes_items" group="non_wetted_material">
          <option value="#URLDecode(non_wetted_material)#">
        </cfloop>
      </datalist>

          <div class="col-4">
            
            <div class="form-group">
                <label for="plastic_system_materials">MOC - Elastomer</label>
                <button type="button" class="btn btn-link pull-right" data-toggle="popover" data-placement="left" title="Materials of Construction - Elastomer" data-content="Please enter whatever material you need for the Elastomer">?</button>
                <input type="text" class="form-control" id="plastic_system_materials" name="plastic_system_materials" list="flexing_membrane_list" value="#URLDecode(this_enquiry.plastic_system_materials)#">
            </div> 
          
          </div>

      <datalist id="flexing_membrane_list">
        <cfloop query="all_quotes_items" group="flexing_membrane">
          <option value="#URLDecode(flexing_membrane)#">
        </cfloop>
      </datalist>

        </div>


        <div class="row">

        <div class="col-3">
        <!-- Liquid being pumped (water, diesel, slurry etc) -->
        <div class="form-group">
            <label for="pumped_liquid">Fluid being pumped</label>
            <input type="text" class="form-control" id="pumped_liquid" name="pumped_liquid" list="fluid_list" value="#URLDecode(this_enquiry.pumped_liquid)#">
        </div>  
        </div>


      <datalist id="fluid_list">
        <cfloop query="all_enq_items" group="pumped_liquid">
          <option value="#URLDecode(pumped_liquid)#">
        </cfloop>
      </datalist>

        <div class="col-3">
        <!-- Pipework Size -->
        <div class="form-group">
          <label for="pipe_size">Pipework Size</label>
          <input type="text" class="form-control" id="pipe_size" name="pipe_size" value="#URLDecode(this_enquiry.pipe_size)#">
        </div>

        </div>
        <div class="col-3">

        <!-- Connection Type Required (Flange, NPT, Tri Clamp etc) -->
        <div class="form-group">
           <label for="connection_type">Connection Type Required</label>
            <select id="connection_type" name="connection_type" class="form-control">
              <option value="#URLDecode(this_enquiry.connection_type)#">#URLDecode(this_enquiry.connection_type)#</option>
              <option value="Flange">Flange</option>
              <option value="NPT">NPT</option>
              <option value="BSP">BSP</option>
              <option value="Tri Clamp">Tri Clamp</option>
              <option value="Other">Other</option>
            </select>
        </div>

        </div>
        <div class="col-3">
          
        <!-- Single Connection or Twin Flow-Through Required -->
        <div class="form-group">
           <label for="connection_type_2">Single or Twin Connection</label>
            <select id="connection_type_2" name="connection_type_2" class="form-control">
              <option value="#URLDecode(this_enquiry.connection_type_2)#">#URLDecode(this_enquiry.connection_type_2)#</option>
              <option value="Single Connection">Single Connection</option>
              <option value="Twin Flow-Through">Twin Flow-Through</option>
            </select>
        </div>

        </div>
          
    </div>

        <!-- System Operating Temperature -->
        <div class="row">
            <div class="col-lg-3">
            <div class="form-group">
            <label for="system_temp">Max Operating Temperature</label>
                  <input type="number" class="form-control" id="system_temp" name="system_temp" value="#URLDecode(this_enquiry.system_temp)#">
            </div>
            </div>

            <div class="col-lg-3">
            <div class="form-group">
            <label for="system_temp_units">System Operating Units</label>
                <select id="system_temp_units" name="system_temp_units" class="form-control">
                  <option value="#this_enquiry.system_temp_units#">#URLDecode(this_enquiry.system_temp_units)#</option>
                  <option value="Degrees Celsius">Degrees Celsius</option>
                  <option value="Fahrenheit">Fahrenheit</option>
                </select>
            </div>
            </div>

            <div class="col-lg-3">
            <div class="form-group">
            <label for="system_design_temp">System Design Temperature</label>
                  <input type="number" class="form-control" id="system_design_temp" name="system_design_temp"  value="#URLDecode(this_enquiry.system_design_temp)#">
            </div>
            </div>

            <div class="col-lg-3">
            <div class="form-group">
            <label for="system_design_temp_units">System Design Units</label>
                <select id="system_design_temp_units" name="system_design_temp_units" class="form-control">
                  <option value="#this_enquiry.system_design_temp_units#">#URLDecode(this_enquiry.system_design_temp_units)#</option>
                  <option value="Degrees Celsius">Degrees Celsius</option>
                  <option value="Fahrenheit">Fahrenheit</option>
                </select>
            </div>
            </div>
          </div>
        
        <!-- System Maximum Working Pressure -->
        <div class="row">
            <div class="col-lg-3">
            <div class="form-group">
            <label for="max_system_pressure">System Working Pressure</label>
                  <input type="number" class="form-control" id="max_system_pressure" name="max_system_pressure" placeholder="80"  value="#URLDecode(this_enquiry.max_system_pressure)#">
            </div>
            </div>

            <div class="col-lg-3">
            <div class="form-group">
            <label for="max_system_pressure_units">System Working Pressure Units</label>
                <select id="max_system_pressure_units" name="max_system_pressure_units" class="form-control">
                  <option value="#this_enquiry.max_system_pressure_units#">#URLDecode(this_enquiry.max_system_pressure_units)#</option>
                  <option value="PSI">PSI</option>
                  <option value="BAR">BAR</option>
                </select>
            </div>
            </div>

            <div class="col-lg-3">
            <div class="form-group">
            <label for="system_design_pressure">System Design Pressure</label>
                  <input type="number" class="form-control" id="system_design_pressure" name="system_design_pressure" value="#URLDecode(this_enquiry.system_design_pressure)#">
            </div>
            </div>

            <div class="col-lg-3">
            <div class="form-group">
            <label for="system_design_pressure_units">System Design Pressure Units</label>
                <select id="system_design_pressure_units" name="system_design_pressure_units" class="form-control">
                  <option value="PSI">PSI</option>
                  <option value="BAR">BAR</option>
                </select>
            </div>
            </div>
          </div>


        <h3>Enquiry Action</h3>
        <hr>


        <cfif this_enquiry.actioned eq '0'>
          <cfset option_title = "New">
        <cfelseif this_enquiry.actioned eq '3'>
          <cfset option_title = "Wating for Client">
        <cfelseif this_enquiry.actioned eq '2'>
          <cfset option_title = "In Progress">
        <cfelseif this_enquiry.actioned eq '4'>
          <cfset option_title = "Completed">
        </cfif>

        <!-- Connection Type Required (Flange, NPT, Tri Clamp etc) -->
        <div class="form-group">
           <label for="actioned">Enquiry Status</label>
            <select id="actioned" name="actioned" class="form-control">
              <option value="#URLDecode(this_enquiry.actioned)#">#option_title#</option>
              <option value="0">New</option>
              <option value="3">Wating for Client</option>
              <option value="2">In Progress</option>
              <option value="4">Completed</option>
            </select>
        </div>

        <div class="form-group">
          <label>Action Date</label>
            <input type="text" id="action_date" name="action_date" class="form-control" placeholder="A reminder date to action the next step of their query" value="#this_enquiry.action_date#">
        </div>

        <div class="form-group">
        <label for="enquiry_notes">Enquiry Notes</label>
          <textarea class="form-control" id="enquiry_notes" name="enquiry_notes">#URLDecode(this_enquiry.notes)#</textarea>
        </div>
        <button type="submit" class="btn btn-success btn-block">Save Enquiry</button>

      </form>

    </main>
    <!-- End of container -->

            </cfoutput>


    <hr>

  <cfinclude template="includes/footer.cfm">
  <cfinclude template="includes/java_include.cfm">

  <!-- JS for autocomplete -->
  <script src="js/jquery-ui-1.12.1.js"></script>
  <script>
  $( function() {
    var availableTags1 = [
      "Stainless Steel",
      "Duplex",
      "Carbon Steel"
    ];
    $( "#metalic_system_materials" ).autocomplete({
      source: availableTags1
    });
  } );

    $( function() {
    var availableTags2 = [
      "Nitrile",
      "EPDM",
      "Viton"
    ];
    $( "#plastic_system_materials" ).autocomplete({
      source: availableTags2
    });
  } );

$(function() {
        $( "#action_date" ).datepicker({
            dateFormat : 'dd/mm/yy',
            changeMonth : true,
            changeYear : true,
        });
    }); 

$(function () {
  $('[data-toggle="popover"]').popover()
})
     </script>
  <!-- End of autocomplete JS -->
  
</body>
</html>