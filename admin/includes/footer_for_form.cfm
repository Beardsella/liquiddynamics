<!-- Visible Footer -->
<footer>
    <div class="row">
        <div class="col-lg-12">
	        <hr>
            <p>Copyright &copy; Liquid Dynamics Group Ltd 2018</p>
        </div>
    </div>
</footer>

<!-- div.container-fluid > div.row = Closed -->
</div>
</div>    

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery.1.11.2.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<!-- tablesorter - https://github.com/christianbach/tablesorter -->
<script type="text/javascript" src="js/jquery-latest.js"></script> 
<script type="text/javascript" src="js/jquery.tablesorter.js"></script> 

<script type="text/JavaScript">
	$(document).ready(function(){ 
		$("#ss_table").tablesorter(); 
	}); 
</script>
<!-- End of tablesorter -->