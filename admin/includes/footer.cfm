<footer class="footer bg-dark">

	<nav class="navbar navbar-expand-md navbar-dark bg-dark">

		<div class="container-fluid">
	        <div class="">
	          <a href="http://agb.codes" target="_blank" class="agb-link">Built by agb.codes</a>
	        </div>
	        <div class="text-center">
	          <a href="" class="agb-link">Copyright &copy; Liquid Dynamics Group Ltd 2018</a>
	        </div>
	        <div class="text-right">
	          <a class="agb-link" href="#top">Back to top</a>
	        </div>
		</div>
	
	</nav>

</footer>