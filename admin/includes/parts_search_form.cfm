<cfquery name="all_parts_data" datasource="agbcodes_ldi" cachedWithin="#createTimeSpan( 0, 1, 0, 0 )#">
  SELECT preventer_type, volume, membrane, PRESS, connection, soild_material, OTHER
  FROM alpha4
</cfquery>

<button class="btn btn-primary float-right" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
    Advanced Search
  </button>

<p class="lead">A searchable DB of replacement parts.</p>

<cfif isDefined("FORM.preventer_type") OR isDefined("FORM.volume") OR isDefined("FORM.membrane") OR isDefined("FORM.PRESS") OR isDefined("FORM.part_connection") OR isDefined("FORM.soild_material") OR isDefined("FORM.OTHER")>

  <cfoutput>

    <p>Current search:</p>
    <ul>
      <cfif FORM.preventer_type IS NOT "">
        <li>preventer_type #qualifer_1# "#FORM.preventer_type#"</li>
      </cfif>
       <cfif FORM.volume IS NOT "">
        <li>volume #qualifer_2# "#FORM.volume#"</li>
      </cfif>
       <cfif FORM.membrane IS NOT "">
        <li>membrane #qualifer_3# "#FORM.membrane#"</li>
      </cfif>
       <cfif FORM.PRESS IS NOT "">
        <li>PRESS #qualifer_4# "#FORM.PRESS#"</li>
      </cfif>
       <cfif FORM.part_connection IS NOT "">
        <li>part_connection #qualifer_5# "#FORM.part_connection#"</li>
      </cfif>
       <cfif FORM.soild_material IS NOT "">
        <li>soild_material #qualifer_6# "#FORM.soild_material#"</li>
      </cfif>
       <cfif FORM.OTHER IS NOT "">
        <li>OTHER #qualifer_7# "#FORM.OTHER#"</li>
      </cfif>
    </ul>

  </cfoutput>

</cfif>


<div class="collapse" id="collapseExample">
  <div class="card card-body">


<form class="form" action="parts.cfm" method="post" enctype="multipart/form-data">

  <div class="row">

    <div class="col-md">
      <label for="preventer_type">Preventer Type</label>
      <div class="input-group mb-3">
        <input list="preventer_types" type="text" class="form-control input-group-text" id="preventer_type" name="preventer_type" placeholder="Search for preventer type">
      </div>
      <div class="form-check fuzzy_search_toggle">
        <input class="form-check-input" type="checkbox" id="fuzzy_search_1" name="fuzzy_search_1" checked>
        <label class="form-check-label" for="fuzzy_search_1">
          Fuzzy Search
        </label>
      </div>
    </div>

    <datalist id="preventer_types">
      <cfoutput query="all_parts_data" group="preventer_type">
        <option value="#URLDecode(preventer_type)#">
      </cfoutput>
    </datalist>

    <div class="col-md">
      <label for="volume">Volume</label>
      <div class="input-group mb-3">
        <input list="volume_list" type="text" class="form-control" id="volume" name="volume" placeholder="Search for volume">
      </div>
            <div class="form-check fuzzy_search_toggle">
        <input class="form-check-input" type="checkbox" id="fuzzy_search_2" name="fuzzy_search_2" checked>
        <label class="form-check-label" for="fuzzy_search_2">
          Fuzzy Search
        </label>
      </div>
    </div>

    <datalist id="volume_list">
      <cfoutput query="all_parts_data" group="volume">
        <option value="#URLDecode(volume)#">
      </cfoutput>
    </datalist>


    <div class="col-md">
      <label for="membrane">Membrane</label>
      <div class="input-group mb-3">
        <input list="membrane_list" type="text" class="form-control" id="membrane" name="membrane" placeholder="Search for membrane">
      </div>
      <div class="form-check fuzzy_search_toggle">
        <input class="form-check-input" type="checkbox" id="fuzzy_search_3" name="fuzzy_search_3" checked>
        <label class="form-check-label" for="fuzzy_search_3">
          Fuzzy Search
        </label>
      </div>
    </div>

    <datalist id="membrane_list">
      <cfoutput query="all_parts_data" group="membrane">
        <option value="#URLDecode(membrane)#">
      </cfoutput>
    </datalist>

    <div class="col-md">
      <label for="PRESS">Pressure</label>
      <div class="input-group mb-3">
        <input list="PRESS_list" type="text" class="form-control" id="PRESS" name="PRESS" placeholder="Search for pressure">
      </div>
      <div class="form-check fuzzy_search_toggle">
        <input class="form-check-input" type="checkbox" id="fuzzy_search_4" name="fuzzy_search_4" checked>
        <label class="form-check-label" for="fuzzy_search_4">
          Fuzzy Search
        </label>
      </div>
    </div>

    <datalist id="PRESS_list">
      <cfoutput  query="all_parts_data" group="PRESS">
        <option value="#URLDecode(PRESS)#">
      </cfoutput>
    </datalist>


    <div class="col-md">
      <label for="part_connection">Connection</label>
      <div class="input-group mb-3">
        <input list="part_connection_list" type="text" class="form-control" id="part_connection" name="part_connection" placeholder="Search for Connection">
      </div>
      <div class="form-check fuzzy_search_toggle">
        <input class="form-check-input" type="checkbox" id="fuzzy_search_5" name="fuzzy_search_5" checked>
        <label class="form-check-label" for="fuzzy_search_5">
          Fuzzy Search
        </label>
      </div>
    </div>

    <datalist id="part_connection_list">
      <cfoutput query="all_parts_data" group="connection">
        <option value="#URLDecode(connection)#">
      </cfoutput>
    </datalist>


    <div class="col-md">
      <label for="soild_material">Soild Material</label>
      <div class="input-group mb-3">
        <input list="soild_material_list" type="text" class="form-control" id="soild_material" name="soild_material" placeholder="Search for Soild Material">
      </div>
      <div class="form-check fuzzy_search_toggle">
        <input class="form-check-input" type="checkbox" id="fuzzy_search_6" name="fuzzy_search_6" checked>
        <label class="form-check-label" for="fuzzy_search_6">
          Fuzzy Search
        </label>
      </div>
    </div>


    <datalist id="soild_material_list">
      <cfoutput query="all_parts_data" group="soild_material">
        <option value="#URLDecode(soild_material)#">
      </cfoutput>
    </datalist>


    <div class="col-md">
      <label for="OTHER">Other</label>
      <div class="input-group mb-3">
        <input list="OTHER_list" type="text" class="form-control" id="OTHER" name="OTHER" placeholder="Search for other">
      </div>
            <div class="form-check fuzzy_search_toggle">
        <input class="form-check-input" type="checkbox" id="fuzzy_search_7" name="fuzzy_search_7" checked>
        <label class="form-check-label" for="fuzzy_search_7">
          Fuzzy Search
        </label>
      </div>
    </div>

    <datalist id="OTHER_list">
      <cfoutput query="all_parts_data" group="OTHER">
        <option value="#URLDecode(OTHER)#">
      </cfoutput>
    </datalist>


  </div>
  <div class="row">
    <div class="col-md">

    <button class="btn btn-outline-success btn-block" type="submit">Search</button>
</div>
  </div>

</form>

  </div>
</div>