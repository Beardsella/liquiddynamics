<!-- Navigation -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
<a class="navbar-brand" href="index.cfm">Liquid Dynamics Group Ltd</a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse justify-content-end" id="navbarsExampleDefault">
    <ul class="navbar-nav">

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="enquiryDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Enquiries
        </a>
        <div class="dropdown-menu" aria-labelledby="enquiryDropdown">
          <a class="dropdown-item" href="customer-enquiry.cfm">New Enquiry</a>
          <a class="dropdown-item" href="enquiries-overview.cfm">Enquiry Overview</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="quoteDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Quotes
        </a>
        <div class="dropdown-menu" aria-labelledby="quoteDropdown">
          <a class="dropdown-item" href="create-quote.cfm">New Quote</a>
          <a class="dropdown-item" href="quote_overview.cfm">Quote Overview</a>
        </div>
      </li>

      <li class="nav-item text-center">
        <a  class="nav-link" href="contracts_overview.cfm">Contracts</a>
      </li>

      <li class="nav-item text-center">
        <a  class="nav-link" href="companies_overview.cfm">Companies</a>
      </li>

      <li class="nav-item text-center">
        <a  class="nav-link" href="contacts_overview.cfm">Contacts</a>
      </li>
      <li class="nav-item text-center">
        <a  class="nav-link" href="parts.cfm">Parts</a>
      </li>
      <li class="nav-item text-right">
        <a  class="nav-link" href="processing_files/logout.cfm">Logout</a>
      </li>
    </ul>    
  </div>
  <!-- /.navbar-collapse -->

</nav>