<!-- jquey -->
<script src="js/jquery-3.2.1.min.js"></script>

<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>

<script src="js/popper.min.js"></script>

<!-- Core bootstrap js functions -->
<script src="js/bootstrap.min.js"></script>

<!-- Cookie banner required by law for cookie useage -->
<script type="text/javascript" id="cookiebanner" src="js/cookiebanner.js"></script>

<!-- tablesorter - https://github.com/christianbach/tablesorter -->
<script type="text/javascript" src="js/jquery.tablesorter.js"></script> 

<script type="text/JavaScript">
	$(document).ready(function(){ 
		$("#ss_table").tablesorter(); 
	}); 
</script>
<!-- End of tablesorter -->

<!-- liquiddynamics Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64993543-1', 'auto');
  ga('send', 'pageview');
</script>
