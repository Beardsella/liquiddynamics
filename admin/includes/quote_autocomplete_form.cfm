<cfquery name="all_parts_data" datasource="agbcodes_ldi" cachedWithin="#createTimeSpan( 0, 1, 0, 0 )#">
  SELECT preventer_type, volume, membrane, PRESS, connection, soild_material, OTHER
  FROM alpha4
</cfquery>

<div class="row mt-3">

  <div class="col-md">
    <p class="align-middle">Unit Ref</p>
  </div>

  <div class="col-md">
    <label for="preventer_type">Preventer Type</label>
    <div class="input-group mb-3">
      <input list="preventer_types" type="text" class="form-control input-group-text" id="preventer_type" name="preventer_type">
    </div>
  </div>

  <datalist id="preventer_types">
    <cfoutput query="all_parts_data" group="preventer_type">
      <option value="#URLDecode(preventer_type)#">
    </cfoutput>
  </datalist>

  <div class="col-md">
    <label for="volume">Volume</label>
    <div class="input-group mb-3">
      <input list="volume_list" type="text" class="form-control" id="volume" name="volume">
    </div>
  </div>

  <datalist id="volume_list">
    <cfoutput query="all_parts_data" group="volume">
      <option value="#URLDecode(volume)#">
    </cfoutput>
  </datalist>


  <div class="col-md">
    <label for="membrane">Membrane</label>
    <div class="input-group mb-3">
      <input list="membrane_list" type="text" class="form-control" id="membrane" name="membrane">
    </div>
  </div>

  <datalist id="membrane_list">
    <cfoutput query="all_parts_data" group="membrane">
      <option value="#URLDecode(membrane)#">
    </cfoutput>
  </datalist>

  <div class="col-md">
    <label for="PRESS">Pressure</label>
    <div class="input-group mb-3">
      <input list="PRESS_list" type="text" class="form-control" id="PRESS" name="PRESS">
    </div>
  </div>

  <datalist id="PRESS_list">
    <cfoutput  query="all_parts_data" group="PRESS">
      <option value="#URLDecode(PRESS)#">
    </cfoutput>
  </datalist>


  <div class="col-md">
    <label for="part_connection">Connection</label>
    <div class="input-group mb-3">
      <input list="part_connection_list" type="text" class="form-control" id="part_connection" name="part_connection">
    </div>
  </div>

  <datalist id="part_connection_list">
    <cfoutput query="all_parts_data" group="connection">
      <option value="#URLDecode(connection)#">
    </cfoutput>
  </datalist>


  <div class="col-md">
    <label for="soild_material">Soild Material</label>
    <div class="input-group mb-3">
      <input list="soild_material_list" type="text" class="form-control" id="soild_material" name="soild_material">
    </div>
  </div>


  <datalist id="soild_material_list">
    <cfoutput query="all_parts_data" group="soild_material">
      <option value="#URLDecode(soild_material)#">
    </cfoutput>
  </datalist>


  <div class="col-md">
    <label for="OTHER">Other</label>
    <div class="input-group mb-3">
      <input list="OTHER_list" type="text" class="form-control" id="OTHER" name="OTHER">
    </div>
  </div>

  <datalist id="OTHER_list">
    <cfoutput query="all_parts_data" group="OTHER">
      <option value="#URLDecode(OTHER)#">
    </cfoutput>
  </datalist>


</div>