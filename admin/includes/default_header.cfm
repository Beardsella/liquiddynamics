<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="images/favicon.ico">
<meta name="author" content="Adam Beardsell - http://agb.codes">
<meta name="description" content="Liquid Dynamics International - Engineering and manufacturing services">
<meta name="keywords" content="Liquid Dynamics, Engineering, Oil Rig Manufacturing Specialists">

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/custom.css" rel="stylesheet">