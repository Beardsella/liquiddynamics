<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="../images/favicon.ico">
<meta name="author" content="Adam Beardsell">

<!-- Bootstrap core CSS -->
<cfif find("localhost", "#CGI.http_host#")>
	<link rel="stylesheet" async type="text/css" href="../css/bootstrap.css">
<cfelse>
	<link rel="stylesheet" async type="text/css" href="../css/bootstrap.css">
</cfif>

<!-- Product Detail CSS -->
<link href="../css/shop-item.css" rel="stylesheet">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<cfif find("localhost", "#CGI.http_host#")>

  <!-- Don't divert for testing local site  -->

<cfelse>

  <!-- Mobile site divert -->
  <script type="text/javascript">
    <!--
    if (screen.width <= 768) {
      window.location = "http://m.liquiddynamics.co";
    }
    //-->
  </script>

</cfif>

<!-- Menu links for nav.cfm-->
<cfquery name="menu_links">
  SELECT *
  FROM menu_links
  ORDER BY postion
</cfquery>

<!-- Reviews for footer.cfm-->
<cfquery name="all_reviews">
	SELECT *
	FROM testimonials
  WHERE display = 1
  ORDER BY display_order ASC
</cfquery>

<cftry>

<cfif find("localhost", "#CGI.http_host#")>

  <cfset this_page_url = #listlast(cgi.cf_template_path, '/')#>

  <cfelse>

  <cfset this_page_url = #listlast(cgi.cf_template_path, '\')#>

</cfif>

  <cfquery name="these_page_vars">
    SELECT *
    FROM pages
    WHERE page_url = "#URLEncode(this_page_url)#"
  </cfquery>

  <cfcatch>
  
    <!-- Not a DB driven page -->

  </cfcatch>

</cftry>