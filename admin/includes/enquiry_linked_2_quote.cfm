<cfoutput query="linked_enq">
  <h2 class="text-center mt-5">Linked Enquiry - "<a href="enquiry_management.cfm?id=#linked_enq.id#">#linked_enq.formatted_id#</a>"</h2>

  <div class="card">
    <div class="card-body">
      
      <div class="row">

        <div class="col-md-12 form-group">
          <label class="control-label" for="quote_intro">Customer Enquiry Notes</label>
          <div class="">                     
            <p class="well">"#URLDecode(linked_enq.notes)#"</p>
          </div>
        </div>

      </div>

      <div class="clearfix"></div>

      <div class="row">

        <div class="col-6">

          <h4 class="text-center">Pump Parameters</h4>

          <div class="row">
            <div class="col-6">Type Of Pump</div>
            <div class="col-6">#URLDecode(linked_enq.pump_type)#</div>
          </div>
          <div class="row">
            <div class="col-6">No of Displacers</div>
            <div class="col-6">#URLDecode(linked_enq.num_of_displacers)#</div>
          </div>
          <div class="row">
            <div class="col-6">Piston Diameter</div>
            <div class="col-6">#URLDecode(linked_enq.piston_diameter)#</div>
          </div>
          <div class="row">
            <div class="col-6">Stroke Length</div>
            <div class="col-6">#URLDecode(linked_enq.stroke_length)#</div>
          </div>
          <div class="row">
            <div class="col-6">Speed</div>
            <div class="col-2">#URLDecode(linked_enq.speed_value)#</div>
            <div class="col-4">#URLDecode(linked_enq.SPMorRPM)#</div>
          </div>
          <div class="row">
            <div class="col-6">Flow Rate</div>
            <div class="col-2">#URLDecode(linked_enq.flow_rate)#</div>
            <div class="col-4">#URLDecode(linked_enq.flow_rate_units)#</div>
          </div>
          <div class="row">
            <div class="col-6">Working Pressure</div>
            <div class="col-2">#URLDecode(linked_enq.max_system_pressure)#</div>
            <div class="col-4">#URLDecode(linked_enq.max_system_pressure_units)#</div>
          </div>
          <div class="row">
            <div class="col-6">Design Pressure</div>
            <div class="col-2">#URLDecode(linked_enq.system_design_pressure)#</div>
            <div class="col-4">#URLDecode(linked_enq.system_design_pressure_units)#</div>
          </div>
          
        </div>
        <div class="col-6">

          <h4 class="text-center">System Info</h4>
          
          <div class="row">
            <div class="col-6">Fluid</div>
            <div class="col-6">#URLDecode(linked_enq.pumped_liquid)#</div>
          </div>

          <div class="row">
            <div class="col-6">Max Operating Temp</div>
            <div class="col-2">#URLDecode(linked_enq.system_design_temp)#</div>
            <div class="col-4">#URLDecode(linked_enq.system_design_temp_units)#</div>
          </div>

          <div class="row">
            <div class="col-6">Degree of Damping %</div>
            <div class="col-6">#URLDecode(linked_enq.damping_degree)#</div>
          </div>

          <hr>

          <h4 class="text-center">Required Materials Of Construction</h4>

          <div class="row">
            <div class="col-6">Wetted Material</div>
            <div class="col-6">#URLDecode(linked_enq.material_wetted)#</div>
          </div>

          <div class="row">
            <div class="col-6">Non-Wetted Material</div>
            <div class="col-6">#URLDecode(linked_enq.material_non_wetted)#</div>
          </div>
          
          <div class="row">
            <div class="col-6">Elastometers</div>
            <div class="col-6">#URLDecode(linked_enq.plastic_system_materials)#</div>
          </div>
          
          <div class="row">
            <div class="col-6">Connection Type</div>
            <div class="col-6">#URLDecode(linked_enq.connection_type)#</div>
          </div>

        </div>
      </div>

    </div>
  </div>

</cfoutput>