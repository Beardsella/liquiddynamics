<cfset common_params = '#common_params#' + '1'>

<div class="row">
  <div class="col-12">
    <h4><span class="badge badge-warning">Common Parameter ###CurrentRow#</span></h4>
  </div>
</div>

<input type="hidden" name="job_param_id_#CurrentRow#" id="job_param_id_#CurrentRow#" value="#new_common_param.id#">

<div class="row">

  <!-- Text input-->
  <div class="col-md-6 form-group">
    <label class="control-label" for="job_parameter_name">Parameter Name</label>  
    <div class="">
      <input id="job_parameter_name_#CurrentRow#" name="job_parameter_name_#CurrentRow#" type="text" placeholder="Working Pressure" class="form-control input-md" value="#URLDecode(param)#">
    </div>
  </div>

  <!-- Text input-->
  <div class="col-md-6 form-group">
    <label class="control-label" for="job_parameter_value">Parameter Value</label>  
    <div class="">
      <input id="job_parameter_value_#CurrentRow#" name="job_parameter_value_#CurrentRow#" type="text" placeholder="100 bar" class="form-control input-md" value="#URLDecode(param_value)#">
    </div>
  </div>

</div>

<div class="row">

  <!-- Textarea -->
  <div class="col-md-12 form-group">
    <label class="control-label" for="job_parameter_notes">Parameter Notes</label>
    <div class="">                     
      <textarea class="form-control" id="job_parameter_notes_#CurrentRow#" name="job_parameter_notes_#CurrentRow#">#URLDecode(this_quote_params.param_notes)#</textarea>
    </div>
  </div>

</div>

<hr>