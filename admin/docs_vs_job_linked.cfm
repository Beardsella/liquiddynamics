  <cfquery name="linked_pdf_quotes" datasource="agbcodes_ldi">
    SELECT *
    FROM pdfs_db
    WHERE db_id = "#this_job.quote_id#" AND doc_subject = "Quote"
    ORDER BY id DESC
  </cfquery>

  <cfquery name="linked_pdf_wo" datasource="agbcodes_ldi">
    SELECT *
    FROM pdfs_db
    WHERE db_id = "#this_job.quote_id#" AND doc_subject = "Works Order"
    ORDER BY id DESC
  </cfquery>

  <cfquery name="linked_pdf_invoices" datasource="agbcodes_ldi">
    SELECT *
    FROM pdfs_db
    WHERE db_id = "#this_job.quote_id#" AND doc_subject = "Invoice"
    ORDER BY id DESC
  </cfquery>

<p class="lead mt-4">An overview of any linked documents to this contract.</p>

<!-- Button trigger modal -->
<button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#checklist_modal">
  Document check list
</button>



<!-- Modal -->
<div class="modal fade" id="checklist_modal" tabindex="-1" role="dialog" aria-labelledby="checklist_modal_CenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="checklist_modal_CenterTitle">Document Checklist</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="lead">Documents on this list should be accounted for in 99% of cases. Red text means that the document is missing, green text means it has already been uploaded</p>
       <ul>


  <cfquery name="uploaded_sp" datasource="agbcodes_ldi">
    SELECT *
    FROM uploaded_files
    WHERE doc_purpose = "Supplier%20Price" AND linked_job = "#this_job.formatted_id#"
  </cfquery>

        <cfif "#uploaded_sp.recordcount#" GT "0">
          <li class="text-success">Supplier Price</li>
        <cfelse>
          <li class="text-danger">Supplier Price</li>
        </cfif>

  <cfquery name="uploaded_po" datasource="agbcodes_ldi">
    SELECT *
    FROM uploaded_files
    WHERE doc_purpose = "Purchase%20Order" AND linked_job = "#this_job.formatted_id#"
  </cfquery>

        <cfif "#uploaded_po.recordcount#" GT "0">
          <li class="text-success">Purchase Order</li>
        <cfelse>
          <li class="text-danger">Purchase Order</li>
        </cfif>

  <cfquery name="uploaded_po" datasource="agbcodes_ldi">
    SELECT *
    FROM uploaded_files
    WHERE doc_purpose = "Drawing" AND linked_job = "#this_job.formatted_id#"
  </cfquery>

        <cfif "#uploaded_po.recordcount#" GT "0">
          <li class="text-success">Drawing</li>
        <cfelse>
          <li class="text-danger">Drawing</li>
        </cfif>

  <cfquery name="uploaded_po" datasource="agbcodes_ldi">
    SELECT *
    FROM uploaded_files
    WHERE doc_purpose = "Materials%20Map" AND linked_job = "#this_job.formatted_id#"
  </cfquery>

        <cfif "#uploaded_po.recordcount#" GT "0">
          <li class="text-success">Materials Map</li>
        <cfelse>
          <li class="text-danger">Materials Map</li>
        </cfif>

  <cfquery name="uploaded_po" datasource="agbcodes_ldi">
    SELECT *
    FROM uploaded_files
    WHERE doc_purpose = "Material%20Certificate" AND linked_job = "#this_job.formatted_id#"
  </cfquery>

        <cfif "#uploaded_po.recordcount#" GT "0">
          <li class="text-success">Material Certificate</li>
        <cfelse>
          <li class="text-danger">Material Certificate</li>
        </cfif>

  <cfquery name="uploaded_po" datasource="agbcodes_ldi">
    SELECT *
    FROM uploaded_files
    WHERE doc_purpose = "Hydro%20test%20Certificate" AND linked_job = "#this_job.formatted_id#"
  </cfquery>

        <cfif "#uploaded_po.recordcount#" GT "0">
          <li class="text-success">Hydro test Certificate</li>
        <cfelse>
          <li class="text-danger">Hydro test Certificate</li>
        </cfif>

  <cfquery name="uploaded_po" datasource="agbcodes_ldi">
    SELECT *
    FROM uploaded_files
    WHERE doc_purpose = "Certificate%20of%20conformity" AND linked_job = "#this_job.formatted_id#"
  </cfquery>

        <cfif "#uploaded_po.recordcount#" GT "0">
          <li class="text-success">Certificate of conformity</li>
        <cfelse>
          <li class="text-danger">Certificate of conformity</li>
        </cfif>

  <cfquery name="uploaded_po" datasource="agbcodes_ldi">
    SELECT *
    FROM uploaded_files
    WHERE doc_purpose = "Label" AND linked_job = "#this_job.formatted_id#"
  </cfquery>

        <cfif "#uploaded_po.recordcount#" GT "0">
          <li class="text-success">Label</li>
        <cfelse>
          <li class="text-danger">Label</li>
        </cfif>

       </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
<!--         <button type="button" class="btn btn-primary">Save changes</button>
 -->      </div>
    </div>
  </div>
</div>


<!-- Button trigger modal -->
<button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#new_file_modal">
  Upload a new file
</button>


  <hr>
<cfoutput>

<div class="row">
  <div class="col-3">

    <h5>Quote PDF's </h5>

    <cfloop query="linked_pdf_quotes">
      <cfset absolute_path_and_file = "#location##linked_pdf_quotes.file_name#">
      <a href="contracts/#this_job.formatted_id#/#linked_pdf_quotes.file_name#" target="_blank">#linked_pdf_quotes.file_name#</a><br>
    </cfloop>

  </div>

  <div class="col-3">

    <h5>Works Order PDF's </h5>

    <cfloop query="linked_pdf_wo">
      <cfset absolute_path_and_file = "#location##linked_pdf_wo.file_name#">
      <a href="contracts/#this_job.formatted_id#/#linked_pdf_wo.file_name#" target="_blank">#linked_pdf_wo.file_name#</a><br>
    </cfloop>

  </div>

  <div class="col-3">

    <h5>Invoice PDF's </h5>

    <cfloop query="linked_pdf_invoices">
      <cfset absolute_path_and_file = "#location##linked_pdf_invoices.file_name#">
      <a href="contracts/#this_job.formatted_id#/#linked_pdf_invoices.file_name#" target="_blank">#linked_pdf_invoices.file_name#</a><br>
    </cfloop>

  </div>



  <div class="col-3">

    
      <h5>Uploaded Files <span class="badge badge-pirmary">#uploaded_files.recordcount#</span></h5>

    <cfif "#uploaded_files.recordcount#" GT "0">

      <cfloop query="uploaded_files">
        <cfset absolute_path_and_file = "#location##uploaded_files.file_name#">
        <a href="contracts/#this_job.formatted_id#/#uploaded_files.file_name#" target="_blank">#uploaded_files.file_name#</a><br>
        <small>#URLDecode(uploaded_files.doc_purpose)#</small>
      </cfloop>

    <cfelse>

      <p>No supporting have been uploaded to this contract yet.</p>

    </cfif>

  </div>

</div>
</cfoutput>



<!-- Modal -->
<div class="modal fade" id="new_file_modal" tabindex="-1" role="dialog" aria-labelledby="new_file_modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload a new file</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

    <cfoutput>
      <form action="processing_files/file_upload.cfm?id=#URL.id#" method="post" enctype="multipart/form-data" addtoken="false">

        <cfoutput>
          <input type="hidden" name="linked_prod_id" id="linked_prod_id" value="#URL.id#">
        </cfoutput>

        <div class="modal-body">                  

          <div class="form-group">
            <label for="new_file_name">New File Name</label>
            <input type="text" name="new_file_name" id="new_file_name" class="form-control" required>
          </div>

          <div class="form-group">
            <label for="doc_purpose">Document Purpose<br>
            <input type="text" id="doc_purpose" name="doc_purpose" list="doc_purpose_list" class="form-control">
          </div>

          <div class="form-group">
            <label for="new_file">New File to upload<br> <small>(Only PDF's accepted)</small></label><br>
            <input type="file" id="new_file" name="new_file" accept=".pdf"><br>
          </div> 


          <datalist id="doc_purpose_list">
               <option value="Supplier Price">
               <option value="Purchase Order">
               <option value="Drawing">
               <option value="Materials Map">
               <option value="Material Certificate">
               <option value="Hydro test Certificate">
               <option value="Certificate of Conformity">
               <option value="Lable">
          </datalist>
        </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-success">Upload File</button>
        </div>

      </form>
</cfoutput>

    </div>
  </div>
</div>